﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BiaTienSiDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class BiaTienSiDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BiaTienSiDAO"/> class.
        /// </summary>
        public BiaTienSiDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<BiaTienSi> GetIQueryable()
        {
            return EDMsDataContext.BiaTienSis;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<BiaTienSi> GetAll()
        {
            return EDMsDataContext.BiaTienSis.OrderBy(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public BiaTienSi GetById(int id)
        {
            return this.EDMsDataContext.BiaTienSis.FirstOrDefault(ob => ob.ID == id);
        }

        public BiaTienSi GetByYear(int year)
        {
            return this.EDMsDataContext.BiaTienSis.FirstOrDefault(ob => ob.NamKhoaThi == year);
        }

        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(BiaTienSi ob)
        {
            try
            {
                EDMsDataContext.AddToBiaTienSis(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(BiaTienSi src)
        {
            try
            {
                BiaTienSi des = (from rs in this.EDMsDataContext.BiaTienSis
                                where rs.ID == src.ID
                                select rs).First();

                des.TenBia = src.TenBia;
                des.Kientruc = src.Kientruc;
                des.ThoiGianDungBia = src.ThoiGianDungBia;
                des.NguoiThamGiaDungBia = src.NguoiThamGiaDungBia;
                des.TieuDeKhoaThi = src.TieuDeKhoaThi;
                des.NamKhoaThi = src.NamKhoaThi;
                des.QuanToChucKhoaThi = src.QuanToChucKhoaThi;
                des.CachThucToChuc = src.CachThucToChuc;
                des.HoTenQueQuan = src.HoTenQueQuan;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(BiaTienSi src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
