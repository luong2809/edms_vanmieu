﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaiLieuHoanCongDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class TaiLieuHoanCongDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaiLieuHoanCongDAO"/> class.
        /// </summary>
        public TaiLieuHoanCongDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TaiLieuHoanCong> GetIQueryable()
        {
            return EDMsDataContext.TaiLieuHoanCongs;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHoanCong> GetAll()
        {
            return EDMsDataContext.TaiLieuHoanCongs.ToList();
        }

        public List<TaiLieuHoanCong> GetByConstruction(List<int> listConstruction)
        {
            return EDMsDataContext.TaiLieuHoanCongs.Where(ob => listConstruction.Contains(ob.ConstructionID.Value)).ToList();
        }

        /// <summary>
        /// The get all by owner.
        /// </summary>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHoanCong> GetAllByOwner(int createdBy)
        {
            return EDMsDataContext.TaiLieuHoanCongs.Where(t => t.CreatedBy == createdBy).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public TaiLieuHoanCong GetById(int id)
        {
            return this.EDMsDataContext.TaiLieuHoanCongs.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHoanCong> GetSpecific(int tranId)
        {
            return this.EDMsDataContext.TaiLieuHoanCongs.Where(t => t.ID == tranId).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(TaiLieuHoanCong ob)
        {
            try
            {
                EDMsDataContext.AddToTaiLieuHoanCongs(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(TaiLieuHoanCong src)
        {
            try
            {
                TaiLieuHoanCong des = (from rs in this.EDMsDataContext.TaiLieuHoanCongs
                                       where rs.ID == src.ID
                                       select rs).First();

                des.DocumentNumber = src.DocumentNumber;
                des.Description = src.Description;
                des.ProjectName = src.ProjectName;
                des.Year = src.Year;
                des.ConstructionID = src.ConstructionID;
                des.ConstructionName = src.ConstructionName;
                des.DocTypeID = src.DocTypeID;
                des.DocTypeName = src.DocTypeName;
                des.FilePath = src.FilePath;
                des.FileExtension = src.FileExtension;
                des.FileExtensionIcon = src.FileExtensionIcon;
                des.FileSize = src.FileSize;
                des.HasAttachFile = src.HasAttachFile;
                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(TaiLieuHoanCong src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
