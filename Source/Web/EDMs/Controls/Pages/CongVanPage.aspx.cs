﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Business.Services;

namespace EDMs.Controls.Pages
{
    public partial class CongVanPage : Page
    {
        private readonly CongVanService congVanService = new CongVanService();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.Page.IsPostBack)
            {
            }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocuments();
        }

        protected void LoadDocuments(bool isbind = false)
        {
            this.grdDocument.DataSource = this.congVanService.GetAll();

            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.congVanService.Delete(disId);

            this.grdDocument.Rebind();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var ID = item.GetDataKeyValue("ID");
                ImageButton btn = (ImageButton)item["EditButton"].Controls[0];
                btn.Attributes.Add("OnClick", "ShowEditForm(" + ID + ");return false;");
            }
        }
    }
}