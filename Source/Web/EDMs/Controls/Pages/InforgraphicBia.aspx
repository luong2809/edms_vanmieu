﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InforgraphicBia.aspx.cs" Inherits="edms.Controls.Pages.InforgraphicBia" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="/Content/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/Content/styles.css" />

    <style type="text/css">
        html, body, form {
            height: 100%;
        }

        .container-fluid {
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .bg-content {
            background-image: url(/Images/inforgraphic-bia.svg);
            background-repeat: no-repeat;
            width: 870px;
            height: 1540px;
        }

        .margin-header {
            margin-top: 340px;
        }

        .tenbia {
            color: #343434;
            font-size: 20px;
            font-weight: bold;
        }

        .tentiensi {
            color: #343434;
            font-size: 16px;
            font-weight: bold;
            white-space:pre-line
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptBlock runat="server">
            <script src="/Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
            <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript">

            </script>
        </telerik:RadScriptBlock>
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>

        <div class="container-fluid">
            <div class="row bg-content">
                <div class="col">
                    <div class="row margin-header">
                    </div>
                    <div class="row">
                        <div class="col-3"></div>
                        <div class="col-6 text-center">
                            <asp:Label ID="lblTenBia" runat="server" CssClass="tenbia" Text=""></asp:Label>
                        </div>
                        <div class="col-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-4">
                            <asp:Label ID="lblCotTrai" runat="server" CssClass="tentiensi" Text=""></asp:Label>
                        </div>
                        <div class="col-4">
                            <asp:Label ID="lblCotPhai" runat="server" CssClass="tentiensi" Text=""></asp:Label>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
