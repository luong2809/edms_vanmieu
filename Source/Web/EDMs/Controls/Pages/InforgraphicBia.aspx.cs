﻿using EDMs.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace edms.Controls.Pages
{
    public partial class InforgraphicBia : System.Web.UI.Page
    {
        private BiaTienSiService biaTienSiService = new BiaTienSiService();
        private TienSiService tienSiService = new TienSiService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!this.Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["biaId"]))
                {
                    var obj = this.biaTienSiService.GetById(Convert.ToInt32(this.Request.QueryString["biaId"]));
                    if(obj!=null)
                    {
                        lblTenBia.Text = obj.TenBia;
                        var tiensiList = this.tienSiService.GetByBia(obj.ID);
                        int count=1;
                        foreach (var item in tiensiList)
                        {
                            if(count<31)
                            {
                                this.lblCotTrai.Text += count + ". " + item.HoTen + "\n";
                            }
                            else
                            {
                                this.lblCotPhai.Text += count + ". " + item.HoTen + "\n";
                            }
                            count++;
                        }
                    }
                }
            }
        }
    }
}