﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site_VM.Master" AutoEventWireup="true" CodeBehind="MenuPermissionPage.aspx.cs" Inherits="EDMs.Controls.Systems.MenuPermissionPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <style type="text/css">
        .span-page {
            font-size: 28px;
            color: #B51F09;
        }

        .no-padding-col {
            padding-left: 0;
            padding-right: 0;
        }

        .bg-page {
            background-color: #fff;
        }

        .border-col {
            border: 1px solid #d3d3d3;
        }

        .padding-page {
            padding: 10px;
        }

        .btn-save {
            width: 100%;
            height: 35px;
            background-color: #4E0B0B;
            color: #FAF9EF;
            border: 0;
            border-radius: 3px 4px;
            /*font-family: Roboto, sans-serif;*/
            font-size: 13px;
            cursor: pointer;
        }

        .RadToolBar .rtbChoiceArrow {
            width: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap {
            background-color: transparent !important;
            border: 0 !important;
        }

        .RadToolBarDropDown_Bootstrap {
            color: #B51F09 !important;
        }

        .RadToolBar_Bootstrap .rtbOuter {
            border: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap .rtbWrap {
            padding: 6px 10px !important;
        }

        .RadToolBar .rtbIcon {
            width: 27px;
            height: 27px;
        }

        .RadToolBarDropDown_Bootstrap .rtbIcon {
            width: 16px;
            top: 4px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentTool" runat="server">
    <%-- manager code js --%>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $('iframe').load(function () {
                    //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                });
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function clientNodeChecked(sender, eventArgs) {
                var i;
                var node = eventArgs.get_node();
                var childNodes = node.get_allNodes();

                eventArgs.get_node().set_checkable(false);

                //To change the status of all child nodes when the parent checked/ unchecked
                if (childNodes.length > 0) {
                    for (i = 0; i < childNodes.length; i++) {
                        childNodes[i].set_checked(node.get_checked());
                    }
                }

                //To uncheck parent Nodes when any child Node has been unchecked
                if (!node.get_checked()) {
                    while (node.get_parent().set_checked != null) {
                        node.get_parent().set_checked(false);
                        node = node.get_parent();
                    }
                }

                //To check parent Nodes when all child nodes has been checked
                if (node.get_checked()) {
                    var parentNode = node.get_parent();
                    while (parentNode.set_checked != null) {
                        var allNodes = parentNode.get_nodes();
                        for (i = 0; i < allNodes.get_count() ; i++) {
                            if (!allNodes.getNode(i).get_checked()) {
                                eventArgs.get_node().set_checkable(true);
                                return;
                            }
                        }
                        parentNode.set_checked(true);
                        parentNode = parentNode.get_parent();
                    }
                }

                eventArgs.get_node().set_checkable(true);
            }
        </script>
    </telerik:RadCodeBlock>

    <%-- manager ajax page --%>
    <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="ajaxCustomer_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <%-- loading page --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" MinDisplayTime="1000" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>

    <%-- hidden field --%>
    <asp:HiddenField runat="server" ID="lblId" />

    <%-- manager window popup --%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditPage" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="570" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="row justify-content-between align-items-center">
        <div class="col-lg-4">
            <span class="span-page">MENU PERMISSION</span>
        </div>
        <div class="col-lg-auto">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <div class="container bg-page">
        <div class="row padding-page">
            <div class="col padding-page border-col">
                <div class="row padding-page justify-content-center">
                    <div class="col-auto">
                        <telerik:RadComboBox ID="ddlGroup" runat="server" Skin="Bootstrap" RenderMode="Lightweight" AutoPostBack="true" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged"></telerik:RadComboBox>
                    </div>
                    <div class="col no-padding-col">
                        <asp:Button ID="btnSave" runat="server" Text="LƯU" CssClass="btn-save" OnClick="btnSave_Click" Width="150px" />
                    </div>
                </div>
            </div>
            <div class="col border-col">
                <div class="row padding-page">
                    <telerik:RadTreeView ID="treePemissionMenus" runat="server" Skin="Bootstrap" Height="100%" Width="100%" CheckBoxes="True" TriStateCheckBoxes="False" CheckChildNodes="False"
                        DataFieldID="MenuId"
                        DataTextField="Description"
                        DataFieldParentID="ParentId"
                        DataValueField="MenuId"
                        OnClientNodeChecked="clientNodeChecked">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="true" CheckedField="IsPermitted" />
                        </DataBindings>
                    </telerik:RadTreeView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
