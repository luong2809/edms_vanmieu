﻿using System;
using System.Web.UI;
using EDMs.Data.Entities;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Utilities;
using System.IO;

namespace EDMs.Controls.Systems
{
    public partial class ChangePasswordEditPage : Page
    {
        private readonly UserService userService = new UserService();

        public ChangePasswordEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.CreatedInfo.Visible = true;
                var obj = this.userService.GetByID(UserSession.Current.User.Id);
                if (obj != null)
                {
                    var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                    this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                    if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                    {
                        this.lblCreated.Text += "<br/>";
                        var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                        this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                    }
                    else
                    {
                        this.lblUpdated.Visible = false;
                    }
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.txtNewPassword.Text.Length > 0 && this.txtNewPassword.Text == this.txtReNewPassword.Text)
            {
                var id = Convert.ToInt32(this.Request.QueryString["obj"]);
                var obj = this.userService.GetByID(UserSession.Current.User.Id);
                if (obj != null)
                {
                    obj.LastUpdatedBy = UserSession.Current.User.Id;
                    obj.LastUpdatedDate = DateTime.Now;
                    this.userService.ChangePassword(UserSession.Current.User.Id, Utility.GetMd5Hash(txtNewPassword.Text));
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
            else
            {
                this.lblMessage.Text = "Nhập lại mật khẩu không trùng với mật khẩu mới.";
            }
        }
    }
}