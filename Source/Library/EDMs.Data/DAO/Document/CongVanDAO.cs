﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CongVanDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class CongVanDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CongVanDAO"/> class.
        /// </summary>
        public CongVanDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<CongVan> GetIQueryable()
        {
            return EDMsDataContext.CongVans;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<CongVan> GetAll()
        {
            return EDMsDataContext.CongVans.ToList();
        }

        /// <summary>
        /// The get all by owner.
        /// </summary>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<CongVan> GetAllByOwner(int createdBy)
        {
            return EDMsDataContext.CongVans.Where(t => t.CreatedBy == createdBy).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public CongVan GetById(int id)
        {
            return this.EDMsDataContext.CongVans.FirstOrDefault(ob => ob.ID == id);
        }

        public CongVan GetLasted()
        {
            return this.EDMsDataContext.CongVans.OrderByDescending(t=>t.ID).FirstOrDefault();
        }

        #endregion

        #region GET ADVANCE

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<CongVan> GetSpecific(int tranId)
        {
            return this.EDMsDataContext.CongVans.Where(t => t.ID == tranId).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(CongVan ob)
        {
            try
            {
                EDMsDataContext.AddToCongVans(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(CongVan src)
        {
            try
            {
                CongVan des = (from rs in this.EDMsDataContext.CongVans
                                where rs.ID == src.ID
                                select rs).First();

                des.SoCongVan = src.SoCongVan;
                des.Description = src.Description;
                des.SoNoiGui = src.SoNoiGui;
                des.TenNoiGui = src.TenNoiGui;
                des.SoNoiNhan = src.SoNoiNhan;
                des.TenNoiNhan = src.TenNoiNhan;
                des.SoNguoiGui = src.SoNguoiGui;
                des.TenNguoiGui = src.TenNguoiGui;
                des.SoNguoiNhan = src.SoNguoiNhan;
                des.TenNguoiNhan = src.TenNguoiNhan;
                des.NgayGui = src.NgayGui;
                des.LoaiCongVan = src.LoaiCongVan;
                des.FilePath = src.FilePath;
                des.FileExtension = src.FileExtension;
                des.FileExtensionIcon = src.FileExtensionIcon;
                des.FileSize = src.FileSize;
                des.HasAttachFile = src.HasAttachFile;
                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(CongVan src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
