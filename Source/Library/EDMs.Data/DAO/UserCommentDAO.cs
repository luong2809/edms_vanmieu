﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserCommentDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO;
using EDMs.Data.Entities;

namespace SPF.Data.DAO
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class UserCommentDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserCommentDAO"/> class.
        /// </summary>
        public UserCommentDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<UserComment> GetIQueryable()
        {
            return this.EDMsDataContext.UserComments;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<UserComment> GetAll()
        {
            return this.EDMsDataContext.UserComments.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public UserComment GetById(int id)
        {
            return this.EDMsDataContext.UserComments.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        public bool Update(UserComment src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.UserComments
                                where rs.ID == src.ID
                                select rs).First();

                des.UserId = src.UserId;
                des.UserName = src.UserName;
                des.Comment = src.Comment;
                des.Reply = src.Reply;
                des.IsReply = src.IsReply;
                des.ReplyBy = src.ReplyBy;
                des.ReplyDate = src.ReplyDate;
                des.ReplyName = src.ReplyName;

                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(UserComment ob)
        {
            try
            {
                this.EDMsDataContext.AddToUserComments(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        
        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(UserComment src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
