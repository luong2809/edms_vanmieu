﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class TienSiService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly TienSiDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TienSiService"/> class.
        /// </summary>
        public TienSiService()
        {
            this.repo = new TienSiDAO();
        }

        #region Get (Advances)
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<TienSi> GetAll()
        {
            return this.repo.GetAll();
        }

        public List<TienSi> GetByBia(int biaID)
        {
            return biaID != 0 ? this.repo.GetByBia(biaID) : this.repo.GetAll();
        }

        public List<TienSi> GetByList(List<int> list)
        {
            return repo.GetByList(list);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public TienSi GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public TienSi GetByName(string name)
        {
            return this.repo.GetByName(name);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(TienSi bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(TienSi bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(TienSi bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
