﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class GroupFunctionsDAO : BaseDAO
    {
        public GroupFunctionsDAO() : base() { }

        #region GET (Basic)
        public IQueryable<GroupFunction> GetIQueryable()
        {
            return EDMsDataContext.GroupFunctions;
        }

        public List<GroupFunction> GetAll()
        {
            return EDMsDataContext.GroupFunctions.ToList();
        }

        public GroupFunction GetById(int id)
        {
            return EDMsDataContext.GroupFunctions.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region Get (Advances)

       
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the GroupFunctions.
        /// </summary>
        /// <param name="GroupFunctions">The GroupFunctions.</param>
        /// <returns></returns>
        public bool DeleteGroupFunctions(List<GroupFunction> GroupFunctions)
        {
            try
            {
                foreach (var item in GroupFunctions)
                {
                    EDMsDataContext.DeleteObject(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Adds the GroupFunctions.
        /// </summary>
        /// <param name="GroupFunctions">The GroupFunctions.</param>
        /// <returns></returns>
        public bool AddGroupFunctions(List<GroupFunction> GroupFunctions)
        {
            try
            {
                foreach (var item in GroupFunctions)
                {
                    EDMsDataContext.AddToGroupFunctions(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool Insert(GroupFunction ob)
        {
            try
            {
                EDMsDataContext.AddToGroupFunctions(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(GroupFunction ob)
        {
            try
            {
                GroupFunction _ob;

                _ob = (from rs in EDMsDataContext.GroupFunctions
                       where rs.ID == ob.ID
                       select rs).First();

                _ob.Name = ob.Name;
                _ob.MemberID = ob.MemberID;
                _ob.MemberName = ob.MemberName;
                _ob.LastUpdatedBy = ob.LastUpdatedBy;
                _ob.LastUpdatedDate = ob.LastUpdatedDate;
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(GroupFunction ob)
        {
            try
            {
                GroupFunction _ob = GetById(ob.ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                GroupFunction _ob = GetById(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

    }
}
