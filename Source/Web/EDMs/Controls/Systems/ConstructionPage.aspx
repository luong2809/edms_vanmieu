﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site_VM.Master" AutoEventWireup="true" CodeBehind="ConstructionPage.aspx.cs" Inherits="EDMs.Controls.Systems.ConstructionPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <style type="text/css">
        .span-page {
            font-size: 28px;
            color: #B51F09;
        }

        .div-tool {
            height: 70px;
            padding-right: 15px;
        }

        .RadToolBar .rtbChoiceArrow {
            width: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap {
            background-color: transparent !important;
            border: 0 !important;
        }

        .RadToolBarDropDown_Bootstrap {
            /*font-family: Roboto, sans-serif !important;*/
            color: #B51F09 !important;
        }

        .RadToolBar_Bootstrap .rtbOuter {
            /*background-color: transparent !important;*/
            border: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap .rtbWrap {
            padding: 6px 10px !important;
        }

        .RadToolBar .rtbIcon {
            width: 27px;
            height: 27px;
        }

        .RadToolBarDropDown_Bootstrap .rtbIcon {
            width: 16px;
            top: 4px !important;
        }

        .RadButton_Bootstrap.rbButton {
            background-color: #4E0B0B !important;
            color: #fff !important;
        }

        #ctl00_contentTool_ctl00_contentBody_grdDocumentPanel {
            height: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentTool" runat="server">
    <%-- manager code js --%>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var ajaxManager;
            function pageLoad() {
                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function RowClick(sender, eventArgs) {
                var id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblId.ClientID %>").value = id;
            }

            function ShowEditForm(id) {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("CategoryEditPage.aspx?obj=" + id, "EditPage");
            }

            function ShowAddForm() {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("CategoryEditPage.aspx", "AddNew");
            }

            function refreshGrid(arg) {
                ajaxManager.ajaxRequest("RebindTreeView");
            }

            //expand dropdown left
            function dropdownOpened(sender, args) {
                var dropDownElem = args.get_item().get_element();
                var rightPos = $telerik.getLocation(dropDownElem).x + $telerik.getSize(dropDownElem).width;
                var dropDownElemContainer = args.get_item()._animationContainer;
                var leftPos = rightPos - dropDownElemContainer.clientWidth;
                dropDownElemContainer.style.left = leftPos + "px";
            }
            //disable ajax control
            function RequestStart(sender, args) {
                args.EventTargetElement.disabled = true;
            }
            function ResponseEnd(sender, args) {
                args.EventTargetElement.disabled = false;
            }

            function onClientContextMenuItemClicking(sender, args) {
                var menuItem = args.get_menuItem();
                var treeNode = args.get_node();
                menuItem.get_menu().hide();

                switch (menuItem.get_value()) {
                    case "Add":
                        var owd = $find("<%=EditPage.ClientID %>");
                    owd.Show();
                    owd.setUrl("ConstructionEditPage.aspx?action=add&parentId=" + treeNode.get_value(), "CustomerDialog");
                    break;
                    case "Edit":
                        var owd = $find("<%=EditPage.ClientID %>");
                        owd.Show();
                        owd.setUrl("ConstructionEditPage.aspx?Id=" + treeNode.get_value(), "CustomerDialog");
                        break;
                    case "Delete":
                        var result = confirm("Are you sure you want to delete the item: " + treeNode.get_text());
                        if (result) {
                            ajaxManager.ajaxRequest("Delete_" + treeNode.get_value());
                        }
                        //args.set_cancel(!result);
                        break;
                }
            }

            function rtvContruction_OnNodeExpandedCollapsed(sender, eventArgs) {
                var allNodes = eventArgs._node.get_treeView().get_allNodes();
                var i;
                var selectedNodes = "";
                for (i = 0; i < allNodes.length; i++) {
                    if (allNodes[i].get_expanded())
                        selectedNodes += allNodes[i].get_value() + "*";
                }
                Set_Cookie("expandedNodesConstruction", selectedNodes, 30);
            }
            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                    ((path) ? ";path=" + path : "") +
                    ((domain) ? ";domain=" + domain : "") +
                    ((secure) ? ";secure" : "");
            }
        </script>
    </telerik:RadCodeBlock>

    <%-- manager ajax page --%>
    <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="ajaxCustomer_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rtvContruction" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <%-- loading page --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" MinDisplayTime="1000" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>

    <%-- hidden field --%>
    <asp:HiddenField runat="server" ID="lblId" />

    <%-- manager window popup --%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditPage" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="570" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="row justify-content-between align-items-center">
        <div class="col-lg-4">
            <span class="span-page">CÔNG TRÌNH</span>
        </div>
        <div class="col-lg-auto">
            <div class="row align-items-center invisible">
                <div class="col-lg-auto" style="padding-right: 0">
                    <div class="row align-items-center div-tool">
                        <telerik:RadButton ID="btnAdd" runat="server" Text="TẠO MỚI" Skin="Bootstrap" RenderMode="Lightweight" Height="40px" AutoPostBack="false" OnClientClicking="ShowAddForm">
                            <Icon PrimaryIconUrl="~/Images/add-trang.svg" PrimaryIconTop="6px" PrimaryIconLeft="8px" PrimaryIconHeight="19px" PrimaryIconWidth="19px" />
                        </telerik:RadButton>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <telerik:RadToolBar ID="CustomerMenu" runat="server" Skin="Bootstrap" Width="100%" Height="40px" OnClientDropDownOpened="dropdownOpened">
                        <Items>
                            <telerik:RadToolBarDropDown ImageUrl="~/Images/settings-do.svg" DropDownWidth="150px">
                                <Buttons>
                                    
                                </Buttons>
                            </telerik:RadToolBarDropDown>
                        </Items>
                    </telerik:RadToolBar>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <%-- grid data --%>
    <div class="container-fluid bg-light">
        <telerik:RadTreeView ID="rtvContruction" runat="server" Skin="Default" RenderMode="Lightweight" Width="100%" Height="100%" ShowLineImages="False"
            OnNodeDataBound="rtvContruction_OnNodeDataBound"
            OnClientContextMenuItemClicking="onClientContextMenuItemClicking"
            OnClientNodeExpanded="rtvContruction_OnNodeExpandedCollapsed"
            OnClientNodeCollapsed="rtvContruction_OnNodeExpandedCollapsed">
            <ContextMenus>
                <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Default" RenderMode="Lightweight">
                    <Items>
                        <telerik:RadMenuItem Value="Add" Text="Add new" ImageUrl="~/Images/add-do-16px.svg" />
                        <telerik:RadMenuItem Value="Edit" Text="Edit" ImageUrl="~/Images/edit-do-16px.svg" />
                        <telerik:RadMenuItem Value="Delete" Text="Delete" ImageUrl="~/Images/delete-do-16px.svg" />
                    </Items>
                </telerik:RadTreeViewContextMenu>
            </ContextMenus>
            <DataBindings>
                <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
            </DataBindings>
        </telerik:RadTreeView>
    </div>
</asp:Content>
