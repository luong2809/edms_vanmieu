﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class RoleDAO : BaseDAO
    {
        public RoleDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Role> GetIQueryable()
        {
            return EDMsDataContext.Roles;
        }
        
        public List<Role> GetAll()
        {
            return EDMsDataContext.Roles.OrderBy(t => t.Name).ToList();
        }

        public Role GetByID(int ID)
        {
            return EDMsDataContext.Roles.FirstOrDefault(ob => ob.Id == ID);
        }
       
        #endregion

        #region Get (Advances)

        #endregion

        #region Insert, Update, Delete
        public bool Insert(Role ob)
        {
            try
            {
                EDMsDataContext.AddToRoles(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Role ob)
        {
            try
            {
                Role _ob;

                _ob = (from rs in EDMsDataContext.Roles
                       where rs.Id == ob.Id
                       select rs).First();

                _ob.Name = ob.Name;
                _ob.Description = ob.Description;
                _ob.Email = ob.Email;
                _ob.Active = ob.Active;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Role ob)
        {
            try
            {
                Role _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Role _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
