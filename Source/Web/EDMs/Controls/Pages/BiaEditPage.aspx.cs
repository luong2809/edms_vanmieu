﻿using System;
using System.Web.UI;
using EDMs.Data.Entities;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;

namespace EDMs.Controls.Pages
{
    public partial class BiaEditPage : Page
    {
        private readonly UserService userService = new UserService();
        private readonly BiaTienSiService biaTienSiService = new BiaTienSiService();

        public BiaEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = biaTienSiService.GetById(Convert.ToInt32(Request.QueryString["obj"]));
                    if (obj != null)
                    {
                        this.txtBia.Text = obj.TenBia;
                        this.txtKientruc.Text = obj.Kientruc;
                        this.txtThoigiandungbia.Text = obj.ThoiGianDungBia.ToString();
                        this.txtNguoithamgiadungbia.Text = obj.NguoiThamGiaDungBia;
                        this.txtTieudekhoathi.Text = obj.TieuDeKhoaThi;
                        this.txtNamtochuc.Text = obj.NamKhoaThi.ToString();
                        this.txtQuantochuc.Text = obj.QuanToChucKhoaThi;
                        this.txtCachthuctochuc.Text = obj.CachThucToChuc;
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var AreaId = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.biaTienSiService.GetById(AreaId);
                    if (obj != null)
                    {
                        obj.TenBia = this.txtBia.Text.Trim();
                        int namThi;
                        int.TryParse(this.txtNamtochuc.Text.Trim(),out namThi);
                        obj.NamKhoaThi = namThi;
                        int thoiGianDungBia;
                        int.TryParse(this.txtThoigiandungbia.Text.Trim(), out thoiGianDungBia);
                        obj.ThoiGianDungBia = thoiGianDungBia;
                        obj.Kientruc = this.txtKientruc.Text.Trim();
                        obj.NguoiThamGiaDungBia = this.txtNguoithamgiadungbia.Text.Trim();
                        obj.TieuDeKhoaThi = this.txtTieudekhoathi.Text.Trim();
                        obj.QuanToChucKhoaThi = this.txtQuantochuc.Text.Trim();
                        obj.CachThucToChuc = this.txtCachthuctochuc.Text.Trim();
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        this.biaTienSiService.Update(obj);
                    }
                }
                else
                {
                    var obj = new BiaTienSi();
                    obj.TenBia = this.txtBia.Text.Trim();
                    int namThi;
                    int.TryParse(this.txtNamtochuc.Text.Trim(), out namThi);
                    obj.NamKhoaThi = namThi;
                    int thoiGianDungBia;
                    int.TryParse(this.txtThoigiandungbia.Text.Trim(), out thoiGianDungBia);
                    obj.ThoiGianDungBia = thoiGianDungBia;
                    obj.Kientruc = this.txtKientruc.Text.Trim();
                    obj.NguoiThamGiaDungBia = this.txtNguoithamgiadungbia.Text.Trim();
                    obj.TieuDeKhoaThi = this.txtTieudekhoathi.Text.Trim();
                    obj.QuanToChucKhoaThi = this.txtQuantochuc.Text.Trim();
                    obj.CachThucToChuc = this.txtCachthuctochuc.Text.Trim();
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    this.biaTienSiService.Insert(obj);
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }
    }
}