﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using ICSharpCode.SharpZipLib.Zip;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Data.Entities;

namespace EDMs.Controls.Pages
{


    public partial class DownloadFolder : Page
    {
        public readonly DocumentService documentService = new DocumentService();
        private readonly FolderService folderService = new FolderService();
        private readonly GroupFunctionService _GroupfunctionService = new GroupFunctionService();
        private readonly FolderPermissionService _FolderPermisstionService = new FolderPermissionService();

        private List<int> GroupFunctionList
        {
            get
            {
                return this._GroupfunctionService.GetByRoleId(UserSession.Current.User.Id).Select(t => t.ID).ToList();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
              
                var type = Request.QueryString["type"];
                var selectfolder = Convert.ToInt32(Request.QueryString["selectedFolder"]);
                var folderObj = this.folderService.GetById(selectfolder);
                if (folderObj != null)
                {
                    var ListChildFodler = (List<int>)Session["ListIdFolder"];
                    var docList = this.documentService.GetAllByFolder(ListChildFodler);
                    var folderSize = docList.Sum(t => t.FileSize);
                    if (folderSize <= 2147483648)
                    {
                        var folderPermission = this.folderService.GetAllSpecificFolder(ListChildFodler.Distinct().ToList());

                        var filename = DateTime.Now.ToBinary() + "_DocPack.zip";
                        var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
                        // ICSharpCode.SharpZipLib.Zip.ZipOutputStream zip = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(File.Create(serverTotalDocPackPath));
                        //  zip.SetLevel(9);
                        // ZipFolder(folderObj, zip, folderPermission);
                        //zip.Finish();
                        // zip.Close();
                       
                            using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                        {
                            zip.AlternateEncoding = Encoding.Unicode;
                            zip.AddDirectory(Server.MapPath(folderObj.DirName));
                           // zip.SaveProgress += zip_SaveProgress;
                            zip.Save(serverTotalDocPackPath);
                        };
                       
                        Response.ClearContent();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("Content-Disposition",
                                          "attachment; filename=" + folderObj.Name + ".zip;");
                        Response.TransmitFile(serverTotalDocPackPath);
                        Response.Flush();
                        File.Delete(serverTotalDocPackPath);
                        Response.End();

                        lbMeseger.Text = "Complete!";
                        //this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
                        // ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "CancelEdit();", true);

                        Response.Write("Ok$" + filename + "$");

                    }
                    else
                    {
                        //Response.Write("Folder capacity is greater than 2GB");
                        lbMeseger.Text = "Folder capacity is greater than 2GB";
                    }

                }
            }
           
               // this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
              //  ScriptManager.RegisterStartupScript(Page, Page.GetType(), "mykey", "CancelEdit();", true);
       
        }

        //private void zip_SaveProgress( object sender, Ionic.Zip.SaveProgressEventArgs e)
        //{
        //    if (e.EventType == Ionic.Zip.ZipProgressEventType.Saving_BeforeWriteEntry)
        //    {
        //        UpdateProgress1.Invoka(new meth)
        //    }

        //}

        #region end
        public void ZipFolder(Folder CurrentFolder, ICSharpCode.SharpZipLib.Zip.ZipOutputStream zStream, List<Folder> ListFolder)
        {
            var SubFolders = ListFolder.Where(t => t.ParentID == CurrentFolder.ID).ToList(); //Directory.GetDirectories(CurrentFolder.);

            //calls the method recursively for each subfolder
            foreach (var Folder in SubFolders)
            {

                ZipFolder(Folder, zStream, ListFolder);
            }
            DirectoryInfo dr = new DirectoryInfo(Server.MapPath(CurrentFolder.DirName));
            string relativePath = dr.Name; // CurrentFolder.DirName.Substring(CurrentFolder.DirName.Length) + "/";

            //the path "/" is not added or a folder will be created
            //at the root of the file
            if (relativePath.Length > 1)
            {
                ZipEntry dirEntry;
                dirEntry = new ZipEntry(relativePath);
                dirEntry.DateTime = DateTime.Now;
            }

            //adds all the files in the folder to the zip
            var listDocuments = this.documentService.GetAllByFolder(CurrentFolder.ID);
            foreach (var file in listDocuments)
            {

                AddFileToZip(zStream, relativePath, Server.MapPath(file.FilePath));
            }
        }
        private void AddFileToZip(ICSharpCode.SharpZipLib.Zip.ZipOutputStream zStream, string relativePath, string file)
        {
            byte[] buffer = new byte[4096];

            //the relative path is added to the file in order to place the file within
            //this directory in the zip
            string fileRelativePath = (relativePath.Length > 1 ? relativePath : string.Empty) + Path.GetFileName(file);

            ZipEntry entry = new ZipEntry(fileRelativePath);
            entry.DateTime = DateTime.Now;
            zStream.PutNextEntry(entry);

            using (FileStream fs = File.OpenRead(file))
            {
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
        }
        #endregion
    }
}