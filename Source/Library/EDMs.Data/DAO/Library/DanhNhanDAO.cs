﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DanhNhanDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class DanhNhanDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DanhNhanDAO"/> class.
        /// </summary>
        public DanhNhanDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<DanhNhan> GetIQueryable()
        {
            return EDMsDataContext.DanhNhans;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DanhNhan> GetAll()
        {
            return EDMsDataContext.DanhNhans.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public DanhNhan GetById(int id)
        {
            return this.EDMsDataContext.DanhNhans.FirstOrDefault(ob => ob.ID == id);
        }

        public List<DanhNhan> GetByList(List<int> list)
        {
            return EDMsDataContext.DanhNhans.Where(t => list.Contains(t.ID)).OrderByDescending(t => t.ID).ToList();
        }

        #endregion

        #region GET ADVANCE
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(DanhNhan ob)
        {
            try
            {
                EDMsDataContext.AddToDanhNhans(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(DanhNhan src)
        {
            try
            {
                DanhNhan des = (from rs in this.EDMsDataContext.DanhNhans
                              where rs.ID == src.ID
                              select rs).First();

                des.Danh = src.Danh;
                des.Tu = src.Tu;
                des.Hieu = src.Hieu;
                des.Cha = src.Cha;
                des.Me = src.Me;
                des.NamSinh = src.NamSinh;
                des.NamMat = src.NamMat;
                des.TieuSu = src.TieuSu;
                des.GiaDinh = src.GiaDinh;
                des.QueQuan = src.QueQuan;
                des.VinhDanh = src.VinhDanh;
                des.TacPham = src.TacPham;
                des.FileName = src.FileName;
                des.FilePath = src.FilePath;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(DanhNhan src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
