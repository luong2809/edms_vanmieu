﻿using System;
using System.Web.UI;
using EDMs.Data.Entities;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Utilities;

namespace EDMs.Controls.Systems
{
    public partial class UsersEditPage : Page
    {
        private readonly UserService userService = new UserService();
        private readonly RoleService roleService = new RoleService();

        public UsersEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                LoadCombobox();
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = userService.GetByID(Convert.ToInt32(Request.QueryString["obj"]));
                    if (obj != null)
                    {
                        this.txtUserName.Text = obj.Username;
                        this.txtFullName.Text = obj.FullName;
                        this.txtEmail.Text = obj.Email;
                        ddlRoles.SelectedValue = obj.RoleId.ToString();
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void LoadCombobox()
        {
            var roles = this.roleService.GetAll();
            roles.Insert(0, new Role() { Id = 0, Name = "Select group" });
            this.ddlRoles.DataSource = roles;
            this.ddlRoles.DataValueField = "Id";
            this.ddlRoles.DataTextField = "Name";
            this.ddlRoles.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var AreaId = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.userService.GetByID(AreaId);
                    if (obj != null)
                    {
                        obj.Username = this.txtUserName.Text.Trim();
                        int role;
                        int.TryParse(this.ddlRoles.SelectedValue,out role);
                        obj.RoleId = role;
                        obj.Password = Utility.GetMd5Hash(GlobalVariables.Current.DefaultPasswordForNewUser);
                        obj.FullName = this.txtFullName.Text.Trim();
                        obj.Email = this.txtEmail.Text.Trim();
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        this.userService.Update(obj);
                    }
                }
                else
                {
                    var obj = new User();
                    obj.Username = this.txtUserName.Text.Trim();
                    int role;
                    int.TryParse(this.ddlRoles.SelectedValue, out role);
                    obj.RoleId = role;
                    obj.Password = Utility.GetMd5Hash(GlobalVariables.Current.DefaultPasswordForNewUser);
                    obj.FullName = this.txtFullName.Text.Trim();
                    obj.Email = this.txtEmail.Text.Trim();
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    this.userService.Insert(obj);
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }
    }
}