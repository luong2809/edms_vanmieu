﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FolderPermissionForm.aspx.cs" Inherits="EDMs.Controls.Pages.FolderPermissionForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow-x: hidden;
            overflow-y: auto;
        }


        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }

        div.qlcbFormItem select.min25Percent {
            max-width: 500px !important;
        }

        .accordion dt a {
            /*letter-spacing: 0.1em;*/
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 1px solid #46A3D3;
            font-size: 1.0em;
            font-weight: bold;
            /*letter-spacing: 0.1em;*/
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .CellTable {
            color: #343434;
            border-bottom: 1px solid #343434;
            font-size: 1.0em;
            font-weight: bold;
            /*letter-spacing: 0.1em;*/
        }

        .icon-grid {
            height: 24px !important;
            width: 24px !important;
        }
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshTreeFolder(args);
            GetRadWindow().close();
        }

        function CloseAndRebindGrid(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" Skin="Silk" MinDisplayTime="1000" />

        <div style="width: 100%">
            <ul style="list-style-type: none">
                <div class="qlcbFormItem">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <asp:Label ID="lblFolderDirName" runat="server"></asp:Label>
                            </p>
                            <br />
                            <div runat="server" id="CreatedInfo" visible="False">
                                <p class="dnnFormRequired" style="float: left;">
                                    <asp:Label ID="lblCreated" runat="server"></asp:Label>
                                    <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </ul>
        </div>
        <div style="width: 100%" runat="server" id="divContent">
            <ul style="list-style-type: none">

                <asp:Table ID="tb2" runat="server" Width="100%">
                    <asp:TableRow ID="tb2r2" Width="100%">
                        <asp:TableCell ID="tb2r2c1" Width="350px" Text="Select Object" CssClass="CellTable"></asp:TableCell>
                        <asp:TableCell ID="tb2r2c2" Width="600px" Text="Select Permission" CssClass="CellTable"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="tb2r1" Width="100%">

                        <asp:TableCell ID="tb2r1c1" Width="350px">
                            <%-- <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>Select Object</span>
                    </dt>
                </dl>--%>
                            <li style="width: 300px;">
                                <div>
                                    <label style="width: 5px; float: left; padding-top: 5px;text-align: right;">
                                        <span style="color: #2E5689; text-align: right;"></span>
                                    </label>
                                    <div style="float: left; padding-top: 5px; padding-left: 6px;" class="qlcbFormItem">
                                        <asp:CheckBox ID="cbObjectGroup" runat="server" Text="Groups List" Value="1" AutoPostBack="true" OnCheckedChanged="cbObjectGroup_CheckedChanged" />
                                        <asp:CheckBox ID="cbObjectUser" runat="server" Text="Users List" Value="2" AutoPostBack="true" OnCheckedChanged="cbObjectUser_CheckedChanged" />
                                    </div>
                                </div>

                                <div style="clear: both; font-size: 0;"></div>
                            </li>
                            <li style="width: 300px;" runat="server" id="liGroup">
                                <div>
                                    <label style="width: 5px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                        <span style="color: #2E5689; text-align: right;"></span>
                                    </label>
                                    <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                        <%-- <asp:DropDownList ID="ddlGroup" runat="server" CssClass="min25Percent" Width="400px"/>--%>
                                        <telerik:RadListBox RenderMode="Lightweight" Skin="Windows7" ID="rgListGroup" runat="server" CssClass="min25Percent" CheckBoxes="true" ShowCheckAll="false" Width="250px"
                                            Height="250px">
                                        </telerik:RadListBox>
                                    </div>
                                </div>
                                <div style="clear: both; font-size: 0;"></div>
                            </li>

                            <li style="width: 300px;" runat="server" id="liUser">
                                <div>
                                    <label style="width: 5px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                        <span style="color: #2E5689; text-align: right;"></span>
                                    </label>
                                    <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                        <%--<asp:DropDownList ID="ddlUser" runat="server" CssClass="min25Percent" Width="400px" />--%>
                                        <telerik:RadListBox RenderMode="Lightweight" Skin="Windows7" ID="rgListUser" runat="server" CssClass="min25Percent" CheckBoxes="true" ShowCheckAll="false" Width="250px"
                                            Height="250px">
                                        </telerik:RadListBox>
                                    </div>
                                </div>
                                <div style="clear: both; font-size: 0;"></div>
                            </li>

                        </asp:TableCell>

                        <asp:TableCell ID="tb2r1c2" Width="600px">
                            <%-- <dl class="accordion">
                    <dt style="width: 100%;">
                        <span>Select Permission</span>
                    </dt>
                </dl>--%>
                            <asp:Table Width="600px" runat="server" ID="tb">
                                <asp:TableRow ID="tb1r2" Width="100%">
                                    <asp:TableCell ID="tb1r2c1" ColumnSpan="3">
                                        <li style="width: 500px;">
                                            <div>
                                                <label style="width: 10px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                    <span style="color: #2E5689; text-align: right;"></span>
                                                </label>
                                                <div style="float: left; padding-top: 5px; padding-left: 6px;" class="qlcbFormItem">
                                                    <asp:CheckBox ID="cbApplyAll" runat="server" Text="Apply for all subfolder" Checked="True" />
                                                </div>
                                            </div>
                                            <div style="clear: both; font-size: 0;"></div>
                                        </li>
                                    </asp:TableCell>

                                </asp:TableRow>
                                <asp:TableRow ID="r1" Width="100%">
                                    <asp:TableCell ID="Hiden" Width="8%"> <label style="width: 10px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">
                            </span>
                        </label></asp:TableCell>
                                    <asp:TableCell ID="c1" Width="46%">
                                        <li style="width: 250px;">
                                            <div>
                                                <label style="width: 50px; float: left; padding-top: 5px; padding-right: 5px; text-align: right;">
                                                    <span style="color: #343434; font-size: large; font-weight: bold; text-align: right; border-bottom-width: 2px; border-bottom-color: #343434; border-bottom-style: solid;">Folder
                                                    </span>
                                                </label>
                                                <div style="float: left; padding-top: 5px; padding-left: 6px;" class="qlcbFormItem">
                                                    <div>
                                                        <asp:CheckBox ID="cbFull" runat="server" Text="Full Control" Value="0" AutoPostBack="true" OnCheckedChanged="cbFull_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cbChangepermission" runat="server" Text="Change permissions" Value="1"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cbCreate" runat="server" Text="Create subfolders" Value="2" AutoPostBack="true" OnCheckedChanged="cbCreate_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cbDelete" runat="server" Text="Delete" Value="3" AutoPostBack="true" OnCheckedChanged="cbDelete_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cbRead" runat="server" Text="Read" Value="4"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cbWrite" runat="server" Text="Write" Value="5" AutoPostBack="true" OnCheckedChanged="cbWrite_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="clear: both; font-size: 0;"></div>
                                        </li>
                                    </asp:TableCell>
                                    <asp:TableCell ID="c2" Width="46%" Visible="false">
                                        <li style="width: 250px; border-left-width: 2px; border-left-color: dodgerblue; border-left-style: solid;">
                                            <div>
                                                <label style="width: 50px; float: left; padding-top: 5px; padding-right: 5px; text-align: right;">
                                                    <span style="color: #2E5689; font-size: large; font-weight: bold; text-align: right; border-bottom-width: 2px; border-bottom-color: dodgerblue; border-bottom-style: solid;">File
                                                    </span>
                                                </label>
                                                <div style="float: left; padding-top: 5px; padding-left: 6px;" class="qlcbFormItem">
                                                    <div>
                                                        <asp:CheckBox ID="cb_filefull" runat="server" Text="Full Control" Value="0" AutoPostBack="true" OnCheckedChanged="cb_filefull_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cb_filechange" runat="server" Text="Change permissions" Value="1" AutoPostBack="true" OnCheckedChanged="cb_filechange_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cb_fileCreate" runat="server" Text="Create file" Value="2" AutoPostBack="true" OnCheckedChanged="cb_fileCreate_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cb_Filedelete" runat="server" Text="Delete file" Value="3" AutoPostBack="true" OnCheckedChanged="cb_Filedelete_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cb_fileRead" runat="server" Text="Read file" Value="4" AutoPostBack="true" OnCheckedChanged="cb_fileRead_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cb_fileWrite" runat="server" Text="Write file" Value="5" AutoPostBack="true" OnCheckedChanged="cb_fileWrite_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                    <div>
                                                        <asp:CheckBox ID="cb_fileNoAccess" runat="server" Text="No Access" Value="6" AutoPostBack="true" OnCheckedChanged="cb_fileNoAccess_CheckedChanged"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="clear: both; font-size: 0;"></div>
                                        </li>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>

                            <li style="width: 500px; padding-top: 10px; padding-bottom: 3px; text-align: center">
                                <telerik:RadButton ID="btnSave" runat="server" Text="Add Permission" OnClick="btnSave_Click"
                                    Width="135" Style="text-align: center">
                                    <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                                </telerik:RadButton>
                            </li>


                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>



                <li style="width: 500px;"></li>

            </ul>
        </div>
        <div style="width: 100%">
            <telerik:RadGrid ID="grdPermission"
                runat="server" AllowPaging="True" AutoGenerateColumns="False" CellSpacing="0" CellPadding="0"
                PageSize="50" Height="350" GridLines="None"
                Skin="Windows7"
                OnNeedDataSource="grdPermission_OnNeedDataSource"
                OnDeleteCommand="grdPermission_OnDeteleCommand">
                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                <MasterTableView DataKeyNames="ID" ClientDataKeyNames="ID" Width="100%">
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; records." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ColumnGroups>
                        <telerik:GridColumnGroup HeaderText="Folder Security" HeaderStyle-ForeColor="#343434" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" Name="folder"
                            HeaderStyle-HorizontalAlign="Center" />
                        <telerik:GridColumnGroup HeaderText="File Security" Name="file" HeaderStyle-ForeColor="#343434" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0"
                            HeaderStyle-HorizontalAlign="Center" />
                    </ColumnGroups>
                    <Columns>
                        <%-- <telerik:GridBoundColumn DataField="IsGroup" UniqueName="IsGroup" Display="False" />--%>
                        <telerik:GridBoundColumn DataField="TypeID" UniqueName="TypeID" Display="False" />
                        <telerik:GridButtonColumn UniqueName="DeleteButton" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete-do.svg" ButtonCssClass="icon-grid">
                            <HeaderStyle Width="50px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridButtonColumn>
                        <telerik:GridTemplateColumn AllowFiltering="false" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" UniqueName="Icon">
                            <HeaderStyle Width="30" />
                            <ItemStyle HorizontalAlign="Center" Width="30" />
                            <ItemTemplate>
                                <div runat="server" id="ViewForFolder" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "TypeID"))==1 %>'>
                                    <asp:Image ID="Image13" runat="server" ToolTip=" Group" Width="24px" Height="24px"
                                        ImageUrl="../../Images/group-do.svg" />
                                </div>
                                <div runat="server" id="Div1" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "TypeID"))==2 %>'>
                                    <asp:Image ID="Image14" runat="server" ToolTip=" User " Width="24px" Height="24px"
                                        ImageUrl="../../Images/user-do.svg" />
                                </div>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="ObjectIdName" HeaderText="Groups/Users" UniqueName="colName" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0"
                            ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle Width="55%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="55%" HorizontalAlign="Left"></ItemStyle>
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn HeaderText="Full Control" UniqueName="Folder_IsFullPermission" ColumnGroupName="folder" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" AllowFiltering="False">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_IsFullPermission"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_IsFullPermission")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Change Permission" UniqueName="Folder_ChangePermission" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" ColumnGroupName="folder" AllowFiltering="False">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image2" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_ChangePermission"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_ChangePermission")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Create SubFolder" UniqueName="Folder_CreateSubFolder" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" ColumnGroupName="folder" AllowFiltering="False">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image3" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_CreateSubFolder"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_CreateSubFolder")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Delete" UniqueName="Folder_Delete" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" ColumnGroupName="folder" AllowFiltering="False">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image4" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_Delete"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_Delete")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Read" UniqueName="Folder_Read" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" ColumnGroupName="folder" AllowFiltering="False">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image5" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_Read"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_Read")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Write" UniqueName="Folder_Write" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#c0c0c0" HeaderStyle-BorderColor="#c0c0c0" ColumnGroupName="folder" AllowFiltering="False">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image6" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_Write"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Folder_Write")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Full Control" UniqueName="File_FullPermission" HeaderStyle-ForeColor="#343434" HeaderStyle-BackColor="#99ff99" HeaderStyle-BorderColor="#99ff99" ColumnGroupName="file" AllowFiltering="False" Visible="false">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image7" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_FullPermission"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_FullPermission")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Change Permission" UniqueName="File_ChangePermission" HeaderStyle-BackColor="#99ff99" HeaderStyle-BorderColor="#99ff99" ColumnGroupName="file" AllowFiltering="False" Visible="false">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image8" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_ChangePermission"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_ChangePermission")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Create" UniqueName="File_Create" HeaderStyle-BackColor="#99ff99" HeaderStyle-BorderColor="#99ff99" ColumnGroupName="file" AllowFiltering="False" Visible="false">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image9" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Create"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Create")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Read" UniqueName="File_Read" HeaderStyle-BackColor="#99ff99" HeaderStyle-BorderColor="#99ff99" ColumnGroupName="file" AllowFiltering="False" Visible="false">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image10" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Read"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Read")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Delete" UniqueName="File_Delete" HeaderStyle-BackColor="#99ff99" HeaderStyle-BorderColor="#99ff99" ColumnGroupName="file" AllowFiltering="False" Visible="false">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image11" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Delete"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Delete")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Write" UniqueName="Folder_Write" HeaderStyle-BackColor="#99ff99" HeaderStyle-BorderColor="#99ff99" ColumnGroupName="file" AllowFiltering="False" Visible="false">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image12" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Write"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_Write")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="No Access" UniqueName="File_NoAccess" HeaderStyle-BackColor="#99ff99" HeaderStyle-BorderColor="#99ff99" ColumnGroupName="file" AllowFiltering="False" Visible="false">
                            <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Image ID="Image15" runat="server"
                                    Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_NoAccess"))%>'
                                    ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "File_NoAccess")) ? "~/Images/ok.png" : "" %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <CommandItemStyle Height="25px"></CommandItemStyle>
                </MasterTableView>
                <ClientSettings>
                    <Selecting AllowRowSelect="true"></Selecting>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>

        </div>

        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument" DefaultLoadingPanelID="RadAjaxLoadingPanel2">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ddlGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdPermission">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" />
                        <telerik:AjaxUpdatedControl ControlID="divContent" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" />
                        <telerik:AjaxUpdatedControl ControlID="divContent" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="cbObjectGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="liGroup" />
                        <telerik:AjaxUpdatedControl ControlID="liUser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="cbObjectUser">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="liGroup" />
                        <telerik:AjaxUpdatedControl ControlID="liUser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <%-- <telerik:AjaxSetting AjaxControlID="cbFull">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="cbWrite">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>



                    <telerik:AjaxSetting AjaxControlID="cb_filefull">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                    <telerik:AjaxSetting AjaxControlID="cb_filechange">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                    <telerik:AjaxSetting AjaxControlID="cb_fileCreate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                    <telerik:AjaxSetting AjaxControlID="cb_fileRead">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                    <telerik:AjaxSetting AjaxControlID="cb_Filedelete">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                    <telerik:AjaxSetting AjaxControlID="cb_fileNoAccess">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tb" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;

                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }

            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
