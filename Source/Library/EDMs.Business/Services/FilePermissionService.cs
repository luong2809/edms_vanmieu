﻿
namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;
   public class FilePermissionService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly FilePermissionDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilePermissionService"/> class.
        /// </summary>
        public FilePermissionService()
        {
            this.repo = new  FilePermissionDAO();
        }
        public List<FilePermission> GetAllByDocumrnt(int documentID)
        {
            return this.repo.GetAllByFolder(documentID);
        }
        public List<FilePermission> GetByObjectId(int objectId, int type)
        {
            return this.repo.GetByObjectId(objectId, type);
        }
        public List<FilePermission> GetAllByDocumrnt(int DocumentId, int type)
        {
            return this.repo.GetAllByDocument(DocumentId, type);
        }
        public List<FilePermission> GetByRoleIdList(List<int> roleId, int documentID)
        {
            return this.repo.GetByRoleIdList(roleId, documentID);
        }
        public List<FilePermission> GetByRoleIdListCategory(List<int> roleId, int categoryId)
        {
            return this.repo.GetByRoleIdListCategory(roleId, categoryId);
        }
        /// <summary>
        /// The get by role id.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="FilePermission"/>.
        /// </returns>
        public List<FilePermission> GetByRoleId(int roleId)
        {
            return this.repo.GetByRoleId(roleId);
        }
        public List<FilePermission> GetByRoleId(int roleId, int categoryId)
        {
            return this.repo.GetByRoleId(roleId, categoryId);
        }
        public List<FilePermission> GetByUserId(int userid)
        {
            return this.repo.GetByUserId(userid);
        }
        public List<FilePermission> GetByUserId(int userid, int categoryid)
        {
            return this.repo.GetByUserId(userid, categoryid);
        }
        public List<FilePermission> GetByRoleIdList(int roleId, int categoryId, int DocumentID)
        {
            return this.repo.GetByRoleIdList(roleId, categoryId, DocumentID);
        }
        public List<FilePermission> GetByUserIdList(int UserId, int categoryId, int DocumentID)
        {
            return this.repo.GetByUserIdList(UserId, categoryId, DocumentID);
        }
        public FilePermission GetByUserI(int UserId, int DocumentID)
        {
            return this.repo.GetByUserI(UserId, DocumentID);
        }
        public List<FilePermission> GetByUserIdList(int UserId, int categoryId, List<int> DocumentID)//typeId ==2 is group
        {
            return this.repo.GetByUserIdList( UserId,  categoryId, DocumentID);
        }
        public List<FilePermission> GetByRoleIdList(int roleId, int categoryId, List<int> DocumentID)//typeId ==1 is group
        {
            return this.repo.GetByRoleIdList(roleId, categoryId, DocumentID);
        }

        #region GET (Basic)
        /// <summary>
        /// Get All Categories GetByUserId
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<FilePermission> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public FilePermission GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(FilePermission bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(FilePermission bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(FilePermission bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}

