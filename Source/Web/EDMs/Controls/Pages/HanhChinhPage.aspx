﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site_VM_Rev1.Master" AutoEventWireup="true" CodeBehind="HanhChinhPage.aspx.cs" Inherits="EDMs.Controls.Pages.HanhChinhPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <style>
        .span-page {
            font-size: 28px;
            color: #B51F09;
        }

        .div-tool {
            height: 70px;
            padding-right: 15px;
        }

        .RadToolBar .rtbChoiceArrow {
            width: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap {
            background-color: transparent !important;
            border: 0 !important;
        }

        .RadToolBarDropDown_Bootstrap {
            color: #B51F09 !important;
        }

        .RadToolBar_Bootstrap .rtbOuter {
            background-color: transparent !important;
            border: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap .rtbWrap {
            padding: 6px 10px !important;
        }

        .RadToolBar .rtbIcon {
            width: 27px;
            height: 27px;
        }

        .RadToolBarDropDown_Bootstrap .rtbIcon {
            width: 16px;
            top: 4px !important;
        }

        .RadButton_Default.rbButton {
            background-color: #4E0B0B !important;
            color: #fff !important;
            padding: 0 10px !important;
            width: 100px;
            background-image: none !important;
        }

        .RadGrid_Silk .rgSelectedRow {
            background-color: #FAF9EF !important;
            color: #252525 !important;
        }

        .icon-grid {
            height: 24px !important;
            width: 24px !important;
        }

        #ctl00_contentTool_ctl00_contentBody_grdDocumentPanel {
            height: 100% !important;
        }

        .tableProperties {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            background-color:#fff;
        }

        .tdProperties, .thProperties {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 5px;
        }

        .tdProperties {
            font-size: 8pt;
        }

        .thProperties {
            font-size: 8.5pt;
        }

        .RadSplitter .rspSlideHeader .rspSlideTitle {
            display: none !important;
        }

        .rspSlideHeaderUndockIcon {
            display: none !important;
        }

        .RadSplitter_Default .rspTabsContainer {
            display: none !important;
        }

        .RadToolBar_Default_Horizontal .rtbOuter {
            background-image: none !important;
        }
        .RadGrid_Silk .rgHeader{
            font-size:12px!important;
        }

        .RadPanelBar_Default a.rpHovered{
            background-image:unset!important;
            background-position:unset!important;
        }
        .btn_folder{
           border:0!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contentMenu" runat="server">
    <telerik:RadPanelBar ID="paneMenu" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="MENU" Value="Menu" runat="server" Width="100%">
                <ContentTemplate>
                    <telerik:RadMenu ID="mainMenu" runat="server" Skin="Bootstrap" RenderMode="Lightweight" Flow="Vertical" Width="100%" Height="100%"></telerik:RadMenu>
                </ContentTemplate>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="radPbCategories" runat="server" Width="100%" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentTool" runat="server">
    <%-- hidden field --%>
    <asp:HiddenField runat="server" ID="lblId" />
    <asp:HiddenField runat="server" ID="lblDocId" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblCategoryId" />
    <asp:HiddenField runat="server" ID="lbFolderSize" />
    <asp:HiddenField runat="server" ID="lbUserId" />
    <asp:HiddenField runat="server" ID="lbRoleId" />
    <asp:HiddenField runat="server" ID="isFolder" />
    <asp:HiddenField runat="server" ID="filePath" />
    <asp:HiddenField runat="server" ID="fileName" />
    <asp:HiddenField runat="server" ID="fileExtensionIcon" />
    <asp:HiddenField runat="server" ID="IsUpdateFolder" />
    <%-- manager code js --%>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $('iframe').load(function () {
                    //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function RowClick(sender, eventArgs) {
                var id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = id;
                var grid = sender;
                var MasterTable = grid.get_masterTableView();
                var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];
                cell = MasterTable.getCellByColumnUniqueName(row, "IsFolder");
                document.getElementById("<%= isFolder.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileName");
                document.getElementById("<%= fileName.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FilePath");
                document.getElementById("<%= filePath.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileExtensionIcon");
                document.getElementById("<%= fileExtensionIcon.ClientID %>").value = cell.innerHTML;
             }

             function RowSelected(sender, eventArgs) {
                 var Id = eventArgs.getDataKeyValue("ID");
                 document.getElementById("<%= lblDocId.ClientID %>").value = Id;
                var grid = sender;
                var MasterTable = grid.get_masterTableView();
                var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];

                cell = MasterTable.getCellByColumnUniqueName(row, "IsFolder");
                document.getElementById("<%= isFolder.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileName");
                document.getElementById("<%= fileName.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FilePath");
                document.getElementById("<%= filePath.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileExtensionIcon");
                document.getElementById("<%= fileExtensionIcon.ClientID %>").value = cell.innerHTML;
                //ajaxManager.ajaxRequest("RowClick_" + Id);
            }

            function ShowChangePerssion() {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var owd = $find("<%=FolderPermission.ClientID %>");
                owd.Show();
                owd.setUrl("FolderPermissionForm.aspx?folderId=" + selectedFolder, "FolderPermission");
            }

            function MoveFolder() {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var owd = $find("<%=MoveFolder.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/MoveFileAndFolder.aspx?mode=1&folderId=" + selectedFolder, "MoveFolder");
            }
            function PermissionDocument() {
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var owd = $find("<%=DocPermission.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/FilePermissionForm.aspx?DocID=" + docId, "DocPermission");
            }
            function FileDownload() {
                var fileName = document.getElementById("<%= fileName.ClientID %>").value;
                var filePath = document.getElementById("<%= filePath.ClientID %>").value;
                var link = document.createElement("a");
                link.download = fileName;
                link.href = filePath;
                link.click();
                link.remove();
            }
            function FileView() {
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var extensionName = document.getElementById("<%= fileExtensionIcon.ClientID %>").value;
                if (extensionName == "images/pdffile.png") {
                    ShowViewerFile(docId);
                }
                else if (extensionName == "images/video.png") {
                    ShowVideoForm(docId);
                }
                else if (extensionName == "images/picture.png" || extensionName == "images/bmp.png" || extensionName == "images/gif_file.png") {
                    ShowImageForm(docId);
                }
            }
            function FileCheckout() {
                var fileName = document.getElementById("<%= fileName.ClientID %>").value;
                var filePath = document.getElementById("<%= filePath.ClientID %>").value;
                var link = document.createElement("a");
                link.download = fileName;
                link.href = filePath;
                link.click();
                link.remove();
                //ajaxManager.ajaxRequest("CheckoutDoc");
            }

            function FileDuplicateRevision() {
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/RetrieveDocumentForm.aspx?docId=" + docId, "CustomerDialog");
            }

            function OnClientShow(sender, args) {
                if (!sender.isPinned())
                    sender.togglePin();
            }

            function ShowViewerFile(Id) {

                // if (path.toLowerCase().search(".pdf")) {
                var owd = $find("<%= ViewerFile.ClientID %>");
                owd.Show();
                owd.moveTo(120, 60);
                owd.setUrl("ViewerFilePDF.aspx?DocId=" + Id + "&File=0", "ViewerFile");
            }

            function ShowVideoForm(id) {
                var owd = $find("<%=VideoView.ClientID %>");
                owd.Show();
                owd.setUrl("ViewVideoFile.aspx?docId=" + id, "VideoView");
            }
            function ShowImageForm(id) {
                var owd = $find("<%=ImageView.ClientID %>");
                owd.Show();
                owd.maximize(true);
                owd.setUrl("ViewImage.aspx?docId=" + id, "ImageView");
            }

            function refreshGridAndFolder(arg) {
                var idfolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    ajaxManager.ajaxRequest("ReloadFolder_" + idfolder);
                    if (arg != '') {
                        $find("<%= ajaxCustomer.ClientID %>").ajaxRequest(radTreeFolder);
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
            }

            function btDownloadfolder() {
                // $('#loading').show();
                var foldersize = parseFloat(document.getElementById("<%= lbFolderSize.ClientID %>").value) / 19;
                //alert(foldersize);
                document.getElementById("loading").style.display = "block";
                setTimeout(function () {
                    //window.location.reload(1);
                    document.getElementById("loading").style.display = "none";
                }, 1000 * foldersize);
            }
            function DownloadFolder() {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var userid = document.getElementById("<%= lbUserId.ClientID %>").value;
                var roleid = document.getElementById("<%= lbRoleId.ClientID %>").value;
                <%--var owd = $find("<%=DownloadFolder .ClientID %>");
                owd.Show(); owd.moveTo(screen.width - 250, screen.height - 300);
                owd.setUrl("DownloadFolder.aspx?selectedFolder=" + selectedFolder, "DownloadFolder");--%>
                //$.ajax({
                //    type: 'POST',
                //    url: "DownloadFolder.aspx",
                //    dataType: "text",
                //    data: { 'type': 2, 'selectedFolder': selectedFolder, 'userId': userid, 'roleId': roleid },
                //    beforeSend: function (xmlHttpRequest) {
                //       // alert("Before Sending.");
                //        $('#loading').show();
                //    },
                //    statusCode: function (data) {
                //        alert(data);
                //    },
                //    complete: function (data) {
                //        if (data.indexOf("Ok")>=0) {
                //                $('#loading').hide();
                //        } else {
                //                 alert(data);
                //        }      
                //    },
                //    success: function (data) {
                //        if (data.indexOf("Ok") >= 0) {
                //            $('#loading').hide();
                //        } else {
                //            alert(data);
                //        } 
                //    }
                //});
                var postdata = "selectedFolder=" + selectedFolder
                var xhr = new XMLHttpRequest();
                $('#loading').show();
                xhr.open('POST', 'DownloadFolder.aspx?' + postdata, false);
                xhr.onreadystatechange = function () {

                    if (this.readyState == 4 && this.status == 200) {

                        if (this.responseText.substring(0, 2) == "Ok") {
                            // //var st = this.responseText.split('$')[1];
                            // // alert(st);
                            $('#loading').hide();
                            //// ajaxManager.ajaxRequest("DowloadZipFile_" + selectedFolder);
                        }
                    }
                };
                xhr.send(postdata);
            }

            function ShowEditForm(id) {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("BiaEditPage.aspx?obj=" + id, "EditPage");
            }

            function ShowAddForm() {
                var folId = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("AttachFile.aspx?folId=" + folId, "AttachFile");
            }

            function onNodeClicking(sender, args) {
                var folderValue = args.get_node().get_value();
                document.getElementById("<%= lblFolderId.ClientID %>").value = folderValue;
            }

            function OnClientBeforeClose(sender, args) {
                var oWnd = $find("<%= RadWindowManager1.ClientID %>").getWindowByName("VideoView");
                oWnd.Reload();
            }

            function refreshGrid(arg) {
                if (!arg) {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("Rebind");
                }
                else {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("RebindAndNavigate");
                }
            }

            //expand dropdown left
            function dropdownOpened(sender, args) {
                var dropDownElem = args.get_item().get_element();
                var rightPos = $telerik.getLocation(dropDownElem).x + $telerik.getSize(dropDownElem).width;
                var dropDownElemContainer = args.get_item()._animationContainer;
                var leftPos = rightPos - dropDownElemContainer.clientWidth;
                dropDownElemContainer.style.left = leftPos + "px";
            }
            //disable ajax control
            function onRequestStart(sender, args) {
                //alert(args.get_eventArgument());
                if (args.get_eventArgument().indexOf("DownloadMulti") >= 0 || args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("btDownloadfolder") >= 0) {
                    args.set_enableAjax(false);

                }
            }

            function StandardConfirm(sender, args) {
                args.set_cancel(!window.confirm("Do you want delete file?"));
            }

            function ReloadWindow() {
                window.location.reload();
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function rtvExplore_OnNodeExpandedCollapsed(sender, eventArgs) {
                var allNodes = eventArgs._node.get_treeView().get_allNodes();
                var i;
                var selectedNodes = "";

                for (i = 0; i < allNodes.length; i++) {
                    if (allNodes[i].get_expanded())
                        selectedNodes += allNodes[i].get_value() + "*";
                }
                Set_Cookie("expandedNodes", selectedNodes, 30);
            }

            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());
                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                ((path) ? ";path=" + path : "") +
                ((domain) ? ";domain=" + domain : "") +
                ((secure) ? ";secure" : "");
            }
            function StandardConfirm(sender, args) {
                args.set_cancel(!window.confirm("Do you want to delete item?"));
            }
        </script>
    </telerik:RadCodeBlock>

    <%-- manager ajax page --%>
    <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="ajaxCustomer_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CustomerMenu">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radTreeFolder"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radTreeFolder">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="radMenuFolder" />
                    <telerik:AjaxUpdatedControl ControlID="pnFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lbUserId" />
                    <telerik:AjaxUpdatedControl ControlID="lbRoleId" />
                    <telerik:AjaxUpdatedControl ControlID="lbFolderSize" />
                    <telerik:AjaxUpdatedControl ControlID="btnAdd" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="radPbCategories">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radMenuFolder">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdDocument">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="radMenuFile" />
                    <telerik:AjaxUpdatedControl ControlID="pnFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="radMenuFolder" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radMenuFile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadNotification RenderMode="Lightweight" Position="Center" EnableRoundedCorners="true" EnableShadow="true" Skin="Silk" ID="RadNotification1" runat="server" Width="300" Height="120" Animation="Fade">
        <ContentTemplate>
            <div style="margin: 5px 0 0 10px">
                <asp:Literal ID="lblNotification" runat="server"></asp:Literal>
            </div>
        </ContentTemplate>
    </telerik:RadNotification>

    <%-- loading page --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" MinDisplayTime="1000" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>

    <%-- manager window popup --%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditPage" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="570" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ViewerFile" runat="server" Title="Viewer File"
                VisibleStatusbar="false" Height="600" Width="1250" MaxHeight="600" MaxWidth="1250"
                Left="50px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" OnClientShow="OnClientShow">
            </telerik:RadWindow>
            <telerik:RadWindow ID="VideoView" runat="server" Title="Video Viewer" OnClientBeforeClose="OnClientBeforeClose"
                VisibleStatusbar="false" Height="550" Width="800" MaxHeight="650" MaxWidth="950"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" OnClientShow="OnClientShow">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ImageView" runat="server" Title="Image Viewer" VisibleStatusbar="false" Height="550" Width="800" MaxHeight="550" MaxWidth="800"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" OnClientShow="OnClientShow">
            </telerik:RadWindow>
            <telerik:RadWindow ID="MoveFolder" runat="server" Title="Move Files" OnClientClose="ReloadWindow"
                VisibleStatusbar="false" Height="500" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="FolderPermission" runat="server" Title="Folder Permission"
                VisibleStatusbar="false" Height="650" Width="950"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="DocPermission" runat="server" Title="Document Permission"
                VisibleStatusbar="false" Height="650" Width="950"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="row justify-content-between align-items-center">
        <div class="col-lg-4">
            <span class="span-page">TÀI LIỆU HÀNH CHÍNH</span>
        </div>
        <div class="col-lg-auto">
            <div class="row align-items-center">
                <div class="col-lg-auto" style="padding-right: 0">
                    <div class="row align-items-center div-tool">
                        <telerik:RadButton ID="btnAdd" runat="server" Text="TẠO MỚI" RenderMode="Lightweight" Height="40px" Width="130px" AutoPostBack="false" OnClientClicking="ShowAddForm">
                            <Icon PrimaryIconUrl="~/Images/add-trang.svg" PrimaryIconTop="6px" PrimaryIconLeft="8px" PrimaryIconHeight="19px" PrimaryIconWidth="19px" />
                        </telerik:RadButton>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <telerik:RadToolBar ID="CustomerMenu" runat="server" Skin="Bootstrap" Width="100%" Height="40px" OnClientDropDownOpened="dropdownOpened">
                        <Items>
                            <telerik:RadToolBarDropDown ImageUrl="/Images/settings-do.svg" DropDownWidth="150px">
                                <Buttons>
                                </Buttons>
                            </telerik:RadToolBarDropDown>
                        </Items>
                    </telerik:RadToolBar>
                </div>
                <%--<div class="col-lg-auto">
                    <div class="row align-items-center div-tool">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox-search"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn-search div-search" />
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <%-- grid data --%>
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%" Height="100%">
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                <telerik:RadPane ID="Radpane1" runat="server" Scrolling="Both" Width="270" MinWidth="270">
                    <telerik:RadToolBar ID="radMenuFolder" runat="server" Height="30px" Width="100%">
                        <Items>
                            <telerik:RadToolBarButton runat="server" Value="ShowAll">
                                <ItemTemplate>
                                    <telerik:RadButton ID="btnAddFolder" runat="server"  Width="30px" ToolTip="New folder" Style="text-align: center; margin-left: 5px;" CssClass="btn_folder" Visible="false">
                                        <Icon PrimaryIconUrl="/Images/folder-add-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                    </telerik:RadButton>

                                    <telerik:RadImageButton ID="RadImageButton1" runat="server" OnClick="btnAddFolder_Click" Image-Url="/Images/folder-add-do.svg" Width="40px" Height="30px" ToolTip="New folder"></telerik:RadImageButton>

                                    <telerik:RadButton ID="btrename" runat="server" OnClick="btrename_Click" Width="30px" ToolTip="Rename Folder" Style="text-align: center; margin-left: 5px;" Visible="false">
                                        <Icon PrimaryIconUrl="/Images/folder-rename-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                    </telerik:RadButton>

                                    <telerik:RadImageButton ID="RadImageButton2" runat="server" OnClick="btrename_Click" Image-Url="/Images/folder-rename-do.svg" Width="40px" Height="30px" ToolTip="Rename Folder"></telerik:RadImageButton>
                                    
                                    <telerik:RadButton ID="btDelete" runat="server" OnClientClicking="StandardConfirm" OnClick="btDelete_Click" Width="30px" ToolTip="Delete Folder" Style="text-align: center; margin-left: 5px;" Visible="false">
                                        <Icon PrimaryIconUrl="/Images/delete-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                    </telerik:RadButton>

                                    <telerik:RadImageButton ID="RadImageButton3" runat="server" OnClientClicking="StandardConfirm" OnClick="btDelete_Click" Image-Url="/Images/delete-do.svg" Width="40px" Height="30px" ToolTip="Delete Folder"></telerik:RadImageButton>
                                    
                                    <telerik:RadButton ID="btpermission" runat="server" OnClientClicked="ShowChangePerssion" Width="30px" ToolTip="Change Folder Permission" Style="text-align: center; margin-left: 5px;" Visible="false">
                                        <Icon PrimaryIconUrl="/Images/folder-permission-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                    </telerik:RadButton>
                                    
                                    <telerik:RadImageButton ID="RadImageButton4" runat="server" OnClientClicked="ShowChangePerssion" Image-Url="/Images/folder-permission-do.svg" Width="40px" Height="30px" ToolTip="Change Folder Permission"></telerik:RadImageButton>
                                    
                                    <telerik:RadButton ID="btmove" runat="server" OnClientClicked="MoveFolder" Width="30px" ToolTip="Move Folder" Style="text-align: center; margin-left: 5px;" Visible="false">
                                        <Icon PrimaryIconUrl="/Images/iconfinder_Open.png" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                    </telerik:RadButton>
                                    
                                    <telerik:RadButton ID="btDownloadfolder" runat="server" OnClick="btDownloadfolder_Click" OnClientClicking="btDownloadfolder" Width="30px" ToolTip="Download Folder" Style="text-align: center; margin-left: 5px;" Visible="false">
                                        <Icon PrimaryIconUrl="/Images/folder-downloads-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                    </telerik:RadButton>

                                    <telerik:RadImageButton ID="RadImageButton5" runat="server" OnClientClicking="btDownloadfolder" OnClick="btDownloadfolder_Click" Image-Url="/Images/folder-downloads-do.svg" Width="40px" Height="30px" ToolTip="Download Folder" Visible="false"></telerik:RadImageButton>
                                </ItemTemplate>
                            </telerik:RadToolBarButton>
                        </Items>
                    </telerik:RadToolBar>
                    <telerik:RadSlidingZone ID="SlidingZone1" runat="server" DockedPaneId="RadSlidingPane1">
                        <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server"
                            Title="Folder Tree" Width="320" Height="100%">
                            <telerik:RadTreeView ID="radTreeFolder" runat="server" Width="100%" Height="100%" ShowLineImages="False"
                                OnNodeEdit="radTreeFolder_NodeEdit"
                                OnNodeClick="radTreeFolder_NodeClick"
                                OnNodeDataBound="radTreeFolder_OnNodeDataBound"
                                OnClientNodeExpanded="rtvExplore_OnNodeExpandedCollapsed"
                                OnClientNodeCollapsed="rtvExplore_OnNodeExpandedCollapsed"
                                OnNodeExpand="radTreeFolder_NodeExpand"
                                OnClientNodeClicking="onNodeClicking">
                                <DataBindings>
                                    <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
                                </DataBindings>
                            </telerik:RadTreeView>
                        </telerik:RadSlidingPane>
                    </telerik:RadSlidingZone>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="Radsplitbar1" runat="server">
                </telerik:RadSplitBar>
                <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                    <telerik:RadSplitter ID="Radsplitter1" runat="server" Orientation="Horizontal">
                        <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None" Height="82%">
                            <%--<telerik:RadToolBar ID="radMenuFile" runat="server" Height="30px" Width="100%" Enabled="false">
                                <Items>
                                    <telerik:RadToolBarButton runat="server" Value="ShowAll">
                                        <ItemTemplate>
                                            <telerik:RadButton ID="btnEdit" runat="server" OnClientClicked="ShowEditForm" Width="30px" ToolTip="Edit File" Style="text-align: center; margin-left: 5px;" Visible="false">
                                                <Icon PrimaryIconUrl="/Images/edit-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileDelete" runat="server" OnClientClicking="StandardConfirm" OnClick="btnFileDelete_Click" Width="30px" ToolTip="Delete File" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="/Images/delete-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileDownload" runat="server" OnClientClicked="FileDownload" Width="30px" ToolTip="Download File" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="/Images/download-do.svg" PrimaryIconLeft="2" PrimaryIconTop="-1" PrimaryIconWidth="24" PrimaryIconHeight="24" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileView" runat="server" OnClientClicked="FileView" Width="30px" ToolTip="View File" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="/Images/expview.png" PrimaryIconLeft="7" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </ItemTemplate>
                                    </telerik:RadToolBarButton>
                                </Items>
                            </telerik:RadToolBar>--%>
                            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True" Skin="Silk" RenderMode="Lightweight"
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                                GridLines="None" Height="100%" AllowMultiRowSelection="True"
                                OnDeleteCommand="grdDocument_DeleteCommand"
                                OnNeedDataSource="grdDocument_OnNeedDataSource"
                                OnItemCommand="grdDocument_ItemCommand"
                                OnItemDataBound="grdDocument_ItemDataBound"
                                OnPreRender="grdDocument_PreRender"
                                PageSize="100" Style="outline: none">
                                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" CommandItemDisplay="Top" Font-Size="8pt" AllowFilteringByColumn="true">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false" />
                                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                        <telerik:GridBoundColumn DataField="IsFolder" UniqueName="IsFolder" Display="False" />
                                        <telerik:GridBoundColumn DataField="FilePath" UniqueName="FilePath" Display="False" />
                                        <telerik:GridBoundColumn DataField="FileExtensionIcon" UniqueName="FileExtensionIcon" Display="False" />
                                        <telerik:GridBoundColumn DataField="Name" UniqueName="FileName" Display="False" />
                                        <telerik:GridClientSelectColumn UniqueName="IsSelected">
                                            <HeaderStyle Width="30px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridClientSelectColumn>
                                        <telerik:GridButtonColumn UniqueName="EditButton" ButtonType="ImageButton" ImageUrl="~/Images/edit-do.svg" ButtonCssClass="icon-grid" Visible="false">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridButtonColumn>
                                        <telerik:GridButtonColumn UniqueName="DeleteButton" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete-do.svg" ButtonCssClass="icon-grid">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridButtonColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                            <HeaderStyle Width="30px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewForFolder" visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <asp:Image ID="Image1" runat="server"
                                                        ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' />
                                                </div>
                                                <div runat="server" id="ViewForDoc" visible='<%# (!Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder"))) %>'>
                                                    <div runat="server" id="DocumentFile" visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null %>'>
                                                        <%--<asp:Image ID="CallLink" runat="server"
                                                            ImageUrl=' <%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>'
                                                            ToolTip="Download File"
                                                            Style="cursor: pointer;" AlternateText="Download document" />--%>
                                                        <a download='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                                                            href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' target="_blank">
                                                            <asp:Image ID="GenTrans" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' Style="cursor: pointer;" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="Viewfile">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewFilePDF" visible='<%# (DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null &&  DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/pdffile.png" && !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut"))) %>'>
                                                    <a href='javascript:ShowViewerFile(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="CallLink2" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText="Viewer File PDF" ToolTip="View file" />
                                                        <a />
                                                </div>
                                                <div runat="server" id="VideoFile" visible='<%# (DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null &&  DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/video.png" && !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut"))) %>'>
                                                    <a href='javascript:ShowVideoForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText="Viewer Video" ToolTip="View Video" />
                                                    </a>
                                                </div>
                                                <div runat="server" id="PictureFile" visible='<%# (DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null && DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/picture.png" && !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut"))) %>'>
                                                    <a href='javascript:ShowImageForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)'>
                                                        <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText="Viewer Image" ToolTip="View Image" />
                                                    </a>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn UniqueName="Name" HeaderText="File Name"
                                            DataField="Name" ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle Width="400px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewNameForFolder" visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <a href='#' style="text-decoration: none; color: blue">
                                                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label>
                                                    </a>
                                                </div>
                                                <div runat="server" id="ViewNameForDoc" visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Description" HeaderText="Tiêu đề" UniqueName="Index2" Visible="false"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="300" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True" EnablePostBackOnRowClick="true">
                                    <ClientEvents OnRowClick="RowClick" OnRowSelected="RowSelected" OnGridCreated="GetGridObject" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPane>
                        <telerik:RadSplitBar ID="Radsplitbar2" runat="server">
                        </telerik:RadSplitBar>
                        <telerik:RadPane ID="RadPane5" runat="server" Height="17%">
                            <asp:Panel ID="pnFolder" runat="server">
                                <table class="tableProperties">
                                    <tr>
                                        <th class="thProperties" style="width: 20%">Property name</th>
                                        <th class="thProperties" style="width: 30%">Property value</th>
                                        <th class="thProperties" style="width: 20%">Property name</th>
                                        <th class="thProperties" style="width: 30%">Property value</th>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">Created by</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblCreatedBy" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">Updated by</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblUpdatedBy" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">Created time</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblCreatedTime" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">Updated time</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblUpdatedTime" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">Disk Usage</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblDiskUsage" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">
                                            <asp:Label ID="lblDocumentCount" runat="server" Text="Document Count"></asp:Label></td>
                                        <td class="tdProperties">
                                            <asp:Label ID="txtDocumentCount" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">
                                            <asp:Label ID="lblParentFolder" runat="server" Text="Parent Folder"></asp:Label></td>
                                        <td class="tdProperties">
                                            <asp:Label ID="txtParentFolder" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">
                                            <asp:Label ID="lblFolderCount" runat="server" Text="Folder Count"></asp:Label></td>
                                        <td class="tdProperties">
                                            <asp:Label ID="txtFolderCount" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
