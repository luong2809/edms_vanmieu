﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class FolderService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly FolderDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FolderService"/> class.
        /// </summary>
        public FolderService()
        {
            this.repo = new FolderDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get related folder.
        /// </summary>
        /// <param name="dirname">
        /// The dirname.
        /// </param>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/> folder.
        /// </returns>
        public List<Folder> GetRelatedFolder(string dirname, int folderId)
        {
            return this.repo.GetRelatedFolder(dirname, folderId).Where(t => !t.IsDelete.GetValueOrDefault()).ToList();
        }

        /// <summary>
        /// The get specific folder.
        /// </summary>
        /// <param name="listFolId">
        /// The list fol id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Folder> GetSpecificFolder(List<int> listFolId)
        {
            return this.repo.GetSpecificFolder(listFolId).Where(t => !t.IsDelete.GetValueOrDefault()).ToList();
        }

        public List<Folder> GetSpecificFolderStatic(List<int> listFolId)
        {
            return this.repo.GetSpecificFolderStatic(listFolId).Where(t => !t.IsDelete.GetValueOrDefault()).ToList();
        }

        public List<Folder> GetAllRelatedPermittedFolderItems(List<int> listFolId)
        {
            return this.repo.GetAllRelatedPermittedFolderItems(listFolId).Where(t => !t.IsDelete.GetValueOrDefault()).ToList();
        }

        public List<Folder> GetAllByParentId(int parentId, List<int> listFolId )
        {
            return this.repo.GetAllByParentId(parentId, listFolId).Where(t => !t.IsDelete.GetValueOrDefault()).ToList();
        }
        public List<Folder> GetAllByParentId(int parentId)
        {
            return this.repo.GetAllByParentId(parentId).Where(t => !t.IsDelete.GetValueOrDefault()).ToList();
        }
        public List<Folder> GetAllDeleteByParentId(int parentId)
        {
            return this.repo.GetAllDeleteByParentId(parentId);
        }
        public List<Folder> GetAllSpecificFolder(List<int> listFolId)
        {
            return this.repo.GetAllSpecificFolder(listFolId).Where(t => !t.IsDelete.GetValueOrDefault()).ToList();
        }
        public List<Folder> GetAllMainFolderDelete()
        {
            return this.repo.GetAllMainFolderDelete();
        }

        /// <summary>
        /// The get all by category.
        /// </summary>
        /// <param name="categoryId">
        /// The category id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Folder> GetAllByCategory(int categoryId)
        {
            return this.repo.GetAllByCategory(categoryId).Where(t=> !t.IsDelete.GetValueOrDefault()).ToList();
        } 

        public Folder GetByDirName(string dirName)
        {
            return this.repo.GetByDirName(dirName);
        }

        public Folder GetByName(string Name)
        {
            return this.repo.GetByName(Name);
        }

        //public Folder GetByParentNo(int parentno)
        //{
        //    return this.repo.GetByParentNo(parentno);
        //}

        public Folder GetByParentAndName(int folParent, string folName)
        {
            return this.repo.GetByParentAndName(folParent, folName);
        }

        public Folder GetByParentAndName(string folParent, string folName)
        {
            return this.repo.GetByParentAndName(folParent, folName);
        }

        //public Folder GetByNoAndParent(int no, int parentno)
        //{
        //    return this.repo.GetByNoAndParent(no, parentno);
        //}

        public IEnumerable<Folder> GetAllParentsFolderItemsByChildren(IEnumerable<Folder> children)
        {
            return this.repo.GetAllParentsFolderItemsByChildren(children);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Folder> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Folder GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public string Insert(Folder bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Folder bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Folder bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
