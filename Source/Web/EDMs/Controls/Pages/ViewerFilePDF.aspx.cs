﻿using System;
using System.IO;
using System.Web.UI;
using EDMs.Business.Services;
using System.Web.UI.HtmlControls;

namespace EDMs.Controls.Pages
{
    // using SautinSoft;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ViewerFilePDF : Page
    {
        private readonly TaiLieuTienSiService taiLieuTienSiService;
        //private readonly AttachFilesPackageService attachfilepackageService;
        //private readonly AttachCommentFileService attachcommentfileService;
        public ViewerFilePDF()
        {
            this.taiLieuTienSiService = new TaiLieuTienSiService();
            //this.attachcommentfileService = new AttachCommentFileService();
            //this.attachfilepackageService = new AttachFilesPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["DocId"]))
            {
                var id = Convert.ToInt32(this.Request.QueryString["DocId"]);
                if (this.Request.QueryString["File"] == "0")
                {
                 var docfile = this.taiLieuTienSiService.GetById(id);
                    HtmlGenericControl myObject = new HtmlGenericControl();
                    myObject.TagName = "object";
                    myObject.Attributes.Add("data", docfile.FilePath.ToString());
                    myObject.Attributes.Add("width", "100%");
                    myObject.Attributes.Add("height", "600px");
                    Page.FindControl("Panel1").Controls.Add(myObject);
                //this.pdf.Attributes["src"] = docfile.FilePath.ToString() + "#toolbar=0&navpanes=0";
                }

            } 
            
        }

       
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}