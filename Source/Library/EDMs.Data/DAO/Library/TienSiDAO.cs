﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TienSiDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class TienSiDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TienSiDAO"/> class.
        /// </summary>
        public TienSiDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TienSi> GetIQueryable()
        {
            return EDMsDataContext.TienSis;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TienSi> GetAll()
        {
            return EDMsDataContext.TienSis.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public TienSi GetById(int id)
        {
            return this.EDMsDataContext.TienSis.FirstOrDefault(ob => ob.ID == id);
        }

        public TienSi GetByName(string name)
        {
            return this.EDMsDataContext.TienSis.FirstOrDefault(ob => ob.HoTen == name);
        }

        public List<TienSi> GetByList(List<int> list)
        {
            return EDMsDataContext.TienSis.Where(t => list.Contains(t.ID)).OrderByDescending(t => t.ID).ToList();
        }

        #endregion

        #region GET ADVANCE
        public List<TienSi> GetByBia(int biaID)
        {
            return EDMsDataContext.TienSis.Where(t => t.BiaID == biaID).OrderByDescending(t => t.ID).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(TienSi ob)
        {
            try
            {
                EDMsDataContext.AddToTienSis(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(TienSi src)
        {
            try
            {
                TienSi des = (from rs in this.EDMsDataContext.TienSis
                              where rs.ID == src.ID
                              select rs).First();

                des.HoTen = src.HoTen;
                des.NamSinh = src.NamSinh;
                des.NamMat = src.NamMat;
                des.TieuSu = src.TieuSu;
                des.GiaDinh = src.GiaDinh;
                des.DongHo = src.DongHo;
                des.QueQuan = src.QueQuan;
                des.ThongTinChiTietDiaPhuong = src.ThongTinChiTietDiaPhuong;
                des.ThongTinChiTietVanHoaDiaPhuong = src.ThongTinChiTietVanHoaDiaPhuong;
                des.NamThiDo = src.NamThiDo;
                des.SuNghiep = src.SuNghiep;
                des.TrieuDai = src.TrieuDai;
                des.ChucQuan = src.ChucQuan;
                des.DongGop = src.DongGop;
                des.TacPhamDeLai = src.TacPhamDeLai;
                des.InforgraphicFilePath = src.InforgraphicFilePath;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(TienSi src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
