﻿using System;
using System.Web.UI;
using EDMs.Data.Entities;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Utilities;
using System.IO;
using Telerik.Web.UI;
using System.Web;
using System.Linq;

namespace EDMs.Controls.Systems
{
    public partial class ConstructionEditPage : Page
    {
        private readonly ConstructionTreeService constructionTreeService = new ConstructionTreeService();
        private readonly UserService userService = new UserService();

        public ConstructionEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.txtName.Focus();
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["action"]) && this.Request.QueryString["action"] == "add")
                {
                    var parentObj = this.constructionTreeService.GetById(Convert.ToInt32(this.Request.QueryString["parentId"]));
                    if (parentObj != null)
                    {
                        var rtvParent = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvParent");
                        if (rtvParent != null)
                        {
                            var selectedNode = rtvParent.GetAllNodes().First(t => t.Value == parentObj.ID.ToString());
                            if (selectedNode != null)
                            {
                                rtvParent.GetAllNodes().First(t => t.Value == parentObj.ID.ToString()).Selected = true;
                                this.ExpandParentNode(selectedNode);
                                this.ddlParent.Items[0].Text = selectedNode.Text;
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(this.Request.QueryString["Id"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.constructionTreeService.GetById(Convert.ToInt32(this.Request.QueryString["Id"]));
                    if (obj != null)
                    {
                        this.txtName.Text = obj.Name;
                        this.txtDescription.Text = obj.Description;
                        this.checkActive.Checked = obj.Active.GetValueOrDefault();

                        if (obj.ParentId != null && obj.ParentId != 0)
                        {
                            var rtvParent = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvParent");
                            if (rtvParent != null)
                            {
                                var selectedNode = rtvParent.GetAllNodes().First(t => t.Value == obj.ParentId.ToString());
                                if (selectedNode != null)
                                {
                                    rtvParent.GetAllNodes().First(t => t.Value == obj.ParentId.ToString()).Selected = true;
                                    this.ExpandParentNode(selectedNode);
                                    this.ddlParent.Items[0].Text = selectedNode.Text;
                                }
                            }
                        }

                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        private void LoadComboData()
        {
            var listOptionalTypeDetail = this.constructionTreeService.GetAll();
            var rtvParent = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvParent");
            if (rtvParent != null)
            {
                rtvParent.DataSource = listOptionalTypeDetail;
                rtvParent.DataFieldParentID = "ParentId";
                rtvParent.DataTextField = "Name";
                rtvParent.DataValueField = "ID";
                rtvParent.DataFieldID = "ID";
                rtvParent.DataBind();
            }

            this.RestoreExpandStateTreeView();
        }

        private void ExpandParentNode(RadTreeNode node)
        {
            if (node.ParentNode != null)
            {
                node.ParentNode.Expanded = true;
                this.ExpandParentNode(node.ParentNode);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["Id"]))
                {
                    var optTypeDetailId = Convert.ToInt32(this.Request.QueryString["Id"]);
                    var obj = this.constructionTreeService.GetById(optTypeDetailId);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        var rtvParent = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvParent");
                        if (rtvParent != null && rtvParent.SelectedNode != null)
                        {
                            obj.ParentId = Convert.ToInt32(rtvParent.SelectedNode.Value);
                        }
                        obj.Active = this.checkActive.Checked;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;

                        this.constructionTreeService.Update(obj);
                    }
                }
                else
                {
                    var obj = new ConstructionTree()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        Active = true
                    };
                    var rtvParent = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvParent");
                    if (rtvParent != null && rtvParent.SelectedNode != null)
                    {
                        obj.ParentId = Convert.ToInt32(rtvParent.SelectedNode.Value);
                    }

                    this.constructionTreeService.Insert(obj);
                }

                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }
        protected void rtvParent_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "~/Images/contruction.svg";
        }

        /// <summary>
        /// The restore expand state tree view.
        /// </summary>
        private void RestoreExpandStateTreeView()
        {
            var rtvParent = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvParent");
            if (rtvParent != null)
            {
                // Restore expand state of tree folder
                HttpCookie cookie = this.Request.Cookies["expandedNodesContructionEdit"];
                if (cookie != null)
                {
                    var expandedNodeValues = cookie.Value.Split('*');
                    foreach (var nodeValue in expandedNodeValues)
                    {
                        RadTreeNode expandedNode =
                            rtvParent.FindNodeByValue(HttpUtility.UrlDecode(nodeValue));
                        if (expandedNode != null)
                        {
                            expandedNode.Expanded = true;
                        }
                    }
                }
            }
        }
    }
}