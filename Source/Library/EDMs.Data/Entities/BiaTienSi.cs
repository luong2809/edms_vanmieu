﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Document.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Document type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO;

    /// <summary>
    /// The document.
    /// </summary>
    public partial class BiaTienSi
    {
        public string TenBiaWithYear
        {
            get
            {
                return string.IsNullOrEmpty(this.NamKhoaThi.ToString()) ? this.TenBia : this.TenBia + "-" + this.NamKhoaThi;
            }
        }
    }
}
