﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class TaiLieuHoanCongService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly TaiLieuHoanCongDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaiLieuHoanCongService"/> class.
        /// </summary>
        public TaiLieuHoanCongService()
        {
            this.repo = new TaiLieuHoanCongDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHoanCong> GetSpecific(int tranId)
        {
            return this.repo.GetSpecific(tranId);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<TaiLieuHoanCong> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        public List<TaiLieuHoanCong> GetByConstruction(List<int> listConstruction)
        {
            return this.repo.GetByConstruction(listConstruction);
        }

        /// <summary>
        /// The get all by owner.
        /// </summary>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHoanCong> GetAllByOwner(int createdBy)
        {
            return this.repo.GetAllByOwner(createdBy);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public TaiLieuHoanCong GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(TaiLieuHoanCong bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(TaiLieuHoanCong bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(TaiLieuHoanCong bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion


        public List<TaiLieuHoanCong> GetAllDocList(List<int> ListId)
        {
            return this.repo.GetAll().Where(t => ListId.Contains(t.ID)).ToList();
        }
    }
}
