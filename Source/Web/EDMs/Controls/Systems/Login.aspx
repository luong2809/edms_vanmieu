﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="EDMs.Controls.Systems.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Content/bootstrap.min.css">
    <style>
        html, body, form {
            height: 100%;
            font-family: Roboto, sans-serif;
        }

        .full-background {
            height: 100%;
            background: #E6E0AE;
        }

        .container-fluid {
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .bg-temple {
            background-image: url(../../Images/bgTemple.png);
            background-position: right;
            background-repeat: no-repeat;
            height: 518px;
            padding-right: 0;
        }

        .bg-form-login {
            background-color: #ffffff;
            background-image: url(../../Images/bgFormLogin.svg);
            background-position: center;
            background-repeat: no-repeat;
            height: 518px;
            width: 435px;
        }

        .span-edms {
            display: block;
            font-family: Roboto, sans-serif;
            font-size: 24px;
            color: #B51F09;
        }

        hr {
            border: 1px solid #B51F09;
        }

        .label-form {
            margin-bottom: 0;
            color: #B51F09;
        }

        .textbox-empty {
            font-family: Roboto, sans-serif !important;
            color: #b35b60 !important;
            border-color: #b35b60 !important;
        }

        .textbox-hover {
            border-color: #B51F09 !important;
            color: #B51F09 !important;
        }

        .btn-login {
            width: 100%;
            height: 40px;
            background-color: #4E0B0B;
            color: #FAF9EF;
            border: 0;
            border-radius: 3px 4px;
            font-family: Roboto, sans-serif;
            font-size: 13px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid full-background">
            <div class="row h-100">
                <div class="col-lg-2 my-auto"></div>
                <div class="col-lg-8 my-auto">
                    <div class="row justify-content-lg-center">
                        <div class="col-lg-6 bg-temple"></div>
                        <div class="col-lg-auto bg-form-login">
                            <div class="container">
                                <div class="row align-items-center" style="height: 200px">
                                    <div class="col" style="text-align: center">
                                        <span class="span-edms">CHÀO MỪNG ĐẾN VỚI</span>
                                        <span class="span-edms">EDMS VĂN MIẾU</span>
                                        <div style="margin: 0 50px 0 50px;">
                                            <hr />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div style="margin: 0 50px 0 50px; height: 70px">
                                            <label class="form-text label-form">Tài khoản</label>
                                            <telerik:RadTextBox ID="txtUsername" Skin="Bootstrap" CssClass="textbox-empty" HoveredStyle-CssClass="textbox-hover" RenderMode="Lightweight" runat="server" Width="100%"></telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div style="margin: 0 50px 0 50px;">
                                            <label class="form-text label-form">Mật khẩu</label>
                                            <telerik:RadTextBox ID="txtPassword" Skin="Bootstrap" CssClass="textbox-empty" HoveredStyle-CssClass="textbox-hover" RenderMode="Lightweight" runat="server" TextMode="Password" Width="100%"></telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div style="text-align:center; height:80px">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="label-form" Text=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div style="margin: 0 50px 0 50px; height: 70px">
                                            <asp:Button ID="btnLogin" runat="server" Text="ĐĂNG NHẬP" CssClass="btn-login" OnClick="btnLogin_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 my-auto"></div>
            </div>
        </div>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    </form>

    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script src="../../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript"></script>
    </telerik:RadScriptBlock>
</body>
</html>
