﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using Telerik.Web.UI;
using EDMs.Utilities.Sessions;
using System.Web;

namespace EDMs.Controls.Systems
{
    public partial class ConstructionPage : Page
    {
        private readonly ConstructionTreeService constructionTreeService = new ConstructionTreeService();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                this.LoadContructionTree();
            }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("Delete_"))
            {
                var docTypeId = Convert.ToInt32(e.Argument.Replace("Delete_", ""));
                var tempNode = new RadTreeNode();

                foreach (RadTreeNode node in this.rtvContruction.GetAllNodes())
                {
                    if (node.Value == docTypeId.ToString())
                    {
                        tempNode = node;
                        break;
                    }
                }

                foreach (var node in tempNode.GetAllNodes())
                {
                    constructionTreeService.Delete(Convert.ToInt32(node.Value));
                }

                this.constructionTreeService.Delete(Convert.ToInt32(tempNode.Value));
                this.LoadContructionTree();
                this.RestoreExpandStateTreeView();
            }
            else if (e.Argument == "RebindTreeView")
            {
                this.LoadContructionTree();
            }
        }

        protected void rtvContruction_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            
        }

        private void LoadContructionTree()
        {
            var listContruction = this.constructionTreeService.GetAll();
            this.rtvContruction.DataSource = listContruction;
            this.rtvContruction.DataFieldParentID = "ParentId";
            this.rtvContruction.DataTextField = "Name";
            this.rtvContruction.DataValueField = "ID";
            this.rtvContruction.DataFieldID = "ID";
            this.rtvContruction.DataBind();
            this.RestoreExpandStateTreeView();
        }

        private void RestoreExpandStateTreeView()
        {
            // Restore expand state of tree folder
            HttpCookie cookie = this.Request.Cookies["expandedNodesConstruction"];
            if (cookie != null)
            {
                var expandedNodeValues = cookie.Value.Split('*');
                foreach (var nodeValue in expandedNodeValues)
                {
                    RadTreeNode expandedNode = this.rtvContruction.FindNodeByValue(HttpUtility.UrlDecode(nodeValue));
                    if (expandedNode != null)
                    {
                        expandedNode.Expanded = true;
                    }
                }
            }
        }
    }
}

