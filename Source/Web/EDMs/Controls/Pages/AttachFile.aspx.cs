﻿using System;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Data.Entities;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using EDMs.Utilities;
using System.Linq;

namespace EDMs.Controls.Pages
{
    public partial class AttachFile : Page
    {
        private readonly TaiLieuHanhChinhService taiLieuHanhChinhService = new TaiLieuHanhChinhService();
        private readonly FolderService folderService = new FolderService();

        public AttachFile()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                try
                {
                    var error = "";
                    if (!string.IsNullOrEmpty(this.Request.QueryString["folId"]))
                    {
                        var folId = Convert.ToInt32(this.Request.QueryString["folId"]);
                        var folder = this.folderService.GetById(folId);
                       // var targetFolder = folder.DirName;
                        var serverFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + folder.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + folder.DirName;
                        var listUpload = docuploader.UploadedFiles;
                        if (listUpload.Count > 0)
                        {
                            foreach (UploadedFile docFile in listUpload)
                            {
                                var docFileName = docFile.FileName;
                                var docFileNameOrignal = docFile.FileName;

                                var objDoc = new TaiLieuHanhChinh();

                                var docObjLeaf = this.taiLieuHanhChinhService.GetSpecific(folId, docFileNameOrignal);
                                if (docObjLeaf.Count != 0)
                                {
                                    error += docFileName + " ;";
                                }
                                else
                                {
                                    objDoc.FolderID = folId;
                                    objDoc.CreatedBy = UserSession.Current.User.Id;
                                    objDoc.CreatedDate = DateTime.Now;
                                    var serverDocFileName = Utility.RemoveAllSpecialCharacter(docFileName);

                                    // Path file to save on server disc
                                    var saveFilePath = Path.Combine(Server.MapPath(serverFolder), serverDocFileName);

                                    // Path file to download from server
                                    var serverFilePath = serverFolder + "/" + serverDocFileName;
                                    var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                    objDoc.FileSize = docFile.ContentLength;
                                    objDoc.FilePath = serverFilePath;
                                    objDoc.FileExtension = fileExt;
                                    if (!Utility.FileIcon.ContainsKey(fileExt.ToLower()))
                                    {
                                        Utility.FileIcon.Add(fileExt.ToLower(), "/images/otherfile.png");
                                    }
                                    objDoc.FileExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt.ToLower()]) ? Utility.FileIcon[fileExt.ToLower()] : "/images/otherfile.png";
                                    objDoc.Name = serverDocFileName;
                                    objDoc.FileNameOriginal = docFileNameOrignal;
                                    objDoc.CategoryID = folder.CategoryID;
                                    docFile.SaveAs(saveFilePath, true);
                                    if (File.Exists(saveFilePath))
                                    {
                                        this.taiLieuHanhChinhService.Insert(objDoc);
                                    }
                                }
                                var ListNodeExpanded = (List<int>)Session["ListNodeExpanded"];
                                ListNodeExpanded.Add(folId);
                                UpdatePropertisFolder(folder.CategoryID.GetValueOrDefault(), ListNodeExpanded);
                            }
                        }
                    }

                    if (error != "")
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = "Have error when upload document file: <br/>'" + error + "'<br/> Already exists.";
                    }
                    else
                    {
                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                    }
                }
                catch (Exception ex)
                {
                    this.blockError.Visible = true;
                    this.lblError.Text = "Have error when upload document file: <br/>'" + ex.Message + "'";
                }
            }
        }

        private void UpdatePropertisFolder(int categori, List<int> ListNodeCurrent)
        {
            //var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
            //    (t=> t.ID).ToList();
            //var listFolder= listFolder
            foreach (var folderItem in ListNodeCurrent.OrderByDescending(t => t))
            {
                var objFolder = this.folderService.GetById(folderItem);
                // List<int> listTemp = new List<int>();
                var subfolderList = this.folderService.GetAllByParentId(folderItem);
                objFolder.NumberOfSubfolder = subfolderList.Count + subfolderList.Sum(t => t.NumberOfSubfolder);
                ////subfolderList.Add(objFolder);
                var docList = this.taiLieuHanhChinhService.GetAllByFolder(objFolder.ID);
                var folderSize = docList.Sum(t => t.FileSize) + subfolderList.Sum(t => t.FolderSize);
                objFolder.NumberOfDocument = docList.Count + subfolderList.Sum(t => t.NumberOfDocument);
                objFolder.FolderSize = folderSize;
                this.folderService.Update(objFolder);
            }
        }
    }
}