﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Net;
using System.Net.Mail;
using System.Text;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using EDMs.Data.Entities;
using Telerik.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;

namespace EDMs.Web.Controls.Document
{

    public class NotificationDoc
    {
        private string docNo;
        private string error;

        public NotificationDoc()
        { }

        public NotificationDoc(string docNo, string error)
        {
            DocNo = docNo;
            Error = error;
        }

        public string DocNo
        {
            get
            {
                return docNo;
            }

            set
            {
                docNo = value;
            }
        }

        public string Error
        {
            get
            {
                return error;
            }

            set
            {
                error = value;
            }
        }
    }

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocMasterList : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly TienSiService tienSiService = new TienSiService();
        private readonly BiaTienSiService biaTienSiService = new BiaTienSiService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocMasterList()
        {

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            this.blockError.Visible = false;
            this.lblError.Visible = false;
            this.lblError2.Text = "";
            var tiensiList = new List<TienSi>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("/Imports") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);
                        // Instantiate a new workbook
                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        for (int i = 0; i < workbook.Worksheets.Count; i++)
                        {
                            var worksheet = workbook.Worksheets[i];
                            currentSheetName = worksheet.Name;
                            var biaObj = this.biaTienSiService.GetByYear(Convert.ToInt32(currentSheetName));

                            if (biaObj != null)
                            {
                                // Create a datatable
                                var dataTable = new DataTable();

                                // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                dataTable = worksheet.Cells.ExportDataTableAsString(0, 0, worksheet.Cells.MaxRow, 2);

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                    {
                                        var tiensiObj = this.tienSiService.GetByName(dataRow["Column1"].ToString());
                                        if (tiensiObj == null)
                                        {
                                            tiensiObj = new TienSi();
                                            tiensiObj.BiaID = biaObj.ID;
                                            tiensiObj.HoTen = dataRow["Column1"].ToString().Trim();
                                            tiensiObj.QueQuan = dataRow["Column2"].ToString().Trim();
                                            tiensiObj.CreatedBy = UserSession.Current.User.Id;
                                            tiensiObj.CreatedDate = DateTime.Now;
                                            this.tienSiService.Insert(tiensiObj);
                                        }
                                        else
                                        {
                                            tiensiObj.BiaID = biaObj.ID;
                                            tiensiObj.HoTen = dataRow["Column1"].ToString().Trim();
                                            tiensiObj.QueQuan = dataRow["Column2"].ToString().Trim();
                                            this.tienSiService.Update(tiensiObj);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}