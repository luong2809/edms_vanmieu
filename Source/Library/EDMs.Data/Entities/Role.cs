﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    /// <summary>
    /// The role.
    /// </summary>
    public partial class Role
    {
        private int parentId;

        public int ParentId
        {
            get
            {
                return this.parentId;
            }
            set
            {
                this.parentId = value;
            }
        }
    }
}
