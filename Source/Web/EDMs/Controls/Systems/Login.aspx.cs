﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using System.Web.Security;
using System.Configuration;
using EDMs.Utilities;
using EDMs.Utilities.Sessions;
using EDMs.Data.Entities;

namespace EDMs.Controls.Systems
{
    public partial class Login : Page
    {
        private readonly UserService userService = new UserService();
        private readonly PermissionService permissionService = new PermissionService();
        private readonly MenuService menuService = new MenuService();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.txtUsername.Focus();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            var user = userService.GetUserByUsername(username);
            var permission = permissionService.GetByRoleId(user.RoleId.GetValueOrDefault()).First();
            var menu = menuService.GetByID(permission.MenuId);
            if (ConfigurationManager.AppSettings["EnableLDAP"] == "true")
            {
                if (username == "admin")
                {
                    if (user != null && user.Password == Utility.GetMd5Hash(password))
                    {
                        UserSession.CreateSession(user);
                        FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                        if (Session["ReturnURL"] != null)
                        {
                            var returnUrl = Session["ReturnURL"].ToString();
                            Session.Remove("ReturnURL");
                            Response.Redirect("~" + returnUrl);
                        }
                        else
                        {
                            Response.Redirect("~/Controls/Pages/BiaTienSiPage.aspx");
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Username or password is incorrect. ";
                    }
                }
                else
                {
                    var adPath = ConfigurationManager.AppSettings["LDAPPath"];
                    var domain = ConfigurationManager.AppSettings["Domain"];
                    var adAuth = new LdapAuthentication(adPath);
                    if (adAuth.IsAuthenticated(domain, username, password))
                    {
                        if (user != null)
                        {
                            UserSession.CreateSession(user);
                            FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                            if (Session["ReturnURL"] != null)
                            {
                                var returnUrl = Session["ReturnURL"].ToString();
                                Session.Remove("ReturnURL");
                                Response.Redirect("~" + returnUrl);
                            }
                            else
                            {
                                Response.Redirect("~/Controls/Pages/BiaTienSiPage.aspx");
                            }
                        }
                        else
                        {
                            var newUser = new User()
                            {
                                Username = username,
                                Password = Utility.GetMd5Hash(password),
                                FullName = adAuth.FilterAttribute,
                                Email = username + "@" + domain
                            };

                            var isSuccess = this.userService.Insert(newUser);
                            if (isSuccess)
                            {
                                UserSession.CreateSession(newUser);
                                Response.Redirect("~/Controls/Pages/BiaTienSiPage.aspx");
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Username or password is incorrect. ";
                    }
                }
            }
            else
            {
                if (user != null && user.Password == Utility.GetMd5Hash(password))
                {
                    UserSession.CreateSession(user);
                    FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                    if (Session["ReturnURL"] != null)
                    {
                        var returnUrl = Session["ReturnURL"].ToString();
                        Session.Remove("ReturnURL");
                        Response.Redirect("~" + returnUrl);
                    }
                    else
                    {
                        Response.Redirect(menu.Url);
                    }
                }
                else
                {
                    lblMessage.Text = "Username or password is incorrect. ";
                }
            }
        }
    }
}