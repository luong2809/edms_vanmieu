﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Data.Entities;
using System.Linq;
using System.IO;
using Telerik.Web.UI;
using System.Collections.Generic;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Utilities;

namespace EDMs.Controls.Pages
{
    public partial class CongVanEditPage : Page
    {
        private readonly UserService userService;
        private readonly RoleService rolesevice;
        private readonly CongVanService congVanService;

        public CongVanEditPage()
        {
            this.userService = new UserService();
            this.rolesevice = new RoleService();
            this.congVanService = new CongVanService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.congVanService.GetById(Convert.ToInt32(this.Request.QueryString["obj"]));
                    if (obj != null)
                    {
                        this.ddlLoai.SelectedValue = obj.LoaiCongVan.GetValueOrDefault().ToString();
                        this.txtSoCongVan.Text = obj.SoCongVan;
                        this.ddlNoiGui.SelectedValue = obj.SoNoiGui.ToString();
                        this.ddlNoiNhan.SelectedValue = obj.SoNoiNhan.ToString();

                        var userListTo = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlNoiNhan.SelectedValue)).OrderBy(t => t.FullName).ToList();
                        this.ddlNguoiNhan.DataSource = userListTo;
                        this.ddlNguoiNhan.DataTextField = "FullName";
                        this.ddlNguoiNhan.DataValueField = "Id";
                        this.ddlNguoiNhan.DataBind();
                        var userListFrom = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlNoiGui.SelectedValue)).OrderBy(t => t.FullName).ToList();
                        this.ddlNguoiGui.DataSource = userListFrom;
                        this.ddlNguoiGui.DataTextField = "FullName";
                        this.ddlNguoiGui.DataValueField = "Id";
                        this.ddlNguoiGui.DataBind();

                        this.ddlNguoiGui.SelectedValue = obj.SoNguoiGui.ToString();
                        this.ddlNguoiNhan.SelectedValue = obj.SoNguoiNhan.ToString();
                        this.txtNgayGui.SelectedDate = obj.NgayGui;
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }

                }
                else
                {
                    var lasted = this.congVanService.GetLasted();
                    var arrayCV = lasted.SoCongVan.Split('-');
                    if (arrayCV.Length > 1)
                    {
                        int soCV;
                        var flag = int.TryParse(arrayCV[1], out soCV);
                        if (flag) { soCV++; }
                        this.txtSoCongVan.Text = "CV-" + soCV.ToString().PadLeft(4, '0');
                    }
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var toId = Convert.ToInt32(this.ddlNguoiNhan.SelectedValue);
                var fromId = Convert.ToInt32(this.ddlNguoiGui.SelectedValue);
                var toUserObj = this.userService.GetByID(toId);
                var fromUserObj = this.userService.GetByID(fromId);
                var toObj = this.rolesevice.GetByID(Convert.ToInt32(this.ddlNoiNhan.SelectedValue));
                var fromObj = this.rolesevice.GetByID(Convert.ToInt32(this.ddlNoiGui.SelectedValue));
                var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };

                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.congVanService.GetById(docId);
                    if (obj != null)
                    {
                        obj.SoCongVan = this.txtSoCongVan.Text.Trim();
                        obj.LoaiCongVan = Convert.ToInt32(this.ddlLoai.SelectedValue);
                        obj.SoNguoiNhan = toUserObj != null ? toUserObj.Id : 0;
                        obj.TenNguoiNhan = toUserObj != null ? toUserObj.FullName : string.Empty;
                        obj.SoNguoiGui = fromUserObj != null ? fromUserObj.Id : 0;
                        obj.TenNguoiGui = fromUserObj != null ? fromUserObj.FullName : string.Empty;
                        obj.SoNoiGui = Convert.ToInt32(ddlNoiGui.SelectedValue);
                        obj.TenNoiGui = fromObj != null ? fromObj.Name : string.Empty;
                        obj.SoNoiNhan = Convert.ToInt32(ddlNoiNhan.SelectedValue);
                        obj.TenNoiNhan = toObj != null ? toObj.Name : string.Empty;
                        obj.NgayGui = this.txtNgayGui.SelectedDate;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;

                        foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                        {
                            var docFileName = Utility.RemoveAllSpecialCharacter(docFile.FileName);
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                            var oldfilePath = Server.MapPath(obj.FilePath);
                            var newServerPath = "../../Transmittals/Generated/" + filename;
                            var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;
                            docFile.SaveAs(newFilePath);
                            obj.FileExtension = fileExt;
                            obj.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png";
                            obj.FileSize = (double)docFile.ContentLength / 1024;
                            obj.HasAttachFile = true;
                            obj.FilePath = newServerPath;

                            if (File.Exists(oldfilePath))
                            {
                                File.Delete(oldfilePath);
                            }
                        }
                        this.congVanService.Update(obj);
                    }
                }
                else
                {
                    var obj = new CongVan();
                    obj.SoCongVan = this.txtSoCongVan.Text.Trim();
                    obj.LoaiCongVan = Convert.ToInt32(this.ddlLoai.SelectedValue);
                    obj.SoNguoiNhan = toUserObj != null ? toUserObj.Id : 0;
                    obj.TenNguoiNhan = toUserObj != null ? toUserObj.FullName : string.Empty;
                    obj.SoNguoiGui = fromUserObj != null ? fromUserObj.Id : 0;
                    obj.TenNguoiGui = fromUserObj != null ? fromUserObj.FullName : string.Empty;
                    obj.SoNoiGui = Convert.ToInt32(ddlNoiGui.SelectedValue);
                    obj.TenNoiGui = fromObj != null ? fromObj.Name : string.Empty;
                    obj.SoNoiNhan = Convert.ToInt32(ddlNoiNhan.SelectedValue);
                    obj.TenNoiNhan = toObj != null ? toObj.Name : string.Empty;
                    obj.NgayGui = this.txtNgayGui.SelectedDate;
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    var transId = this.congVanService.Insert(obj);
                    if (transId != null)
                    {
                        foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                        {
                            var docFileName = Utility.RemoveAllSpecialCharacter(docFile.FileName);
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                            var oldfilePath = Server.MapPath(obj.FilePath);
                            var newServerPath = "../../Transmittals/Generated/" + filename;
                            var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;
                            docFile.SaveAs(newFilePath);
                            obj.FileExtension = fileExt;
                            obj.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png";
                            obj.FileSize = (double)docFile.ContentLength / 1024;
                            obj.HasAttachFile = true;
                            obj.FilePath = newServerPath;
                            if (File.Exists(oldfilePath))
                            {
                                File.Delete(oldfilePath);
                            }
                            this.congVanService.Update(obj);
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }

        private void LoadComboData()
        {
            var grouplist = this.rolesevice.GetAll().OrderBy(t => t.Name).ToList();
            this.ddlNoiGui.DataSource = grouplist;
            this.ddlNoiGui.DataTextField = "Name";
            this.ddlNoiGui.DataValueField = "ID";
            this.ddlNoiGui.DataBind();
            this.ddlNoiGui.SelectedIndex = 0;

            this.ddlNoiNhan.DataSource = grouplist;
            this.ddlNoiNhan.DataTextField = "Name";
            this.ddlNoiNhan.DataValueField = "ID";
            this.ddlNoiNhan.DataBind();
            this.ddlNoiNhan.SelectedIndex = 0;
        }

        protected void ddlNoiGui_SelectedIndexChanged(object sender, EventArgs e)
        {
            var userListFrom = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlNoiGui.SelectedValue)).OrderBy(t => t.FullName).ToList();
            this.ddlNguoiGui.DataSource = userListFrom;
            this.ddlNguoiGui.DataTextField = "FullName";
            this.ddlNguoiGui.DataValueField = "Id";
            this.ddlNguoiGui.DataBind();

        }

        protected void ddlNoiNhan_SelectedIndexChanged(object sender, EventArgs e)
        {
            var userListTo = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlNoiNhan.SelectedValue)).OrderBy(t => t.FullName).ToList();
            this.ddlNguoiNhan.DataSource = userListTo;
            this.ddlNguoiNhan.DataTextField = "FullName";
            this.ddlNguoiNhan.DataValueField = "Id";
            this.ddlNguoiNhan.DataBind();
        }
    }
}