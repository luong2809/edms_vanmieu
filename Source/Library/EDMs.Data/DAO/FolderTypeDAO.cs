﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FolderTypeDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class FolderTypeDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FolderTypeDAO"/> class.
        /// </summary>
        public FolderTypeDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<FolderType> GetIQueryable()
        {
            return this.EDMsDataContext.FolderTypes;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<FolderType> GetAll()
        {
            return this.EDMsDataContext.FolderTypes.ToList();
        }

        public FolderType GetByFolder(int folderId)
        {
            return this.EDMsDataContext.FolderTypes.FirstOrDefault(t =>t.FolderID == folderId);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public FolderType GetById(int id)
        {
            return this.EDMsDataContext.FolderTypes.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE
        ////public List<FolderType> GetAllWithoutDeparment()
        ////{
        ////    return this.EDMsDataContext.FolderTypes.Where(t => t.RoleId == 0 || t.RoleId == null).ToList();
        ////}

        ////public List<FolderType> GetAllByDeparment(int deparmentId)
        ////{
        ////    return this.EDMsDataContext.FolderTypes.Where(t => t.RoleId == deparmentId).ToList();
        ////} 

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(FolderType ob)
        {
            try
            {
                this.EDMsDataContext.AddToFolderTypes(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(FolderType src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.FolderTypes
                                where rs.ID == src.ID
                                select rs).First();
                des.FolderID = src.FolderID;
                des.DocTypeID = src.DocTypeID;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(FolderType src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
