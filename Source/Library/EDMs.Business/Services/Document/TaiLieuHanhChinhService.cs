﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class TaiLieuHanhChinhService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly TaiLieuHanhChinhDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaiLieuHanhChinhService"/> class.
        /// </summary>
        public TaiLieuHanhChinhService()
        {
            this.repo = new TaiLieuHanhChinhDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHanhChinh> GetSpecific(int folderID, string fileName)
        {
            return this.repo.GetSpecific(folderID, fileName);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<TaiLieuHanhChinh> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// The get all by owner.
        /// </summary>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHanhChinh> GetAllByFolder(int folderID)
        {
            return this.repo.GetAllByFolder(folderID);
        }

        public List<TaiLieuHanhChinh> GetAllByFolder(List<int> folderID)
        {
            return this.repo.GetAllByFolder(folderID);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public TaiLieuHanhChinh GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(TaiLieuHanhChinh bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(TaiLieuHanhChinh bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(TaiLieuHanhChinh bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion


        public List<TaiLieuHanhChinh> GetAllDocList(List<int> ListId)
        {
            return this.repo.GetAll().Where(t => ListId.Contains(t.ID)).ToList();
        }
    }
}
