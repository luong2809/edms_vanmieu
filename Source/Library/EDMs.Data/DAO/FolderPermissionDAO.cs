﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FolderPermissionDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class FolderPermissionDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FolderPermissionDAO"/> class.
        /// </summary>
        public FolderPermissionDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<FolderPermission> GetIQueryable()
        {
            return EDMsDataContext.FolderPermissions;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<FolderPermission> GetAll()
        {
            return EDMsDataContext.FolderPermissions.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public FolderPermission GetById(int id)
        {
            return this.EDMsDataContext.FolderPermissions.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// The get by role id.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="FolderPermission"/>.
        /// </returns>
        public List<FolderPermission> GetByRoleId(int roleId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == roleId && t.TypeID == 1).ToList();
        }

        public List<FolderPermission> GetByObjectId(int objectId, int type)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == objectId && t.TypeID==type).ToList();
        }
        public List<FolderPermission> GetCategory(List<int> roleId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => roleId.Contains(t.ObjectId.Value) && t.TypeID == 1).ToList();
        }
        public bool AddGroupDataPermissions(List<FolderPermission> groupDataPermission)
        {
            try
            {
                foreach (var item in groupDataPermission)
                {
                    EDMsDataContext.AddToFolderPermissions(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public List<FolderPermission> GetCategoryByUsser(int User)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId==User && t.TypeID == 2).ToList();
        }
        public List<FolderPermission> GetByUserId(int UserId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == UserId && t.TypeID == 2).ToList();
        }
        public List<FolderPermission> GetByUserId(int UserId, int categoryId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == UserId && t.CategoryId == categoryId && t.TypeID == 2).ToList();
        }
        public List<FolderPermission> GetByRoleId(int roleId, int categoryId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == roleId && t.CategoryId == categoryId && t.TypeID == 1).ToList();
        }
        public List<FolderPermission> GetByUserIdList(int UserId, int categoryId, int folderId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == UserId && t.TypeID == 2 && t.CategoryId == categoryId && t.FolderId == folderId).ToList();
        }
        public List<FolderPermission> GetByRoleIdList(int roleId, int categoryId, int folderId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == roleId && t.TypeID == 1 && t.CategoryId == categoryId && t.FolderId == folderId).ToList();
        }
        public List<FolderPermission> GetByRoleIdList(List<int> roleId, int folderId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => roleId.Contains(t.ObjectId.Value) && t.TypeID == 1 && t.FolderId == folderId).ToList();
        }
        public List<FolderPermission> GetByUserIdList(int UserId, int categoryId, List<int> folderId)//typeId ==2 is group
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == UserId && t.TypeID == 2 && t.CategoryId == categoryId && folderId.Contains(t.FolderId.Value)).ToList();
        }
        public FolderPermission GetByUserIdList(int UserId,  int folderId)//typeId ==2 is group
        {
            return this.EDMsDataContext.FolderPermissions.FirstOrDefault(t => t.ObjectId == UserId && t.TypeID == 2 &&  folderId==t.FolderId);
        }
        public List<FolderPermission> GetByRoleIdList(int roleId, int categoryId, List<int> folderId)//typeId ==1 is group
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.ObjectId == roleId && t.TypeID==1 && t.CategoryId == categoryId && folderId.Contains(t.FolderId.Value)).ToList();
        }
        public List<FolderPermission> GetFolderByRoleIdList(int categoryId, List<int> roleId)//typeId ==1 is group
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => roleId.Contains(t.ObjectId.Value) && t.TypeID == 1 && t.CategoryId == categoryId).ToList();
        }
        public List<FolderPermission> GetFolderByRoleIdList(List<int> roleId)//typeId ==1 is group
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => roleId.Contains(t.ObjectId.Value) && t.TypeID == 1).ToList();
        }
        public List<FolderPermission> GetAllByFolder(int folderId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.FolderId == folderId).ToList();
        }
        public List<FolderPermission> GetAllByFolder(List<int> folderId)
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => folderId.Contains(t.FolderId.Value)).ToList();
        }
        //typeId ==1 is group //typeId ==2 is group
        public List<FolderPermission> GetAllByFolder(int folderId, int type )
        {
            return this.EDMsDataContext.FolderPermissions.Where(t => t.FolderId == folderId && t.TypeID==type).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool DeleteFolderPermission(List<FolderPermission> FolderPermission)
        {
            try
            {
                foreach (var item in FolderPermission)
                {
                    EDMsDataContext.DeleteObject(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Adds the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool AddFolderPermissions(List<FolderPermission> FolderPermission)
        {
            try
            {
                foreach (var item in FolderPermission)
                {
                    EDMsDataContext.AddToFolderPermissions(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(FolderPermission ob)
        {
            try
            {
                EDMsDataContext.AddToFolderPermissions(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(FolderPermission src)
        {
            try
            {
                FolderPermission des = (from rs in this.EDMsDataContext.FolderPermissions
                                where rs.ID == src.ID
                                select rs).First();

                des.CategoryId = src.CategoryId;
                des.FolderId = src.FolderId;
                des.ObjectId = src.ObjectId;
                des.TypeID = src.TypeID;
                des.TypeName = src.TypeName;
                des.Folder_ChangePermission = src.Folder_ChangePermission;
                des.Folder_IsFullPermission = src.Folder_IsFullPermission;
                des.Folder_CreateSubFolder = src.Folder_CreateSubFolder;
                des.Folder_Delete = src.Folder_Delete;
                des.Folder_Read = src.Folder_Read;
                des.Folder_Write = src.Folder_Write;
                des.File_FullPermission = src.File_FullPermission;
                des.File_ChangePermission = src.File_ChangePermission;
                des.File_Create = src.File_Create;
                des.File_Delete = src.File_Delete;
                des.File_Read = src.File_Read;
                des.File_Write = src.File_Write;
                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;
                des.ObjectIdName = src.ObjectIdName;
                des.File_NoAccess = src.File_NoAccess;
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(FolderPermission src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
