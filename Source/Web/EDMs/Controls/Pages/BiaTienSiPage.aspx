﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site_VM.Master" AutoEventWireup="true" CodeBehind="BiaTienSiPage.aspx.cs" Inherits="EDMs.Controls.Pages.BiaTienSiPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <style>
        .span-page {
            font-size: 28px;
            color: #B51F09;
        }

        .div-tool {
            height: 70px;
            padding-right: 15px;
        }

        .RadToolBar .rtbChoiceArrow {
            width: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap {
            background-color: transparent !important;
            border: 0 !important;
        }

        .RadToolBarDropDown_Bootstrap {
            color: #B51F09 !important;
        }

        .RadToolBar_Bootstrap .rtbOuter {
            background-color: transparent !important;
            border: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap .rtbWrap {
            padding: 6px 10px !important;
        }

        .RadToolBar .rtbIcon {
            width: 27px;
            height: 27px;
        }

        .RadToolBarDropDown_Bootstrap .rtbIcon {
            width: 16px;
            top: 4px !important;
        }

        .RadButton_Bootstrap.rbButton {
            background-color: #4E0B0B !important;
            color: #fff !important;
        }

        .RadGrid_Silk .rgSelectedRow {
            background-color: #FAF9EF !important;
            color: #252525 !important;
        }

        .icon-grid {
            height: 24px !important;
            width: 24px !important;
        }

        #ctl00_contentTool_ctl00_contentBody_grdDocumentPanel {
            height: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentTool" runat="server">
    <%-- manager code js --%>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $('iframe').load(function () {
                    //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function RowClick(sender, eventArgs) {
                var id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblId.ClientID %>").value = id;
            }

            function ShowInforgraphic(id) {
                var owd = $find("<%=Inforgraphic.ClientID %>");
                owd.Show();
                owd.setUrl("InforgraphicBia.aspx?biaId=" + id, "Infographic");
            }

            function ShowEditForm(id) {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("BiaEditPage.aspx?obj=" + id, "EditPage");
            }

            function ShowAddForm() {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("BiaEditPage.aspx", "AddNew");
            }

            function refreshGrid(arg) {
                if (!arg) {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("Rebind");
                }
                else {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("RebindAndNavigate");
                }
            }

            //expand dropdown left
            function dropdownOpened(sender, args) {
                var dropDownElem = args.get_item().get_element();
                var rightPos = $telerik.getLocation(dropDownElem).x + $telerik.getSize(dropDownElem).width;
                var dropDownElemContainer = args.get_item()._animationContainer;
                var leftPos = rightPos - dropDownElemContainer.clientWidth;
                dropDownElemContainer.style.left = leftPos + "px";
            }
            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                if(strValue=="ImportFileLoad"){
                    ajaxManager.ajaxRequest("ImportFileLoad");
                }

            }
            //disable ajax control
            function RequestStart(sender, args) {
                args.EventTargetElement.disabled = true;
            }
            function ResponseEnd(sender, args) {
                args.EventTargetElement.disabled = false;
            }
        </script>
    </telerik:RadCodeBlock>

    <%-- manager ajax page --%>
    <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="ajaxCustomer_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <%-- loading page --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" MinDisplayTime="1000" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>

    <%-- hidden field --%>
    <asp:HiddenField runat="server" ID="lblId" />

    <%-- manager window popup --%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditPage" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="570" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="Inforgraphic" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="1600" Width="900" CenterIfModal="true" Modal="true" KeepInScreenBounds="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="row justify-content-between align-items-center">
        <div class="col-lg-4">
            <span class="span-page">BIA TIẾN SĨ</span>
        </div>
        <div class="col-lg-auto">
            <div class="row align-items-center">
                <div class="col-lg-auto" style="padding-right: 0">
                    <div class="row align-items-center div-tool">
                        <telerik:RadButton ID="btnAdd" runat="server" Text="TẠO MỚI" Skin="Bootstrap" RenderMode="Lightweight" Height="40px" Width="130px" AutoPostBack="false" OnClientClicking="ShowAddForm" Visible="false">
                            <Icon PrimaryIconUrl="~/Images/add-trang.svg" PrimaryIconTop="6px" PrimaryIconLeft="8px" PrimaryIconHeight="19px" PrimaryIconWidth="19px" />
                        </telerik:RadButton>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <telerik:RadToolBar ID="CustomerMenu" runat="server" Skin="Bootstrap" Width="100%" Height="40px" OnClientButtonClicking="OnClientButtonClicking" OnClientDropDownOpened="dropdownOpened">
                        <Items>
                            <telerik:RadToolBarDropDown ImageUrl="~/Images/settings-do.svg" DropDownWidth="150px">
                                <Buttons>
                                    <telerik:RadToolBarButton Value="ImportFileLoad" Text="Import file load"></telerik:RadToolBarButton>
                                </Buttons>
                            </telerik:RadToolBarDropDown>
                        </Items>
                    </telerik:RadToolBar>
                </div>
                <%--<div class="col-lg-auto">
                    <div class="row align-items-center div-tool">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox-search"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn-search div-search" />
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <%-- grid data --%>
    <telerik:RadGrid ID="grdDocument" runat="server" Skin="Silk" RenderMode="Lightweight" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None" Height="98%"
        OnItemDataBound="grdDocument_ItemDataBound" OnDeleteCommand="grdDocument_DeleteCommand" OnNeedDataSource="grdDocument_OnNeedDataSource" PageSize="100" Style="outline: none">
        <GroupingSettings CaseSensitive="false" />
        <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" TableLayout="Auto" CssClass="rgMasterTable" AllowFilteringByColumn="true">
            <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; items." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
            <Columns>
                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                <telerik:GridButtonColumn UniqueName="EditButton" ButtonType="ImageButton" ImageUrl="~/Images/edit-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn UniqueName="DeleteButton" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete-do.svg" ButtonCssClass="icon-grid" Visible="false">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn UniqueName="Inforgraphic" ButtonType="ImageButton" ImageUrl="~/Images/info-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn UniqueName="TenBia" DataField="TenBia" HeaderText="Bia" AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" FilterControlWidth="97%">
                    <HeaderStyle Width="200px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Kientruc" HeaderText="Kiến trúc" UniqueName="Kientruc" Visible="false">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NamKhoaThi" HeaderText="Năm khoa thi" UniqueName="NamKhoaThi" AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" FilterControlWidth="97%">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TieuDeKhoaThi" HeaderText="Tiêu đề khoa thi" UniqueName="TieuDeKhoaThi" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Left" Width="180px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="QuanToChucKhoaThi" HeaderText="Quan tổ chức" UniqueName="QuanToChucKhoaThi" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CachThucToChuc" HeaderText="Cách thức thi" UniqueName="CachThucToChuc" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ThoiGianDungBia" HeaderText="Năm dựng bia" UniqueName="ThoiGianDungBia" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NguoiThamGiaDungBia" HeaderText="Người dựng bia" UniqueName="NguoiThamGiaDungBia" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="HoTenQueQuan" HeaderText="Lí lịch" UniqueName="HoTenQueQuan" Visible="false">
                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
            <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True"></Resizing>
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowClick="RowClick"></ClientEvents>
            <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
        </ClientSettings>
    </telerik:RadGrid>
</asp:Content>
