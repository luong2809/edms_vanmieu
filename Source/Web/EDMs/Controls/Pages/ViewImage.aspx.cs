﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Controls.Pages
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ViewImage : Page
    {

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly TienSiService tienSiService = new TienSiService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ViewImage()
        {
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var id = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.tienSiService.GetById(id);
                    //this.imgDocument.ImageUrl = docObj.FilePath;
                    this.image.Src= obj.InforgraphicFilePath;
                }
            }
        }

    }
}