﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using System.Web.Security;

namespace EDMs
{
    public partial class Site_VM : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        public int RoleId
        {
            get { return UserSession.Current.User.RoleId != null ? UserSession.Current.User.RoleId.Value : -1; }
        }
        private readonly MenuService menuService = new MenuService();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblTime.Text = DateTime.Now.ToString("dddd, dd MMM");
            this.lblUser.Text = "Xin chào, " + UserSession.Current.User.Username;
            LoadMainMenu();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!UserSession.Current.IsAvailable)
            {
                if (Request.RawUrl != "/Controls/Systems/Login.aspx")
                {
                    Session.Add("ReturnURL", Request.RawUrl);
                }
                Response.Redirect("~/Controls/Systems/Login.aspx");
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? string.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? string.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        private void LoadMainMenu()
        {
            var menus = this.menuService.GetAllRelatedPermittedMenuItems(RoleId, 1).OrderBy(t => t.Priority).ToList();

            this.mainMenu.DataSource = menus;
            this.mainMenu.DataTextField = "Description";
            this.mainMenu.DataNavigateUrlField = "Url";
            this.mainMenu.DataFieldID = "Id";
            this.mainMenu.DataValueField = "Id";
            this.mainMenu.DataFieldParentID = "ParentId";
            this.mainMenu.DataBind();

            //if (Session["SelectedMainMenu"] != null)
            //{
            //    this.MainMenu.Items[Convert.ToInt32(Session["SelectedMainMenu"])].Selected = true;
            //    Session.Remove("SelectedMainMenu");
            //}

            this.SetIcon(this.mainMenu.Items, menus);
        }

        private void SetIcon(RadMenuItemCollection menu, List<EDMs.Data.Entities.Menu> menuData)
        {
            foreach (RadMenuItem menuItem in menu)
            {
                if (!string.IsNullOrEmpty(menuItem.Value))
                {
                    var tempMenu = menuData.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
                    if (tempMenu != null)
                    {
                        menuItem.ImageUrl = tempMenu.Icon;
                    }
                }

                if (menuItem.Items.Count > 0)
                {
                    this.SetIcon(menuItem.Items, menuData);
                }
            }
        }

        protected void toolbarAvatar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton btn = e.Item as RadToolBarButton;
            if(btn.CommandName=="Logout")
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                UserSession.DestroySession();
                Response.Redirect("~/Controls/Systems/Login.aspx");
                Response.StatusCode = 401;
                Response.StatusDescription = "Unauthorized";
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");
                Response.End();
                Session.Clear();
            }
        }
    }
}