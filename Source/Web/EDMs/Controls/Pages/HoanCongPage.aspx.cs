﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Data.Entities;

namespace EDMs.Controls.Pages
{
    public partial class HoanCongPage : Page
    {
        private readonly TaiLieuHoanCongService taiLieuHoanCongService = new TaiLieuHoanCongService();
        private readonly ConstructionTreeService constructionTreeService = new ConstructionTreeService();
        private readonly MenuService menuService = new MenuService();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.Page.IsPostBack)
            {
                panelMenu.Items[0].Expanded = false;
                LoadCombobox();
                LoadMainMenu();
            }
        }

        public void LoadCombobox()
        {
            RadPanelItem item = panelDoctype.FindItemByValue("Construction");
            RadTreeView rtvConstruction = (RadTreeView)item.FindControl("rtvConstruction");
            var listConstruction = this.constructionTreeService.GetAll();
            rtvConstruction.DataSource = listConstruction;
            rtvConstruction.DataFieldParentID = "ParentId";
            rtvConstruction.DataTextField = "Name";
            rtvConstruction.DataValueField = "ID";
            rtvConstruction.DataFieldID = "ID";
            rtvConstruction.DataBind();
        }

        private void LoadMainMenu()
        {
            RadPanelItem item = panelMenu.FindItemByValue("Menu");
            RadMenu mainMenu = (RadMenu)item.FindControl("mainMenu");
            var menus = this.menuService.GetAllRelatedPermittedMenuItems(UserSession.Current.RoleId, 1).OrderBy(t => t.Priority).ToList();
            mainMenu.DataSource = menus;
            mainMenu.DataTextField = "Description";
            mainMenu.DataNavigateUrlField = "Url";
            mainMenu.DataFieldID = "Id";
            mainMenu.DataValueField = "Id";
            mainMenu.DataFieldParentID = "ParentId";
            mainMenu.DataBind();
            this.SetIcon(mainMenu.Items, menus);
        }

        private void SetIcon(RadMenuItemCollection menu, List<EDMs.Data.Entities.Menu> menuData)
        {
            foreach (RadMenuItem menuItem in menu)
            {
                if (!string.IsNullOrEmpty(menuItem.Value))
                {
                    var tempMenu = menuData.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
                    if (tempMenu != null)
                    {
                        menuItem.ImageUrl = tempMenu.Icon;
                    }
                }

                if (menuItem.Items.Count > 0)
                {
                    this.SetIcon(menuItem.Items, menuData);
                }
            }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            LoadDocuments(false);
        }

        protected void LoadDocuments(bool isbind = false)
        {
            var list = new List<TaiLieuHoanCong>();
            RadPanelItem item = panelDoctype.FindItemByValue("Construction");
            RadTreeView rtvConstruction = (RadTreeView)item.FindControl("rtvConstruction");
            if (rtvConstruction.SelectedNode != null)
            {
                List<int> listChildConstruction = this.constructionTreeService.GetByParent(Convert.ToInt32(rtvConstruction.SelectedValue)).Select(t=>t.ID).ToList();
                listChildConstruction.Add(Convert.ToInt32(rtvConstruction.SelectedValue));
                list = this.taiLieuHoanCongService.GetByConstruction(listChildConstruction);
            }
            else
            {
                list = this.taiLieuHoanCongService.GetAll();
            }
            this.grdDocument.DataSource = list;

            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }

        protected void rtvConstruction_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "~/Images/Construction.svg";
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.taiLieuHoanCongService.Delete(disId);

            this.grdDocument.Rebind();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var ID = item.GetDataKeyValue("ID");
                ImageButton btn = (ImageButton)item["EditButton"].Controls[0];
                btn.Attributes.Add("OnClick", "ShowEditForm(" + ID + ");return false;");
            }
        }

        protected void rtvConstruction_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            LoadDocuments(true);
        }
    }
}