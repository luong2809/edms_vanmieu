﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InfographicTienSi.aspx.cs" Inherits="edms.Controls.Pages.InfographicTienSi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../../Content/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../../Content/styles.css" />

    <style type="text/css">
        html, body, form {
            height: 100%;
        }

        .container-fluid {
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .bg-head {
            background-image: url(../../Images/bg.svg);
            background-repeat: no-repeat;
            height: 439px;
        }
        .bg-content {
            background-image: url(../../Images/content.svg);
            background-repeat: no-repeat;
            height: 1500px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptBlock runat="server">
            <script src="../../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
            <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript">

            </script>
        </telerik:RadScriptBlock>
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>


        <div class="container-fluid">
            <div class="row bg-head" >
            </div>
            <div class="row bg-content">
            </div>
        </div>
    </form>
</body>
</html>
