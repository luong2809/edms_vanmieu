﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Document.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Document type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO;

    /// <summary>
    /// The document.
    /// </summary>
    public partial class TaiLieuHanhChinh
    {
        /// <summary>
        /// Gets the folder doc.
        /// </summary>
        public Folder FolderDoc
        {
            get
            {
                var folderDao = new FolderDAO();
                return folderDao.GetById(this.FolderID.GetValueOrDefault());
            }
        }

        /// <summary>
        /// Gets the created user.
        /// </summary>
        public User CreatedUser
        {
            get
            {
                var userDAO = new UserDAO();
                return userDAO.GetByID(this.CreatedBy.GetValueOrDefault());
            }
        }

        /// <summary>
        /// Gets the last updated user.
        /// </summary>
        public User LastUpdatedUser
        {
            get
            {
                var userDAO = new UserDAO();
                return userDAO.GetByID(this.LastUpdatedBy.GetValueOrDefault());
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is folder.
        /// </summary>
        public bool IsFolder { get; set; }
    }
}
