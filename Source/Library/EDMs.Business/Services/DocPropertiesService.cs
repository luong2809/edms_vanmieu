﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class DocPropertiesService
    {

        private List<Property> properties = new List<Property>()
            {
                new Property { ID = 1, Name = "Doc Number" },
                new Property { ID = 2, Name = "Title" },
                new Property { ID = 3, Name = "Document Type" },
                new Property { ID = 4, Name = "Sender" },
                new Property { ID = 5, Name = "Senders Ref" },
                new Property { ID = 6, Name = "Delivery Date" },
                new Property { ID = 7, Name = "Subject" },
                new Property { ID = 8, Name = "Written By" },
                new Property { ID = 9, Name = "Register By" },
                new Property { ID = 10, Name = "Mean of Delivery" },
                new Property { ID = 11, Name = "Reference" },
                new Property { ID = 12, Name = "Subject Key" },
                new Property { ID = 13, Name = "Recipient" },
                new Property { ID = 14, Name = "Outgoing Seq" },
                new Property { ID = 15, Name = "Recipients" },
                new Property { ID = 16, Name = "Response Required" },
                new Property { ID = 17, Name = "Imcoming Seq" },
                new Property { ID = 18, Name = "Date of Docs" },
                new Property { ID = 19, Name = "Responsible" },
                new Property { ID = 20, Name = "KKSCode" },
                new Property { ID = 21, Name = "UAS Code" },
                new Property { ID = 22, Name = "Discipline Code" },
                new Property { ID = 23, Name = "KKS Name" },
                new Property { ID = 24, Name = "UAS Name" },
                new Property { ID = 25, Name = "Discipline Name" },
                new Property { ID = 26, Name = "Alternate Doc Name" },
                new Property { ID = 27, Name = "Alternate Doc Title" },
                new Property { ID = 28, Name = "Supplier Group Name" },
                new Property { ID = 29, Name = "Sub Group" },
                new Property { ID = 30, Name = "Area" },
                new Property { ID = 31, Name = "Issue Date" },
                new Property { ID = 32, Name = "Revision" },
                new Property { ID = 33, Name = "Lastest Rev Date" },
                new Property { ID = 34, Name = "Skire SB Number" },
                new Property { ID = 35, Name = "Changes Post COD" },
                new Property { ID = 36, Name = "Item Code" },
                new Property { ID = 37, Name = "Item Name" },
                new Property { ID = 38, Name = "Drawing Type Name" },
                new Property { ID = 39, Name = "Drawing Type Code" }
            };

        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Property> GetAll()
        {
            return this.properties;
        }
    }

    /// <summary>
    /// The property.
    /// </summary>
    public class Property
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}
