﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using Telerik.Web.UI;
using EDMs.Utilities.Sessions;

namespace EDMs.Controls.Systems
{
    public class MenuMainPermissionWrapper
    {
        #region Properties

        /// <summary>
        /// Gets or sets the menu.
        /// </summary>
        /// <value>
        /// The menu.
        /// </value>
        public Data.Entities.Menu Menu { get; set; }

        public int MenuId
        {
            get { return Menu.Id; }
        }

        public int? ParentId
        {
            get { return Menu.ParentId; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is permitted.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is permitted; otherwise, <c>false</c>.
        /// </value>
        public bool IsPermitted { get; set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get { return Menu.Description; }
        }

        #endregion
    }
    public partial class MenuPermissionPage : Page
    {
        private readonly RoleService roleService = new RoleService();
        private readonly MenuService menuService = new MenuService();
        private readonly PermissionService permissionService = new PermissionService();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                LoadCombobox();
            }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            
        }

        private void LoadCombobox()
        {
            var roleList = this.roleService.GetAll();
            roleList.Insert(0, new Role { Id = -1, Name = "GROUP" });
            this.ddlGroup.DataSource = roleList;
            this.ddlGroup.DataValueField = "Id";
            this.ddlGroup.DataTextField = "Name";
            this.ddlGroup.DataBind();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            this.LoadPermissionData(Convert.ToInt32(ddlGroup.SelectedValue));
        }

        private void LoadPermissionData(int roleId)
        {
            var menus = this.menuService.GetAllMenusByType(1);
            var permittedMenuIds = this.permissionService.GetByRoleId(roleId).Select(x => x.MenuId).ToList();

            var menuPermissions = menus.Select(menu => new MenuMainPermissionWrapper
            {
                Menu = menu,
                IsPermitted = permittedMenuIds.Any(menuId => menuId == menu.Id)
            });

            treePemissionMenus.DataSource = menuPermissions;
            treePemissionMenus.DataBind();

            //To check the all parents checkbox state
            var allNodes = treePemissionMenus.Nodes;
            foreach (RadTreeNode node in allNodes)
            {
                CheckTreeNodeState(node);
            }
        }

        private static void CheckTreeNodeState(RadTreeNode node)
        {
            if (node.Nodes.Count <= 0) return;

            bool state = true;
            foreach (RadTreeNode childNode in node.Nodes)
            {
                if (childNode.Nodes.Count > 0) CheckTreeNodeState(childNode);
                if (!childNode.Checked) state = false;
            }
            node.Checked = state;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var selectedRoleId = Convert.ToInt32(this.ddlGroup.SelectedValue);
            var checkedNodes = this.treePemissionMenus.CheckedNodes.Where(x => x.Nodes.Count == 0);

            // Gets the stored permissions
            var permissions = this.permissionService.GetByRoleId(selectedRoleId);

            // Gets the objects have changed status from 'Checked' to 'Unchecked'
            var deletePermissions = permissions.Where(x => !checkedNodes.Any(checkedNode => checkedNode.Value == x.MenuId.ToString()));
            deletePermissions = deletePermissions.Where(x => x.Menu.Type == 1);

            // Get the object have 'Checked' status
            var addPermissions = checkedNodes.Where(node => !permissions.Any(y => y.MenuId.ToString() == node.Value))
                .Select(node => new Permission
                {
                    MenuId = int.Parse(node.Value),
                    RoleId = selectedRoleId
                });

            var isDeleted = this.permissionService.DeletePermissions(deletePermissions.ToList());
            var isAdded = this.permissionService.AddPermissions(addPermissions.ToList());
        }
    }
}

