﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaiLieuTienSiDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class TaiLieuTienSiDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaiLieuTienSiDAO"/> class.
        /// </summary>
        public TaiLieuTienSiDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TaiLieuTienSi> GetIQueryable()
        {
            return EDMsDataContext.TaiLieuTienSis;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuTienSi> GetAll()
        {
            return EDMsDataContext.TaiLieuTienSis.OrderByDescending(t => t.ID).ToList();
        }

        public List<TaiLieuTienSi> AdvanceSearch(List<string> list)
        {
            return EDMsDataContext.TaiLieuTienSis.Where(t => list.Contains(t.FileName)).OrderByDescending(t => t.ID).ToList();
        }

        public List<TaiLieuTienSi> AdvanceSearch(List<string> list, int docID)
        {
            return EDMsDataContext.TaiLieuTienSis.Where(t => t.TienSiID == docID && list.Contains(t.FileName)).OrderByDescending(t => t.ID).ToList();
        }

        public List<TaiLieuTienSi> GetByTienSi(int id)
        {
            return EDMsDataContext.TaiLieuTienSis.Where(t => t.TienSiID == id).OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public TaiLieuTienSi GetById(int id)
        {
            return this.EDMsDataContext.TaiLieuTienSis.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(TaiLieuTienSi ob)
        {
            try
            {
                EDMsDataContext.AddToTaiLieuTienSis(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(TaiLieuTienSi src)
        {
            try
            {
                TaiLieuTienSi des = (from rs in this.EDMsDataContext.TaiLieuTienSis
                                     where rs.ID == src.ID
                                     select rs).First();

                des.HoTen = src.HoTen;
                des.TienSiID = src.TienSiID;
                des.FilePath = src.FilePath;
                des.FileName = src.FileName;
                des.FileExtension = src.FileExtension;
                des.FileExtensionIcon = src.FileExtensionIcon;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(TaiLieuTienSi src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
