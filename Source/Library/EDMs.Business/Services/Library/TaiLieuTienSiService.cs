﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class TaiLieuTienSiService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly TaiLieuTienSiDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaiLieuTienSiService"/> class.
        /// </summary>
        public TaiLieuTienSiService()
        {
            this.repo = new TaiLieuTienSiDAO();
        }

        #region Get (Advances)
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<TaiLieuTienSi> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        public List<TaiLieuTienSi> AdvanceSearch(List<string> list)
        {
            return this.repo.AdvanceSearch(list);
        }

        public List<TaiLieuTienSi> AdvanceSearch(List<string> list, int docID)
        {
            return this.repo.AdvanceSearch(list,docID);
        }

        public List<TaiLieuTienSi> GetByTienSi(int id)
        {
            return this.repo.GetByTienSi(id);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public TaiLieuTienSi GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(TaiLieuTienSi bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(TaiLieuTienSi bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(TaiLieuTienSi bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
