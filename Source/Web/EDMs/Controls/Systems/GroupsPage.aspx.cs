﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using Telerik.Web.UI;
using EDMs.Utilities.Sessions;

namespace EDMs.Controls.Systems
{
    public partial class GroupsPage : Page
    {
        private readonly RoleService roleService = new RoleService();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                if (ConfigurationManager.AppSettings["EnableLDAP"] == "true")
                {
                    this.grdDocument.MasterTableView.GetColumn("DeleteButton").Display = false;
                }
            }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocuments();
        }

        private void LoadDocuments()
        {
            var userList = this.roleService.GetAll().OrderBy(t => t.Name);
            this.grdDocument.DataSource = userList;
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.roleService.Delete(disId);

            this.grdDocument.Rebind();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var ID = item.GetDataKeyValue("ID");
                ImageButton btn = (ImageButton)item["EditButton"].Controls[0];
                btn.Attributes.Add("OnClick", "ShowEditForm(" + ID + ");return false;");
            }
        }
    }
}

