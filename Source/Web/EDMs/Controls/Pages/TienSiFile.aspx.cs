﻿using System;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Data.Entities;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using System.Diagnostics;
using Spire.Pdf;
using Spire.Pdf.General.Find;

namespace EDMs.Controls.Pages
{
    public partial class TienSiFile : Page
    {
        private readonly TaiLieuTienSiService taiLieuTienSiService = new TaiLieuTienSiService();

        Nest.ConnectionSettings connectionSettings;
        Nest.ElasticClient elasticClient;

        public TienSiFile()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            connectionSettings = new Nest.ConnectionSettings()
                .DefaultMappingFor<documentElastic>(i => i
                .IndexName("vanmieu")
                .TypeName("_doc"))
                .EnableDebugMode()
                .PrettyJson()
                .RequestTimeout(TimeSpan.FromMinutes(2));
            elasticClient = new Nest.ElasticClient(connectionSettings);
            if (!this.IsPostBack)
            {

            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("Highlight"))
            {
                if (!string.IsNullOrEmpty(Request.QueryString["search"]))
                {
                    int id = Convert.ToInt32(e.Argument.Split('_')[1]);
                    var doc = this.taiLieuTienSiService.GetById(id);

                    string search = Request.QueryString["search"];
                    var targetFolder = "../../DocumentLibrary/Temp";
                    var saveFilePath = Path.Combine(Server.MapPath(targetFolder), doc.FileName);

                    PdfDocument pdf = new PdfDocument(Server.MapPath(doc.FilePath));
                    PdfTextFind[] result = null;
                    foreach (PdfPageBase page in pdf.Pages)
                    {
                        result = page.FindText(search, false, true).Finds;
                        foreach (PdfTextFind find in result)
                        {
                            find.ApplyHighLight();
                        }
                    }
                    pdf.SaveToFile(saveFilePath);
                    Process.Start(saveFilePath);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["obj"]))
            {
                var docId = Convert.ToInt32(this.Request.QueryString["obj"]);
                var targetFolder = "../../DocumentLibrary/TienSi";

                // const string TargetFolder = "../../DocumentResource/DCR";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/TienSi";
                var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };

                var listUpload = docuploader.UploadedFiles;
                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileName = docFile.FileName;
                        //var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;

                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(targetFolder), docFileName);

                        // Path file to download from server
                        var serverFilePath = serverFolder + "/" + docFileName;
                        var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                        docFile.SaveAs(saveFilePath);

                        var attachFile = new TaiLieuTienSi()
                        {
                            TienSiID = docId,
                            FileName = docFileName,
                            FileExtension = fileExt,
                            FilePath = serverFilePath,
                            FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                            FileSize = (double)docFile.ContentLength / 1024,
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now
                        };

                        this.taiLieuTienSiService.Insert(attachFile);
                    }
                }
            }

            this.docuploader.UploadedFiles.Clear();

            this.grdDocument.Rebind();
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var obj = this.taiLieuTienSiService.GetById(docId);
            if (File.Exists(Server.MapPath(obj.FilePath)))
            {
                File.Delete(Server.MapPath(obj.FilePath));
            }
            this.taiLieuTienSiService.Delete(docId);

            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<TaiLieuTienSi> list = new List<TaiLieuTienSi>();
            if (!string.IsNullOrEmpty(Request.QueryString["obj"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["obj"]);
                if (!string.IsNullOrEmpty(Request.QueryString["search"]))
                {
                    string search = Request.QueryString["search"];
                    //Search query to retrieve info
                    var response = elasticClient.Search<documentElastic>(s => s
                        .Query(q => q
                        .Match(m => m
                        .Field(f => f.content).Query(search + "*")))
                        .Size(10000));
                    List<string> docList = new List<string>();
                    foreach (var hit in response.Hits)
                    {
                        //string doc = hit.Source.file.filename.Substring(0, hit.Source.file.filename.LastIndexOf('.'));
                        docList.Add(hit.Source.file.filename);
                        //lblResult.Text += doc + "<br/>";
                    }
                    list = this.taiLieuTienSiService.AdvanceSearch(docList, docId);
                }
                else
                {
                    list = this.taiLieuTienSiService.GetByTienSi(docId);
                }
            }
            grdDocument.DataSource = list;
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}