﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewImage.aspx.cs" Inherits="EDMs.Controls.Pages.ViewImage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    
    <script type="text/javascript">

    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="overflow:auto !important">
        <div align="center" style="width: 100%">
      <%--  <asp:Image runat="server" ID="imgDocument" Width="100%"/>--%>
         <input type="image" runat="server" id="image">
        </div>

    </form>
</body>
</html>
