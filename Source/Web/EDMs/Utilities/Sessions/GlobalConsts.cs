﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EDMs.Utilities.Sessions
{
    public static class GlobalConsts
    {
        #region CommonIds

        public const int AdminRoleId = 1;
        public const int AdminUserId = 1;
        
        #endregion

        #region Paths

        public const string LoginFormPath = "~/Controls/Systems/Login.aspx";
        public const string SystemUserControlPath = "~/Controls/System/UserListControl.ascx";
        public const string SystemRoleControlPath = "~/Controls/System/RoleListControl.ascx";
        public const string SystemPermissionControlPath = "~/Controls/System/PermissionControl.ascx";
        public const string SystemPermissionDataControlPath = "~/Controls/System/PermissionDataControl.ascx";

        #endregion
    }
}