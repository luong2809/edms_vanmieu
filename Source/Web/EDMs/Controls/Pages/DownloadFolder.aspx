﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadFolder.aspx.cs" Inherits="EDMs.Controls.Pages.DownloadFolder" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

          function CancelEdit() {
              var oWindow = GetRadWindow();
              oWindow.argument = null;
              oWindow.close();
              document.location.reload();
              return false;
        }


    </script>
    <title></title>

</head>
<body>
    <form id="form1" runat="server" style=" height:115px; width:200px; background-image: url('Images/ezgif-4.gif');">
        <asp:UpdateProgress ID="UpdateProgress1"  runat="server"></asp:UpdateProgress>
      <br />
      <asp:Label runat="server" ID="lbMeseger"></asp:Label>

    </form>
</body>
</html>
