﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site_VM.Master" AutoEventWireup="true" CodeBehind="DanhNhanPage.aspx.cs" Inherits="EDMs.Controls.Pages.DanhNhanPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <style>
        .span-page {
            font-size: 28px;
            color: #B51F09;
        }

        .div-tool {
            height: 70px;
            padding-right: 15px;
        }

        .textbox-search {
            border: 0;
            height: 40px;
            border-radius: 3px 0 0 3px;
            padding-left: 10px;
        }

        .no-padding-col {
            padding-left: 0;
            padding-right: 0;
        }

        .div-search {
            height: 40px;
            width: 40px;
            background-color: #fff;
        }

        .btn-search {
            background-image: url(../../Images/search-do.svg);
            background-size: 19px;
            background-repeat: no-repeat;
            background-position: center;
            border: 0;
            box-shadow: none;
            border-radius: 0 3px 3px 0;
        }

        .ddl-bia {
            height: 26px !important;
        }

        #ctl00_contentTool_ctl00_contentBody_grdDocumentPanel {
            height: 100% !important;
        }

        .RadComboBox_Bootstrap .rcbHovered {
            border-color: #4E0B0B !important;
            color: #343434 !important;
            background-color: #fff !important;
        }

        .RadComboBoxDropDown_Bootstrap .rcbItem {
            color: #343434 !important;
        }

        .RadComboBoxDropDown_Bootstrap .rcbHovered {
            color: #fff !important;
            background-color: #4E0B0B !important;
        }

        .textbox-edit-form {
            width: 300px;
            float: left;
            border-color: #D3D3D3 #D3D3D3 #D3D3D3 #4E0B0B;
            color: #000000;
            font: 12px "segoe ui";
            border-width: 1px;
            border-style: solid;
            border-left-width: 5px;
            border-radius: 4px;
            padding: 2px 1px 3px;
            vertical-align: middle;
            margin: 0;
            padding-left: 5px;
            padding-right: 5px;
        }

        .textbox-required {
            border-left-color: #B51F09;
        }

        .row-edit-form {
            margin-bottom: 5px;
            margin-left: 0;
            margin-right: 0;
        }

        .col-edit-form {
            padding-left: 0;
            padding-right: 0;
        }

        .label-edit-form {
            color: #303030;
            font-size: 13px;
            white-space: pre-wrap;
        }

        .row-header {
            border-bottom: 1px solid;
            border-bottom-color: #d3d3d3;
        }

        .text-name {
            font-size: 16pt;
            font-weight: bold;
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentTool" runat="server">
    <%-- manager code js --%>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $('iframe').load(function () {
                    //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }
        </script>
    </telerik:RadCodeBlock>

    <%-- manager ajax page --%>
    <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="pnTieuSu">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnTieuSu" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="pnVinhDanh">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lblVinhDanh" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="pnTacPham">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnTacPham" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="pnThongTinChung">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnThongTinChung" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <%-- loading page --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>

    <%-- hidden field --%>
    <asp:HiddenField runat="server" ID="lblId" />
    <asp:HiddenField runat="server" ID="lblBiaId" />

    <%-- manager window popup --%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditPage" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="570" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="AttachFile" runat="server" Title="File" VisibleStatusbar="false" Height="450" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="Inforgraphic" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="2500" Width="1500" Left="150px" Style="z-index: 8001" CenterIfModal="true" Modal="true" KeepInScreenBounds="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="row justify-content-between align-items-center">
        <div class="col-lg-4">
            <span class="span-page">DANH NHÂN</span>
        </div>
        <div class="col-lg-auto">
            <div class="row align-items-center">
                <div class="col-lg-auto" style="padding-right: 0">
                    <div class="row align-items-center div-tool">
                        <telerik:RadButton ID="btnAdd" Visible="false" runat="server" Text="TẠO MỚI" Skin="Bootstrap" RenderMode="Lightweight" Height="40px" Width="130px" AutoPostBack="false" OnClientClicking="ShowAddForm">
                            <Icon PrimaryIconUrl="~/Images/add-trang.svg" PrimaryIconTop="6px" PrimaryIconLeft="8px" PrimaryIconHeight="19px" PrimaryIconWidth="19px" />
                        </telerik:RadButton>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <telerik:RadToolBar ID="CustomerMenu" Visible="false" runat="server" Skin="Bootstrap" Width="100%" Height="40px" OnClientButtonClicking="OnClientButtonClicking">
                        <Items>
                            <telerik:RadToolBarDropDown ImageUrl="~/Images/settings-do.svg" DropDownWidth="150px">
                                <Buttons>
                                </Buttons>
                            </telerik:RadToolBarDropDown>
                        </Items>
                    </telerik:RadToolBar>
                </div>
                <div class="col-lg-auto">
                    <div class="row align-items-center div-tool">
                        <telerik:RadComboBox ID="ddlDanhNhan" runat="server" Skin="Bootstrap" RenderMode="Lightweight" InputCssClass="ddl-bia" Style="width: 100% !important;" MaxHeight="450px" AutoPostBack="true" OnSelectedIndexChanged="ddlDanhNhan_SelectedIndexChanged"></telerik:RadComboBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">

    <div class="row full-background">
        <div class="col-9">
            <asp:Panel ID="pnTieuSu" runat="server">
                <div class="row row-edit-form row-header justify-content-between">
                    <div class="col-2 col-edit-form">
                        <asp:Label ID="Label2" runat="server" Width="100%" Text="Tiểu Sử"></asp:Label>
                    </div>
                    <div class="col-auto col-edit-form">
                        <asp:ImageButton ID="btnSaveTieuSu" runat="server" Width="24px" Height="24px" OnClick="btnSaveTieuSu_Click" ImageUrl="/Images/save-do.svg" Visible="false" />
                        <asp:ImageButton ID="btnEditTieuSu" runat="server" Width="24px" Height="24px" CommandName="edit" OnClick="btnEditTieuSu_Click" ImageUrl="/Images/edit-do.svg" />
                    </div>
                </div>
                <div class="row row-edit-form">
                    <div class="col col-edit-form">
                        <asp:Label ID="lblTieuSu" CssClass="label-edit-form" runat="server" Width="100%" Text="-"></asp:Label>
                        <asp:TextBox ID="txtTieuSu" runat="server" Width="100%" TextMode="MultiLine" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnVinhDanh" runat="server">
                <div class="row row-edit-form row-header justify-content-between">
                    <div class="col-2 col-edit-form">
                        <asp:Label ID="Label1" runat="server" Width="100%" Text="Vinh Danh"></asp:Label>
                    </div>
                    <div class="col-auto col-edit-form">
                        <asp:ImageButton ID="btnSaveVinhDanh" runat="server" Width="24px" Height="24px" OnClick="btnSaveVinhDanh_Click" ImageUrl="/Images/save-do.svg" Visible="false" />
                        <asp:ImageButton ID="btnEditVinhDanh" runat="server" Width="24px" Height="24px" CommandName="edit" OnClick="btnEditVinhDanh_Click" ImageUrl="/Images/edit-do.svg" />
                    </div>
                </div>
                <div class="row row-edit-form">
                    <div class="col col-edit-form">
                        <asp:Label ID="lblVinhDanh" CssClass="label-edit-form" runat="server" Width="100%" Text="-"></asp:Label>
                        <asp:TextBox ID="txtVinhDanh" runat="server" Width="100%" TextMode="MultiLine" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnTacPham" runat="server">
                <div class="row row-edit-form row-header justify-content-between">
                    <div class="col-2 col-edit-form">
                        <asp:Label ID="Label4" runat="server" Width="100%" Text="Tác Phẩm"></asp:Label>
                    </div>
                    <div class="col-auto col-edit-form">
                        <asp:ImageButton ID="btnSaveTacPham" runat="server" Width="24px" Height="24px" OnClick="btnSaveTacPham_Click" ImageUrl="/Images/save-do.svg" Visible="false" />
                        <asp:ImageButton ID="btnEditTacPham" runat="server" Width="24px" Height="24px" CommandName="edit" OnClick="btnEditTacPham_Click" ImageUrl="/Images/edit-do.svg" />
                    </div>
                </div>
                <div class="row row-edit-form">
                    <div class="col col-edit-form">
                        <asp:Label ID="lblTacPham" CssClass="label-edit-form" runat="server" Width="100%" Text="-"></asp:Label>
                        <asp:TextBox ID="txtTacPham" runat="server" Width="100%" TextMode="MultiLine" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
            </asp:Panel>
        </div>

        <div class="col-3" style="border: 1px solid #d3d3d3">
            <asp:Panel ID="pnThongTinChung" runat="server">
                <div class="row row-edit-form row-header justify-content-between">
                    <div class="col col-edit-form">
                        <div class="text-center text-name">
                            <asp:Label ID="lblTen" runat="server" Width="100%" Text="-"></asp:Label>
                        </div>
                    </div>
                    <div class="col-auto col-edit-form">
                        <asp:ImageButton ID="btnSaveThongTinChung" runat="server" Width="24px" Height="24px" OnClick="btnSaveThongTinChung_Click" ImageUrl="/Images/save-do.svg" Visible="false" />
                        <asp:ImageButton ID="btnEditThongTinChung" runat="server" Width="24px" Height="24px" CommandName="edit" OnClick="btnEditThongTinChung_Click" ImageUrl="/Images/edit-do.svg" />
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <asp:Image ID="imgDanhNhan" runat="server" Width="200px" Height="350px" />
                        <telerik:RadAsyncUpload ID="uploadImageDanhNhan" runat="server" MultipleFileSelection="Disabled" Visible="false"></telerik:RadAsyncUpload>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <asp:Label ID="Label14" runat="server" Width="100%" Text="Thông tin chung"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <span>Danh</span>
                    </div>
                    <div class="col">
                        <asp:Label ID="lblDanh" runat="server" Width="100%" Text="-"></asp:Label>
                        <asp:TextBox ID="txtDanh" runat="server" Width="100%" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <span>Tự</span>
                    </div>
                    <div class="col">
                        <asp:Label ID="lblTu" runat="server" Width="100%" Text="-"></asp:Label>
                        <asp:TextBox ID="txtTu" runat="server" Width="100%" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <span>Hiệu</span>
                    </div>
                    <div class="col">
                        <asp:Label ID="lblHieu" runat="server" Width="100%" Text="-"></asp:Label>
                        <asp:TextBox ID="txtHieu" runat="server" Width="100%" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <span>Thân phụ</span>
                    </div>
                    <div class="col">
                        <asp:Label ID="lblCha" runat="server" Width="100%" Text="Chu Văn Thiện"></asp:Label>
                        <asp:TextBox ID="txtCha" runat="server" Width="100%" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <span>Thân mẫu</span>
                    </div>
                    <div class="col">
                        <asp:Label ID="lblMe" runat="server" Width="100%" Text="Lê Thị Chuân"></asp:Label>
                        <asp:TextBox ID="txtMe" runat="server" Width="100%" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <span>Sinh</span>
                    </div>
                    <div class="col">
                        <asp:Label ID="lblSinh" runat="server" Width="100%" Text="25/8/1292"></asp:Label>
                        <asp:TextBox ID="txtSinh" runat="server" Width="100%" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <span>Mất</span>
                    </div>
                    <div class="col">
                        <asp:Label ID="lblMat" runat="server" Width="100%" Text="1370"></asp:Label>
                        <asp:TextBox ID="txtMat" runat="server" Width="100%" CssClass="textbox-edit-form textbox-required" Visible="false" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>
