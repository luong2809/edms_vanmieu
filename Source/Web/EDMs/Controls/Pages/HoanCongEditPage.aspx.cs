﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Data.Entities;
using System.Linq;
using System.IO;
using Telerik.Web.UI;
using System.Collections.Generic;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Utilities;

namespace EDMs.Controls.Pages
{
    public partial class HoanCongEditPage : Page
    {
        private readonly UserService userService = new UserService();
        private readonly TaiLieuHoanCongService taiLieuHoanCongService = new TaiLieuHoanCongService();
        private readonly DisciplineService disciplineService = new DisciplineService();
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();
        private readonly ConstructionTreeService constructionTreeService = new ConstructionTreeService();

        public HoanCongEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboBox();

                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.taiLieuHoanCongService.GetById(Convert.ToInt32(this.Request.QueryString["obj"]));
                    if (obj != null)
                    {
                        var rtvConstruction = (RadTreeView)this.ddlConstruction.Items[0].FindControl("rtvConstruction");
                        if (rtvConstruction != null)
                        {
                            var selectedNode = rtvConstruction.GetAllNodes().First(t => t.Value == obj.ConstructionID.ToString());
                            if (selectedNode != null)
                            {
                                rtvConstruction.GetAllNodes().First(t => t.Value == obj.ConstructionID.ToString()).Selected = true;
                                this.ExpandParentNode(selectedNode);
                                this.ddlConstruction.Items[0].Text = selectedNode.Text;
                            }
                        }
                        this.txtYear.Text = obj.Year.ToString();
                        this.txtDocumentNumber.Text = obj.DocumentNumber;
                        this.txtDecription.Text = obj.Description;
                        this.ddlDocumentType.SelectedValue = obj.DocTypeID.ToString();
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void ExpandParentNode(RadTreeNode node)
        {
            if (node.ParentNode != null)
            {
                node.ParentNode.Expanded = true;
                this.ExpandParentNode(node.ParentNode);
            }
        }

        public void LoadComboBox()
        {
            var doctypeList = this.documentTypeService.GetAll();
            this.ddlDocumentType.DataValueField = "ID";
            this.ddlDocumentType.DataTextField = "Name";
            this.ddlDocumentType.DataSource = doctypeList;
            this.ddlDocumentType.DataBind();

            var listOptionalTypeDetail = this.constructionTreeService.GetAll();
            var rtvConstruction = (RadTreeView)this.ddlConstruction.Items[0].FindControl("rtvConstruction");
            if (rtvConstruction != null)
            {
                rtvConstruction.DataSource = listOptionalTypeDetail;
                rtvConstruction.DataFieldParentID = "ParentId";
                rtvConstruction.DataTextField = "Name";
                rtvConstruction.DataValueField = "ID";
                rtvConstruction.DataFieldID = "ID";
                rtvConstruction.DataBind();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };

                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.taiLieuHoanCongService.GetById(docId);
                    if (obj != null)
                    {
                        int year;
                        int.TryParse(this.txtYear.Text.Trim(), out year);
                        obj.Year = year;
                        obj.DocumentNumber = this.txtDocumentNumber.Text.Trim();
                        obj.Description = this.txtDecription.Text.Trim();
                        var rtvConstruction = (RadTreeView)this.ddlConstruction.Items[0].FindControl("rtvConstruction");
                        if (rtvConstruction != null && rtvConstruction.SelectedNode != null)
                        {
                            obj.ConstructionID = Convert.ToInt32(rtvConstruction.SelectedNode.Value);
                            obj.ConstructionName = rtvConstruction.SelectedNode.Text;
                        }
                        if (ddlDocumentType.SelectedItem != null)
                        {
                            obj.DocTypeID = Convert.ToInt32(this.ddlDocumentType.SelectedValue);
                            obj.DocTypeName = this.ddlDocumentType.SelectedItem.Text;
                        }
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;

                        foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                        {
                            var docFileName = Utility.RemoveAllSpecialCharacter(docFile.FileName);
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                            var oldfilePath = Server.MapPath(obj.FilePath);
                            var newServerPath = "/HoanCong/" + filename;
                            var newFilePath = Server.MapPath("/HoanCong/") + filename;
                            docFile.SaveAs(newFilePath);
                            obj.FileExtension = fileExt;
                            obj.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png";
                            obj.FileSize = (double)docFile.ContentLength / 1024;
                            obj.HasAttachFile = true;
                            obj.FilePath = newServerPath;
                            if (File.Exists(oldfilePath))
                            {
                                File.Delete(oldfilePath);
                            }
                        }
                        this.taiLieuHoanCongService.Update(obj);
                    }
                }
                else
                {
                    var obj = new TaiLieuHoanCong();
                    int year;
                    int.TryParse(this.txtYear.Text.Trim(), out year);
                    obj.Year = year;
                    obj.DocumentNumber = this.txtDocumentNumber.Text.Trim();
                    obj.Description = this.txtDecription.Text.Trim();
                    var rtvConstruction = (RadTreeView)this.ddlConstruction.Items[0].FindControl("rtvConstruction");
                    if (rtvConstruction != null && rtvConstruction.SelectedNode != null)
                    {
                        obj.ConstructionID = Convert.ToInt32(rtvConstruction.SelectedNode.Value);
                        obj.ConstructionName = rtvConstruction.SelectedNode.Text;
                    }
                    if (ddlDocumentType.SelectedItem != null)
                    {
                        obj.DocTypeID = Convert.ToInt32(this.ddlDocumentType.SelectedValue);
                        obj.DocTypeName = this.ddlDocumentType.SelectedItem.Text;
                    }
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    var transId = this.taiLieuHoanCongService.Insert(obj);
                    if (transId != null)
                    {
                        foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                        {
                            var docFileName = Utility.RemoveAllSpecialCharacter(docFile.FileName);
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            var filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                            var oldfilePath = Server.MapPath(obj.FilePath);
                            var newServerPath = "/HoanCong/" + filename;
                            var newFilePath = Server.MapPath("/HoanCong/") + filename;
                            docFile.SaveAs(newFilePath);
                            obj.FileExtension = fileExt;
                            obj.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png";
                            obj.FileSize = (double)docFile.ContentLength / 1024;
                            obj.HasAttachFile = true;
                            obj.FilePath = newServerPath;
                            if (File.Exists(oldfilePath))
                            {
                                File.Delete(oldfilePath);
                            }
                            this.taiLieuHoanCongService.Update(obj);
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }
        protected void rtvContruction_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "~/Images/contruction.svg";
        }
    }
}