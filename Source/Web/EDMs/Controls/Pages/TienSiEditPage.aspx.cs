﻿
using System;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Data.Entities;

namespace EDMs.Controls.Pages
{
    public partial class TienSiEditPage : Page
    {
        private readonly UserService userService = new UserService();
        private readonly TienSiService tienSiService = new TienSiService();
        private readonly BiaTienSiService biaTienSiService = new BiaTienSiService();

        public TienSiEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadCombobox();
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    this.CreatedInfo.Visible = true;
                    var obj = this.tienSiService.GetById(Convert.ToInt32(this.Request.QueryString["obj"]));
                    if (obj != null)
                    {
                        this.ddlBia.SelectedValue = obj.BiaID.ToString();
                        this.txtHoten.Text = obj.HoTen;
                        this.txtNamSinh.Text = obj.NamSinh.ToString();
                        this.txtNamMat.Text = obj.NamMat.ToString();
                        this.txtNamthi.Text = obj.NamThiDo.ToString();
                        this.txtSuNghiep.Text = obj.SuNghiep;
                        this.txtTrieudai.Text = obj.TrieuDai;
                        this.txtChucquan.Text = obj.ChucQuan;
                        this.txtTieusu.Text = obj.TieuSu;
                        this.txtGiadinh.Text = obj.GiaDinh;
                        this.txtDongho.Text = obj.DongHo;
                        this.txtQuequan.Text = obj.QueQuan;
                        this.txtThongtindiaphuong.Text = obj.ThongTinChiTietDiaPhuong;
                        this.txtThongtinvanhoa.Text = obj.ThongTinChiTietVanHoaDiaPhuong;
                        this.txtDonggop.Text = obj.DongGop;
                        this.txtTacphamdelai.Text = obj.TacPhamDeLai;
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    if(!string.IsNullOrEmpty(this.Request.QueryString["bia"]))
                    {
                        this.ddlBia.SelectedValue = this.Request.QueryString["bia"];
                    }
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void LoadCombobox()
        {
            var biaList = this.biaTienSiService.GetAll();
            biaList.Insert(0, new BiaTienSi() { ID = 0 });
            this.ddlBia.DataSource = biaList;
            this.ddlBia.DataTextField = "TenBia";
            this.ddlBia.DataValueField = "ID";
            this.ddlBia.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var Id = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.tienSiService.GetById(Id);
                    if (obj != null)
                    {
                        int bia;
                        int.TryParse(this.ddlBia.SelectedValue, out bia);
                        obj.BiaID = bia;
                        obj.HoTen = this.txtHoten.Text.Trim();
                        int namSinh;
                        int.TryParse(this.txtNamSinh.Text.Trim(), out namSinh);
                        obj.NamSinh = namSinh;
                        int namMat;
                        int.TryParse(this.txtNamMat.Text.Trim(), out namMat);
                        obj.NamMat = namMat;
                        int namThi;
                        int.TryParse(this.txtNamthi.Text.Trim(), out namThi);
                        obj.NamThiDo = namThi;
                        obj.SuNghiep = this.txtSuNghiep.Text;
                        obj.TrieuDai = this.txtTrieudai.Text.Trim();
                        obj.ChucQuan = this.txtChucquan.Text.Trim();
                        obj.TieuSu = this.txtTieusu.Text.Trim();
                        obj.GiaDinh = this.txtGiadinh.Text.Trim();
                        obj.DongHo = this.txtDongho.Text.Trim();
                        obj.QueQuan = this.txtQuequan.Text.Trim();
                        obj.ThongTinChiTietDiaPhuong = this.txtThongtindiaphuong.Text.Trim();
                        obj.ThongTinChiTietVanHoaDiaPhuong = this.txtThongtinvanhoa.Text.Trim();
                        obj.DongGop = this.txtDonggop.Text.Trim();
                        obj.TacPhamDeLai = this.txtTacphamdelai.Text.Trim();
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        this.tienSiService.Update(obj);
                    }
                }
                else
                {
                    var obj = new TienSi();
                    int bia;
                    int.TryParse(this.ddlBia.SelectedValue, out bia);
                    obj.BiaID = bia;
                    obj.HoTen = this.txtHoten.Text.Trim();
                    int namSinh;
                    int.TryParse(this.txtNamSinh.Text.Trim(), out namSinh);
                    obj.NamSinh = namSinh;
                    int namMat;
                    int.TryParse(this.txtNamMat.Text.Trim(), out namMat);
                    obj.NamMat = namMat;
                    int namThi;
                    int.TryParse(this.txtNamthi.Text.Trim(), out namThi);
                    obj.NamThiDo = namThi;
                    obj.SuNghiep = this.txtSuNghiep.Text;
                    obj.TrieuDai = this.txtTrieudai.Text.Trim();
                    obj.ChucQuan = this.txtChucquan.Text.Trim();
                    obj.TieuSu = this.txtTieusu.Text.Trim();
                    obj.GiaDinh = this.txtGiadinh.Text.Trim();
                    obj.DongHo = this.txtDongho.Text.Trim();
                    obj.QueQuan = this.txtQuequan.Text.Trim();
                    obj.ThongTinChiTietDiaPhuong = this.txtThongtindiaphuong.Text.Trim();
                    obj.ThongTinChiTietVanHoaDiaPhuong = this.txtThongtinvanhoa.Text.Trim();
                    obj.DongGop = this.txtDonggop.Text.Trim();
                    obj.TacPhamDeLai = this.txtTacphamdelai.Text.Trim();
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    this.tienSiService.Insert(obj);
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }
    }
}