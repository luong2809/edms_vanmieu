﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using System.IO;
using System.Web.Hosting;
using EDMs.Utilities.Sessions;

namespace EDMs.Controls.Pages
{
    public partial class DanhNhanPage : Page
    {
        private readonly DanhNhanService danhNhanService = new DanhNhanService();

        Nest.ConnectionSettings connectionSettings;
        Nest.ElasticClient elasticClient;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            connectionSettings = new Nest.ConnectionSettings()
                .DefaultMappingFor<documentElastic>(i => i
                .IndexName("vanmieu")
                .TypeName("_doc"))
                .EnableDebugMode()
                .PrettyJson()
                .RequestTimeout(TimeSpan.FromMinutes(2));
            elasticClient = new Nest.ElasticClient(connectionSettings);
            if (!this.Page.IsPostBack)
            {
                this.LoadCombobox();
                this.LoadData();
            }
        }

        private void LoadCombobox()
        {
            var danhNhanList = this.danhNhanService.GetAll();
            this.ddlDanhNhan.DataSource = danhNhanList;
            this.ddlDanhNhan.DataTextField = "Danh";
            this.ddlDanhNhan.DataValueField = "ID";
            this.ddlDanhNhan.DataBind();
            this.ddlDanhNhan.SelectedIndex = 0;
        }

        private void LoadData()
        {
            var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
            if (obj != null)
            {
                this.imgDanhNhan.ImageUrl = obj.FilePath;
                this.lblTen.Text = obj.Danh;
                this.lblDanh.Text = obj.Danh;
                this.lblTu.Text = obj.Tu;
                this.lblHieu.Text = obj.Hieu;
                this.lblCha.Text = obj.Cha;
                this.lblMe.Text = obj.Me;
                this.lblSinh.Text = obj.NamSinh.ToString();
                this.lblMat.Text = obj.NamMat.ToString();
                this.lblTieuSu.Text = obj.TieuSu;
                this.lblVinhDanh.Text = obj.VinhDanh;
                this.lblTacPham.Text = obj.TacPham;
            }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }

        protected void ddlDanhNhan_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            this.lblBiaId.Value = this.ddlDanhNhan.SelectedValue;
            this.LoadData();
        }

        protected void btnEditTieuSu_Click(object sender, EventArgs e)
        {
            if (btnEditTieuSu.CommandName == "edit")
            {
                LoadEditTieuSu();
            }
            else
            {
                var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
                if (obj != null)
                {
                    LoadShowTieuSu(obj);
                }
            }
        }

        protected void btnEditThongTinChung_Click(object sender, ImageClickEventArgs e)
        {
            if (btnEditThongTinChung.CommandName == "edit")
            {
                LoadEditThongTinChung();
            }
            else
            {
                var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
                if (obj != null)
                {
                    LoadShowThongTinChung(obj);
                }
            }
        }

        protected void btnEditTacPham_Click(object sender, ImageClickEventArgs e)
        {
            if (btnEditTacPham.CommandName == "edit")
            {
                LoadEditTacPham();
            }
            else
            {
                var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
                if (obj != null)
                {
                    LoadShowTacPham(obj);
                }
            }
        }

        protected void btnEditVinhDanh_Click(object sender, ImageClickEventArgs e)
        {
            if (btnEditVinhDanh.CommandName == "edit")
            {
                LoadEditVinhDanh();
            }
            else
            {
                var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
                if (obj != null)
                {
                    LoadShowVinhDanh(obj);
                }
            }
        }

        protected void btnSaveTieuSu_Click(object sender, ImageClickEventArgs e)
        {
            var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
            if (obj != null)
            {
                obj.TieuSu = this.txtTieuSu.Text;
                this.danhNhanService.Update(obj);
            }
            LoadShowTieuSu(obj);
        }

        protected void btnSaveVinhDanh_Click(object sender, ImageClickEventArgs e)
        {
            var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
            if (obj != null)
            {
                obj.VinhDanh = this.txtVinhDanh.Text;
                this.danhNhanService.Update(obj);
            }
            LoadShowVinhDanh(obj);
        }

        protected void btnSaveTacPham_Click(object sender, ImageClickEventArgs e)
        {
            var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
            if (obj != null)
            {
                obj.TacPham = this.txtTacPham.Text;
                this.danhNhanService.Update(obj);
            }
            LoadShowTacPham(obj);
        }

        protected void btnSaveThongTinChung_Click(object sender, ImageClickEventArgs e)
        {
            var obj = this.danhNhanService.GetById(Convert.ToInt32(ddlDanhNhan.SelectedValue));
            if (obj != null)
            {
                var listUpload = uploadImageDanhNhan.UploadedFiles;
                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileName = docFile.FileName;
                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath("/Images/DanhNhan"), docFileName);

                        // Path file to download from server
                        var serverFilePath = "/Images/DanhNhan" + "/" + docFileName;
                        docFile.SaveAs(saveFilePath);
                        obj.FileName = docFileName;
                        obj.FilePath = serverFilePath;
                    }
                }
                obj.Danh = this.txtDanh.Text;
                obj.Tu = this.txtTu.Text;
                obj.Hieu = this.txtHieu.Text;
                obj.Cha = this.txtCha.Text;
                obj.Me = this.txtMe.Text;
                obj.NamSinh = Convert.ToInt32(this.txtSinh.Text);
                obj.NamMat = Convert.ToInt32(this.txtMat.Text);
                this.danhNhanService.Update(obj);
            }
            LoadShowThongTinChung(obj);
        }

        private void LoadShowTieuSu(DanhNhan obj)
        {
            this.lblTieuSu.Visible = true;
            this.txtTieuSu.Visible = false;
            this.lblTieuSu.Text = obj.TieuSu;
            btnEditTieuSu.CommandName = "edit";
            btnEditTieuSu.ImageUrl = "/Images/edit-do.svg";
            btnSaveTieuSu.Visible = false;
        }

        private void LoadEditTieuSu()
        {
            this.lblTieuSu.Visible = false;
            this.txtTieuSu.Visible = true;
            this.txtTieuSu.Text = lblTieuSu.Text;
            //int numLines = lblTieuSu.Text.Length - lblTieuSu.Text.Replace(Environment.NewLine, string.Empty).Length;
            int numLines = lblTieuSu.Text.Split('\n').Length;
            txtTieuSu.Rows = numLines;
            btnEditTieuSu.CommandName = "cancel";
            btnEditTieuSu.ImageUrl = "/Images/cancel-do.svg";
            btnSaveTieuSu.Visible = true;
        }

        private void LoadShowVinhDanh(DanhNhan obj)
        {
            this.lblVinhDanh.Visible = true;
            this.txtVinhDanh.Visible = false;
            this.lblVinhDanh.Text = obj.VinhDanh;
            btnEditVinhDanh.CommandName = "edit";
            btnEditVinhDanh.ImageUrl = "/Images/edit-do.svg";
            btnSaveVinhDanh.Visible = false;
        }

        private void LoadEditVinhDanh()
        {
            this.lblVinhDanh.Visible = false;
            this.txtVinhDanh.Visible = true;
            this.txtVinhDanh.Text = lblVinhDanh.Text;
            //int numLines = lblVinhDanh.Text.Length - lblVinhDanh.Text.Replace(Environment.NewLine, string.Empty).Length;
            int numLines = lblVinhDanh.Text.Split('\n').Length;
            txtVinhDanh.Rows = numLines;

            btnEditVinhDanh.CommandName = "cancel";
            btnEditVinhDanh.ImageUrl = "/Images/cancel-do.svg";
            btnSaveVinhDanh.Visible = true;
        }

        private void LoadShowTacPham(DanhNhan obj)
        {
            this.lblTacPham.Visible = true;
            this.txtTacPham.Visible = false;
            this.lblTacPham.Text = obj.TacPham;
            btnEditTacPham.CommandName = "edit";
            btnEditTacPham.ImageUrl = "/Images/edit-do.svg";
            btnSaveTacPham.Visible = false;
        }

        private void LoadEditTacPham()
        {
            this.lblTacPham.Visible = false;
            this.txtTacPham.Visible = true;
            this.txtTacPham.Text = lblTacPham.Text;
            //int numLines = lblTacPham.Text.Length - lblTacPham.Text.Replace(Environment.NewLine, string.Empty).Length;
            int numLines = lblTacPham.Text.Split('\n').Length;
            txtTacPham.Rows = numLines;
            btnEditTacPham.CommandName = "cancel";
            btnEditTacPham.ImageUrl = "/Images/cancel-do.svg";
            btnSaveTacPham.Visible = true;
        }

        private void LoadShowThongTinChung(DanhNhan obj)
        {
            this.imgDanhNhan.Visible = true;
            this.uploadImageDanhNhan.Visible = false;
            this.imgDanhNhan.ImageUrl = obj.FilePath;
            this.lblTen.Visible = true;
            this.lblTen.Text = obj.Danh;
            this.lblDanh.Visible = true;
            this.txtDanh.Visible = false;
            this.lblDanh.Text = obj.Danh;
            this.lblTu.Visible = true;
            this.txtTu.Visible = false;
            this.lblTu.Text = obj.Tu;
            this.lblHieu.Visible = true;
            this.txtHieu.Visible = false;
            this.lblHieu.Text = obj.Hieu;
            this.lblCha.Visible = true;
            this.txtCha.Visible = false;
            this.lblCha.Text = obj.Cha;
            this.lblMe.Visible = true;
            this.txtMe.Visible = false;
            this.lblMe.Text = obj.Me;
            this.lblSinh.Visible = true;
            this.txtSinh.Visible = false;
            this.lblSinh.Text = obj.NamSinh.ToString();
            this.lblMat.Visible = true;
            this.txtMat.Visible = false;
            this.lblMat.Text = obj.NamMat.ToString();
            btnEditThongTinChung.CommandName = "edit";
            btnEditThongTinChung.ImageUrl = "/Images/edit-do.svg";
            btnSaveThongTinChung.Visible = false;
        }

        private void LoadEditThongTinChung()
        {
            this.imgDanhNhan.Visible = false;
            this.uploadImageDanhNhan.Visible = true;
            this.lblTen.Visible = false;
            this.lblDanh.Visible = false;
            this.txtDanh.Visible = true;
            this.txtDanh.Text = lblDanh.Text;
            this.lblTu.Visible = false;
            this.txtTu.Visible = true;
            this.txtTu.Text = lblTu.Text;
            this.lblHieu.Visible = false;
            this.txtHieu.Visible = true;
            this.txtHieu.Text = lblHieu.Text;
            this.lblCha.Visible = false;
            this.txtCha.Visible = true;
            this.txtCha.Text = lblCha.Text;
            this.lblMe.Visible = false;
            this.txtMe.Visible = true;
            this.txtMe.Text = lblMe.Text;
            this.lblSinh.Visible = false;
            this.txtSinh.Visible = true;
            this.txtSinh.Text = lblSinh.Text;
            this.lblMat.Visible = false;
            this.txtMat.Visible = true;
            this.txtMat.Text = lblMat.Text;
            btnEditThongTinChung.CommandName = "cancel";
            btnEditThongTinChung.ImageUrl = "/Images/cancel-do.svg";
            btnSaveThongTinChung.Visible = true;
        }
    }
}