﻿using System;
using System.Web.UI;
using EDMs.Data.Entities;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Utilities;
using System.IO;

namespace EDMs.Controls.Systems
{
    public partial class CategoryEditPage : Page
    {
        private readonly UserService userService = new UserService();
        private readonly CategoryService categoryService = new CategoryService();
        private readonly FolderService folderService = new FolderService();

        public CategoryEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = categoryService.GetById(Convert.ToInt32(Request.QueryString["obj"]));
                    if (obj != null)
                    {
                        this.txtName.Text = obj.Name;
                        this.txtDescription.Text = obj.Description;
                        this.checkActive.Checked = obj.IsActive.GetValueOrDefault();
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var id = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.categoryService.GetById(id);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.IsActive = this.checkActive.Checked;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        this.categoryService.Update(obj);
                        var folderRoot = this.folderService.GetAllByCategory(obj.ID).Find(t => t.ParentID == null);
                        if (folderRoot != null)
                        {
                            folderRoot.Name = this.txtName.Text;
                            folderRoot.Description = this.txtName.Text;
                            this.folderService.Update(folderRoot);
                        }
                    }
                }
                else
                {
                    var obj = new Category();
                    obj.Name = this.txtName.Text.Trim();
                    obj.Description = this.txtDescription.Text.Trim();
                    obj.IsActive = this.checkActive.Checked;
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    var categoryId = this.categoryService.Insert(obj);
                    var folderGUID = Guid.NewGuid();
                    var folder = new Folder()
                    {
                        Name = this.txtName.Text,
                        Description = this.txtName.Text,
                        CategoryID = categoryId,
                        DirName = "HanhChinh" + "/" + folderGUID,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    var folId = this.folderService.Insert(folder);
                    Directory.CreateDirectory(Server.MapPath("/" + folder.DirName));
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }
    }
}