﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BiaEditPage.aspx.cs" Inherits="EDMs.Controls.Pages.BiaEditPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="/Content/bootstrap.min.css" />
    <link href="/Content/styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        body {
            background-image: linear-gradient(#E6E0AE,#FAF9EF);
            background-repeat: no-repeat;
        }

        .btn-save {
            width: 100%;
            height: 40px;
            background-color: #4E0B0B;
            color: #FAF9EF;
            border: 0;
            border-radius: 3px 4px;
            font-family: Roboto, sans-serif;
            font-size: 13px;
            cursor: pointer;
        }

        .dnnFormInfo {
            width: 100%;
        }

        .label-edit-form {
            color: #252525;
            font-size: 13px;
            text-transform: uppercase;
        }

        .textbox-edit-form {
            width: 300px;
            float: left;
            border-color: #D3D3D3 #D3D3D3 #D3D3D3 #4E0B0B;
            color: #000000;
            font: 12px "segoe ui";
            border-width: 1px;
            border-style: solid;
            border-left-width: 5px;
            border-radius: 4px;
            padding: 2px 1px 3px;
            vertical-align: middle;
            margin: 0;
            padding-left: 5px;
            padding-right: 5px;
        }

        .textbox-required {
            border-left-color: #B51F09;
        }

        .row-edit-form {
            margin-bottom: 5px;
        }

        .col-label-edit-form {
            padding-left: 0;
            padding-right: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row row-edit-form">
                <div class="dnnFormInfo">
                    <div class="dnnFormItem dnnFormHelp dnnClear">
                        <p class="dnnFormRequired" style="float: left;">
                            <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                        </p>
                        <div id="CreatedInfo" runat="server" visible="False">
                            <p class="dnnFormRequired" style="float: left;">
                                <asp:Label ID="lblCreated" runat="server"></asp:Label>
                                <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Bia</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtBia" runat="server" CssClass="textbox-edit-form textbox-required" />
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Năm khoa thi</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtNamtochuc" runat="server" CssClass="textbox-edit-form textbox-required" />
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Thời gian dựng bia</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtThoigiandungbia" runat="server" CssClass="textbox-edit-form textbox-required" />
                </div>
            </div>
            <div style="clear: both; font-size: 0;"></div>

            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Kiến trúc</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtKientruc" runat="server" CssClass="textbox-edit-form" />
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Người dựng bia</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtNguoithamgiadungbia" runat="server" TextMode="MultiLine" Rows="3" CssClass="textbox-edit-form" />
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Tiêu đề khoa thi</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtTieudekhoathi" runat="server" TextMode="MultiLine" Rows="3" CssClass="textbox-edit-form" />
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Quan tổ chức khoa thi</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtQuantochuc" runat="server" TextMode="MultiLine" Rows="3" CssClass="textbox-edit-form" />
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4 text-right col-label-edit-form">
                    <span class="label-edit-form">Cách thức tổ chức</span>
                </div>
                <div class="col-8">
                    <asp:TextBox ID="txtCachthuctochuc" runat="server" TextMode="MultiLine" Rows="3" CssClass="textbox-edit-form" />
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4">
                </div>
                <div class="col-8">
                    <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="#B51F09"></asp:Label>
                </div>
            </div>
            <div class="row justify-content-center" style="margin-top: 15px">
                <asp:Button ID="btnSave" runat="server" Text="LƯU" CssClass="btn-save" OnClick="btnSave_Click" Width="200px" />
            </div>
        </div>
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script src="/Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
            <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                function CloseAndRebind(args) {
                    GetRadWindow().BrowserWindow.refreshGrid(args);
                    GetRadWindow().close();
                }

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                    return oWindow;
                }
            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
