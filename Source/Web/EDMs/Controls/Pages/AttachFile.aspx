﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttachFile.aspx.cs" Inherits="EDMs.Controls.Pages.AttachFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../../Content/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../../Content/styles.css" />

    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        body {
            background-image: linear-gradient(#E6E0AE,#FAF9EF);
            background-repeat: no-repeat;
        }

        .btn-save {
            width: 100%;
            height: 40px;
            background-color: #4E0B0B;
            color: #FAF9EF;
            border: 0;
            border-radius: 3px 4px;
            /*font-family: Roboto, sans-serif;*/
            font-size: 13px;
            cursor: pointer;
        }

        .dnnFormInfo {
            width: 100%;
        }

        .row-edit-form {
            margin-bottom: 5px;
        }

        .col-label-edit-form {
            padding-left: 0;
            padding-right: 0;
        }

        .RadUpload_Default .ruFakeInput{
            width:160px!important;
            border-left-color: #B51F09 !important;
            border-width: 0 0 0 5px!important;
            border-radius: 4px!important;
        }
        .RadUpload .ruBrowse{
            background:#4E0B0B!important;
            color:#fff!important;
            border-radius: 4px!important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptBlock runat="server">
            <script src="../../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
            <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                function CloseAndRebind(args) {
                    GetRadWindow().BrowserWindow.refreshGrid(args);
                    GetRadWindow().close();
                }

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                    return oWindow;
                }
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>

        <div class="container-fluid">
            <div class="row row-edit-form">
                <div class="dnnFormInfo">
                    <div class="dnnFormItem dnnFormHelp dnnClear">
                        <p class="dnnFormRequired" style="float: left;">
                            <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4">
                    <span></span>
                </div>
                <div class="col-8">
                    <telerik:RadAsyncUpload runat="server" ID="docuploader" MultipleFileSelection="Automatic" TemporaryFileExpiration="05:00:00" EnableInlineProgress="true"
                        Localization-Cancel="Cancel" Localization-Remove="Remove" Localization-Select="Select">
                    </telerik:RadAsyncUpload>
                </div>
            </div>
            <div class="row justify-content-center" style="margin-top: 10px; margin-bottom:10px">
                <asp:Button ID="btnSave" runat="server" Text="LƯU" CssClass="btn-save" OnClick="btnSave_Click" Width="175px" />
            </div>
            <div class="row justify-content-center" runat="server" ID="blockError" Visible="False" style="margin-top: 10px; margin-bottom:10px">
                <asp:Label runat="server" ID="lblError" Width="500"></asp:Label>
            </div>
        </div>
    </form>
</body>
</html>
