﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using System.IO;
using System.Web.Hosting;
using EDMs.Utilities.Sessions;

namespace EDMs.Controls.Pages
{
    public class documentElastic //class object json from elastic
    {
        public string content { get; set; }
        public file file { get; set; }
    }

    public class file
    {
        public string filename { get; set; }
    }
    public partial class TienSiPage : Page
    {
        private readonly BiaTienSiService biaTienSiService = new BiaTienSiService();
        private readonly TaiLieuTienSiService taiLieuTienSiService = new TaiLieuTienSiService();
        private readonly TienSiService tienSiService = new TienSiService();
        Nest.ConnectionSettings connectionSettings;
        Nest.ElasticClient elasticClient;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            connectionSettings = new Nest.ConnectionSettings()
                .DefaultMappingFor<documentElastic>(i => i
                .IndexName("vanmieu")
                .TypeName("_doc"))
                .EnableDebugMode()
                .PrettyJson()
                .RequestTimeout(TimeSpan.FromMinutes(2));
            elasticClient = new Nest.ElasticClient(connectionSettings);
            if (!this.Page.IsPostBack)
            {
                this.LoadCombobox();
            }
        }

        private void LoadCombobox()
        {
            var biaList = this.biaTienSiService.GetAll();
            biaList.Insert(0, new BiaTienSi() { ID = 0, TenBia = "Tất cả bia" });
            this.ddlBia.DataSource = biaList;
            this.ddlBia.DataTextField = "TenBiaWithYear";
            this.ddlBia.DataValueField = "ID";
            this.ddlBia.DataBind();
            this.ddlBia.SelectedIndex = 0;
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
            else if(e.Argument=="sync")
            {
                string[] filePaths = Directory.GetFiles(Server.MapPath(@"\Imports\Template\"), "*.doc",
                                         SearchOption.AllDirectories);
                foreach (var item in filePaths)
                {
                    FileInfo file = new FileInfo(item);
                    var arrayFileName = file.Name.Split('.');
                    var obj = new TienSi();
                    obj.HoTen = arrayFileName[2].Trim();
                    int namthi;
                    int.TryParse(arrayFileName[0], out namthi);
                    obj.NamThiDo = namthi;
                    var tiensiID = this.tienSiService.Insert(obj);

                    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/TienSi";
                    var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };
                    var serverFilePath = serverFolder + "/" + file.Name;
                    var attachFile = new TaiLieuTienSi()
                    {
                        TienSiID = tiensiID,
                        FileName = file.Name,
                        FileExtension = arrayFileName[3],
                        FilePath = serverFilePath,
                        FileExtensionIcon = fileIcon.ContainsKey(arrayFileName[3].ToLower()) ? fileIcon[arrayFileName[3].ToLower()] : "~/images/otherfile.png",
                        FileSize = (double)file.Length / 1024,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    this.taiLieuTienSiService.Insert(attachFile);
                }
            }
        }

        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocuments(false, false);
        }

        protected void LoadDocuments(bool isbind, bool isAdvanceSearch)
        {
            List<TienSi> list = new List<TienSi>();
            if (isAdvanceSearch)
            {
                if (this.txtSearch.Text != "")
                {
                    //Search query to retrieve info
                    var response = elasticClient.Search<documentElastic>(s => s
                        .Query(q => q
                        .Match(m => m
                        .Field(f => f.content).Query(this.txtSearch.Text + "*")))
                        .Size(10000));
                    List<string> docList = new List<string>();
                    foreach (var hit in response.Hits)
                    {
                        //string doc = hit.Source.file.filename.Substring(0, hit.Source.file.filename.LastIndexOf('.'));
                        docList.Add(hit.Source.file.filename);
                        //lblResult.Text += doc + "<br/>";
                    }
                    var listDocFilter = this.taiLieuTienSiService.AdvanceSearch(docList);
                    var tienSiList = listDocFilter.Select(t => t.TienSiID.GetValueOrDefault()).Distinct().ToList();

                    list = this.tienSiService.GetByList(tienSiList);
                }
                else
                {
                    list = this.tienSiService.GetByBia(Convert.ToInt32(ddlBia.SelectedValue));
                }
            }
            else
            {
                list = this.tienSiService.GetByBia(Convert.ToInt32(ddlBia.SelectedValue));
            }
            grdDocument.DataSource = list;

            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.tienSiService.Delete(disId);

            this.grdDocument.Rebind();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var ID = item.GetDataKeyValue("ID");
                ImageButton btnEdit = (ImageButton)item["EditButton"].Controls[0];
                btnEdit.Attributes.Add("OnClick", "ShowEditForm(" + ID + ");return false;");
                ImageButton btnAttach = (ImageButton)item["AttachFile"].Controls[0];
                btnAttach.Attributes.Add("OnClick", "ShowAttachForm(" + ID + ");return false;");
                ImageButton btnInfographic = (ImageButton)item["Infographic"].Controls[0];
                btnInfographic.Attributes.Add("OnClick", "ShowInfographic(" + ID + ");return false;");
            }
        }

        protected void ddlBia_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            this.lblBiaId.Value = this.ddlBia.SelectedValue;
            this.LoadDocuments(true, false);
        }
    }
}