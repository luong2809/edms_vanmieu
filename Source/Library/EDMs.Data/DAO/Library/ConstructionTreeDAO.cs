﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstructionTreeDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class ConstructionTreeDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConstructionTreeDAO"/> class.
        /// </summary>
        public ConstructionTreeDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ConstructionTree> GetAll()
        {
            return EDMsDataContext.ConstructionTrees.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public ConstructionTree GetById(int id)
        {
            return this.EDMsDataContext.ConstructionTrees.FirstOrDefault(ob => ob.ID == id);
        }

        public List<ConstructionTree> GetByParent(int parentID)
        {
            return EDMsDataContext.ConstructionTrees.Where(t=>t.ParentId == parentID).OrderByDescending(t => t.ID).ToList();
        }

        public List<ConstructionTree> GetByList(List<int> list)
        {
            return EDMsDataContext.ConstructionTrees.Where(t => list.Contains(t.ID)).OrderByDescending(t => t.ID).ToList();
        }

        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(ConstructionTree ob)
        {
            try
            {
                EDMsDataContext.AddToConstructionTrees(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(ConstructionTree src)
        {
            try
            {
                ConstructionTree des = (from rs in this.EDMsDataContext.ConstructionTrees
                              where rs.ID == src.ID
                              select rs).First();

                des.Name = src.Name;
                des.Description = src.Description;
                des.ParentId = src.ParentId;
                des.RoleId = src.RoleId;
                des.Active = src.Active;

                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(ConstructionTree src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
