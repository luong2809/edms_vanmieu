﻿
namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    public class GroupFunctionService
    {
        private readonly GroupFunctionsDAO repo;

        public GroupFunctionService()
        {
            repo = new  GroupFunctionsDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        public List<GroupFunction> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public GroupFunction GetByID(int id)
        {
            return repo.GetById(id);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Get (Advances)

        public List<GroupFunction> GetByRoleId(int UserId)
        {
            return this.repo.GetAll().Where(ob => ob.MemberID != null && ob.MemberID.Trim().Split('$').Where(k => !string.IsNullOrEmpty(k.ToString
                 ())).Select(t => Convert.ToInt32(t)).Contains(UserId)).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the GroupFunctions.
        /// </summary>
        /// <param name="GroupFunctions">The GroupFunctions.</param>
        /// <returns></returns>
        public bool DeleteGroupFunctions(List<GroupFunction> GroupFunctions)
        {
            return repo.DeleteGroupFunctions(GroupFunctions);
        }

        /// <summary>
        /// Adds the GroupFunctions.
        /// </summary>
        /// <param name="GroupFunctions">The GroupFunctions.</param>
        /// <returns></returns>
        public bool AddGroupFunctions(List<GroupFunction> GroupFunctions)
        {
            return repo.AddGroupFunctions(GroupFunctions);
        }
        public bool Insert(GroupFunction bo)
        {
            try
            {
                return repo.Insert(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(GroupFunction bo)
        {
            try
            {
                return repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(GroupFunction bo)
        {
            try
            {
                return repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
