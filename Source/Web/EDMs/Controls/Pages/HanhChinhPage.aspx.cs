﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Data.Entities;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using EDMs.Utilities;
using System.Data;
using Aspose.Cells;

namespace EDMs.Controls.Pages
{
    public partial class HanhChinhPage : Page
    {
        private readonly BiaTienSiService biaTienSiService = new BiaTienSiService();
        private readonly FolderPermissionService folderPermisstionService = new FolderPermissionService();
        private readonly FilePermissionService filePermissionService = new FilePermissionService();
        private readonly GroupFunctionService groupfunctionService = new GroupFunctionService();
        private readonly CategoryService categoryService = new CategoryService();
        private readonly MenuService menuService = new MenuService();
        private readonly FolderService folderService = new FolderService();
        private readonly UserService userService = new UserService();
        private readonly RoleService roleService = new RoleService();
        private readonly TaiLieuHanhChinhService taiLieuHanhChinhService = new TaiLieuHanhChinhService();

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        protected const string UnreadPattern = @"\(\d+\)";

        Nest.ConnectionSettings connectionSettings;
        Nest.ElasticClient elasticClient;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            connectionSettings = new Nest.ConnectionSettings()
                .DefaultMappingFor<documentElastic>(i => i
                .IndexName("mongduong")
                .TypeName("_doc"))
                .EnableDebugMode()
                .PrettyJson()
                .RequestTimeout(TimeSpan.FromMinutes(2));
            elasticClient = new Nest.ElasticClient(connectionSettings);
            if (!this.Page.IsPostBack)
            {
                paneMenu.Items[0].Expanded = false;
                LoadMainMenu();
                var categoryPermission = this.folderPermisstionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryId.GetValueOrDefault()).ToList();
                categoryPermission = categoryPermission.Union(this.folderPermisstionService.GetCategoryByUsser(UserSession.Current.User.Id).Select(t => t.CategoryId.GetValueOrDefault())).Distinct().ToList();
                var listCategory = UserSession.Current.RoleId == 1
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID)).ToList();

                if (listCategory.Any())
                {
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }
                    listCategory.Insert(0, new Category() { ID = -1, Name = "DANH MỤC" });
                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;
                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"/Images/collections.svg";
                        item.NavigateUrl = "HanhChinhPage.aspx?doctype=" + item.Value;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["doctype"]) && this.radPbCategories.Items.Count > 0)
                    {
                        var doctype = Convert.ToInt32(Request.QueryString["doctype"]);
                        this.lblCategoryId.Value = Request.QueryString["doctype"];
                        if (!string.IsNullOrEmpty(Request.QueryString["folId"]))
                        {
                            var folderObj = this.folderService.GetById(Convert.ToInt32(Request.QueryString["folId"]));
                            this.ReLoadTreeFolder(Convert.ToInt32(Request.QueryString["doctype"]), folderObj.ParentID.GetValueOrDefault(), folderObj.ID);
                            List<int> listTemp = new List<int>();
                            var listFolder = this.GetAllChildren(Convert.ToInt32(Request.QueryString["folId"]), ref listTemp);
                            Session["allChildFolder"] = listFolder.Distinct().ToList();
                            listFolder.Add(Convert.ToInt32(Request.QueryString["folId"]));
                            Session["ListIdFolder"] = listFolder.Distinct().ToList();
                            this.grdDocument.CurrentPageIndex = 0;
                            this.LoadDocuments(true, false, listFolder, false);
                        }
                        else
                        {
                            this.LoadTreeFolder(doctype);
                        }

                        var temp = this.radPbCategories.Items[0].Items.FindItemByValue(doctype.ToString());
                        if (temp != null)
                        {
                            temp.Selected = true;
                        }
                    }
                }
            }
        }

        private List<int> GetAllChildren(int parent, ref List<int> childNodes)
        {
            var childFolderList = this.folderService.GetAllByParentId(parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, ref childNodes);
                }
            }
            return childNodes.Distinct().ToList();
        }

        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = folderList.Where(t => t.ParentID == parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            return childNodes.Distinct().ToList();
        }

        protected void LoadDocuments(bool isbind, bool isListAllCategory, List<int> listFolder, bool isAdvanceSearch)
        {
            try
            {
                //RadSearchBox txtSearch = (RadSearchBox)CustomerMenu.Items[3].FindControl("txtSearch");
                var categoryID = Convert.ToInt32(Request.QueryString["doctype"]);
                var isSystemAdmin = this.AdminGroup.Contains(UserSession.Current.RoleId);
                //LoadDocConfigGridView();
                if (this.radTreeFolder.SelectedNode != null)
                {
                    var folderId = Convert.ToInt32(this.radTreeFolder.SelectedNode.Value);
                    var selectedFolder = this.radTreeFolder.SelectedNode;
                    var specialDocPer = new List<SpecialDocPermission>();
                    var listFolderdoc = new List<TaiLieuHanhChinh>();
                    var listDocuments = new List<TaiLieuHanhChinh>();
                    //if (!string.IsNullOrEmpty(txtSearch.Text))
                    //{
                    //    //GET THEO TAT CA FOLDEON NODE CLICK
                    //    var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                    //                          .Where(t => t.FolderId != null && (t.File_Read.GetValueOrDefault() || t.File_Write.GetValueOrDefault()) && !t.File_NoAccess.GetValueOrDefault())
                    //                          .ToList();
                    //    folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"])).Where(t => t.FolderId != null && (t.File_Read.GetValueOrDefault() || t.File_Write.GetValueOrDefault()) && !t.File_NoAccess.GetValueOrDefault())
                    //                                                    )
                    //                                                    .Distinct().ToList();
                    //    var listfolderpermission = folderPermission.Select(t => t.FolderId).ToList();
                    //    var folderfilder = isSystemAdmin ? listFolder.ToList() : listFolder.Where(t => listfolderpermission.Contains(t)).ToList();
                    //    if (isAdvanceSearch)
                    //    {
                    //        //Search query to retrieve info
                    //        var response = elasticClient.Search<documentElastic>(s => s
                    //            .Query(q => q
                    //            .Match(m => m
                    //            .Field(f => f.content).Query(txtSearch.Text + "*")))
                    //            .Size(10000));
                    //        List<string> docList = new List<string>();
                    //        foreach (var hit in response.Hits)
                    //        {
                    //            //string doc = hit.Source.file.filename.Substring(0, hit.Source.file.filename.LastIndexOf('.'));
                    //            docList.Add(hit.Source.file.filename);
                    //            //lblResult.Text += doc + "<br/>";
                    //        }
                    //        var listDocFilter = this.documentService.AdvanceSearch(categoryID, docList);
                    //        if (listDocFilter.Count > 0)
                    //        {
                    //            listDocuments = listDocFilter.Where(t => folderfilder.Contains(t.FolderID.GetValueOrDefault())).ToList();
                    //        }
                    //    }
                    //    else
                    //    {
                    //        listDocuments = this.documentService.SearchDocument(categoryID, txtSearch.Text.Trim());
                    //        listDocuments = listDocuments.Where(t => folderfilder.Contains(t.FolderID.GetValueOrDefault())).ToList();
                    //    }
                    //    var childFoldersInPermission = this.folderService.GetAllSpecificFolder(folderfilder);
                    //    var childFolders = childFoldersInPermission.Where(t => txtSearch.Text.Trim().Split(' ').ToArray().Any(k => t.Name.ToLower().Contains(k.ToLower())));
                    //    listFolderdoc = childFolders.Select(t => new Document()
                    //    {
                    //        ID = Convert.ToInt32(t.ID),
                    //        Name = t.Name,
                    //        FileExtensionIcon = "Images/folder-do.svg",
                    //        IsFolder = true,
                    //        FilePath = ""
                    //    }).ToList();
                    //    //listDocuments = listDocuments.Union(listFolderdoc).ToList();
                    //    // check permission file
                    //    var filepermission = this.filePermissionService.GetByRoleIdListCategory(GroupFunctionList, Convert.ToInt32(Request.QueryString["doctype"])).ToList();
                    //    filepermission = filepermission.Union(this.filePermissionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))).ToList();
                    //    var cheklist = filepermission.Where(t => listDocuments.Select(k => k.ID).Contains(t.DocumentID.GetValueOrDefault())).Select(k => k.DocumentID.GetValueOrDefault()).Distinct().ToList();
                    //    if (cheklist.Any())
                    //    {
                    //        List<int> removeid = new List<int>();
                    //        foreach (var item in cheklist)
                    //        {
                    //            if (filepermission.Where(t => t.DocumentID == item).Where(k => !k.Create.GetValueOrDefault() && k.ChangePermission.GetValueOrDefault() && !k.Delete.GetValueOrDefault() && !k.Read.GetValueOrDefault() && !k.Write.GetValueOrDefault()).Any())
                    //            {
                    //                removeid.Add(item);
                    //            }
                    //        }
                    //        if (removeid.Count > 0)
                    //        {
                    //            listDocuments = listDocuments.Where(t => !removeid.Contains(t.ID)).ToList();
                    //        }
                    //    }
                    //    listFolderdoc = listFolderdoc.Union(listDocuments).ToList();
                    //}
                    //else
                    //{
                    var folderPermission = (List<FolderPermission>)Session["ThisfolderPermission"];
                    listDocuments = this.taiLieuHanhChinhService.GetAllByFolder(folderId);
                    var childFolders = this.folderService.GetAllByParentId(Convert.ToInt32(selectedFolder.Value));
                    listFolderdoc = childFolders.Select(t => new TaiLieuHanhChinh()
                    {
                        ID = Convert.ToInt32(t.ID),
                        Name = t.Name,
                        FileExtensionIcon = "/Images/folder-do.svg",
                        IsFolder = true,
                        FilePath = ""
                    }).ToList();
                    //listDocuments = listDocuments.Union(listFolderdoc).ToList();
                    listFolderdoc = listFolderdoc.Union(listDocuments).ToList();

                    this.grdDocument.DataSource = listFolderdoc;
                    if (isbind)
                    {
                        this.grdDocument.DataBind();
                    }
                }
                else
                {
                    this.grdDocument.DataSource = new List<TaiLieuHanhChinh>();
                    if (isbind)
                    {
                        this.grdDocument.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                var st = ex.Message.ToString();
            }
        }

        private void LoadTreeFolder(int categoryId)
        {
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                                .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                             .Select(t => t.FolderId.GetValueOrDefault()))
                                                            .Distinct().ToList();

            var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission);
            if (listFolder.Count > 0)
            {
                var nodeRoot = listFolder.Find(t => t.ParentID == null);
                var root = new RadTreeNode();
                root.Text = nodeRoot.Name;
                root.ImageUrl = "~/Images/folder-do.svg";
                root.Value = nodeRoot.ID.ToString();
                root.Target = nodeRoot.CategoryID.ToString();
                root.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                root.Expanded = true;

                var listNode = listFolder.Where(t => t.ParentID.GetValueOrDefault() == nodeRoot.ID).OrderBy(t => t.Name);
                foreach (var folder in listNode)
                {
                    RadTreeNode node = new RadTreeNode();
                    node.Text = folder.Name;
                    node.Value = folder.ID.ToString();
                    node.ImageUrl = "~/Images/folder-do.svg";
                    node.Target = nodeRoot.CategoryID.ToString();
                    node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                    root.Nodes.Add(node);
                }

                this.radTreeFolder.Nodes.Clear();
                this.radTreeFolder.Nodes.Add(root);
            }
        }

        private void LoadMainMenu()
        {
            RadPanelItem item = paneMenu.FindItemByValue("Menu");
            RadMenu mainMenu = (RadMenu)item.FindControl("mainMenu");
            var menus = this.menuService.GetAllRelatedPermittedMenuItems(UserSession.Current.RoleId, 1).OrderBy(t => t.Priority).ToList();
            mainMenu.DataSource = menus;
            mainMenu.DataTextField = "Description";
            mainMenu.DataNavigateUrlField = "Url";
            mainMenu.DataFieldID = "Id";
            mainMenu.DataValueField = "Id";
            mainMenu.DataFieldParentID = "ParentId";
            mainMenu.DataBind();
            this.SetIcon(mainMenu.Items, menus);
        }

        private void SetIcon(RadMenuItemCollection menu, List<EDMs.Data.Entities.Menu> menuData)
        {
            foreach (RadMenuItem menuItem in menu)
            {
                if (!string.IsNullOrEmpty(menuItem.Value))
                {
                    var tempMenu = menuData.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
                    if (tempMenu != null)
                    {
                        menuItem.ImageUrl = tempMenu.Icon;
                    }
                }

                if (menuItem.Items.Count > 0)
                {
                    this.SetIcon(menuItem.Items, menuData);
                }
            }
        }

        private void ReLoadTreeFolder(int categoryId, int ParrentNodeSelect, int idFodler)
        {
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                              .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                              .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                             .Select(t => t.FolderId.GetValueOrDefault()))
                                                            .Distinct().ToList();
            var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission);
            if (listFolder.Count > 0)
            {
                var nodeRoot = listFolder.Find(t => t.ParentID == null);
                var root = new RadTreeNode();
                root.Text = nodeRoot.Name;
                root.ImageUrl = "~/Images/folder-do.svg";
                root.Value = nodeRoot.ID.ToString();
                root.Target = nodeRoot.CategoryID.ToString();
                root.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                root.Expanded = true;
                List<int> ListNodeExpanded = new List<int>();
                ListNodeExpanded.Add(idFodler);
                GetListNodeExpanded(ref ListNodeExpanded, ParrentNodeSelect);
                AddNode(ref root, listFolder, ListNodeExpanded, nodeRoot);
                this.radTreeFolder.Nodes.Clear();
                this.radTreeFolder.Nodes.Add(root);
                var selectedFolder = this.radTreeFolder.FindNodeByValue(idFodler.ToString());
                if (selectedFolder != null)
                {
                    var folder = this.folderService.GetById(Convert.ToInt32(idFodler));
                    selectedFolder.Selected = true;
                    UpdatePropertiesFolder(folder);
                    selectedFolder.Expanded = true;
                    this.lbFolderSize.Value = (folder.FolderSize / 1024 / 1024).ToString();
                }
            }
        }

        private void GetListNodeExpanded(ref List<int> ListNode, int _currentFolder)
        {
            var currentFolder = this.folderService.GetById(_currentFolder);
            if (currentFolder != null) ListNode.Add(Convert.ToInt32(currentFolder.ID));
            if (currentFolder != null && currentFolder.ParentID != null)
            {
                GetListNodeExpanded(ref ListNode, currentFolder.ParentID.GetValueOrDefault());
            }
        }

        private void GetListNodeExpanded(ref List<int> ListNode, RadTreeNode currentFolder)
        {
            if (currentFolder != null) ListNode.Add(Convert.ToInt32(currentFolder.Value));
            if (currentFolder != null && currentFolder.ParentNode != null)
            {
                GetListNodeExpanded(ref ListNode, currentFolder.ParentNode);
            }
        }

        private void AddNode(ref RadTreeNode Root, List<Folder> ListFolder, List<int> CurrentNodeTree, Folder currentfolder)
        {
            var listNode = ListFolder.Where(t => t.ParentID.GetValueOrDefault() == currentfolder.ID).OrderBy(t => t.Name);
            foreach (var folder in listNode)
            {

                RadTreeNode node = new RadTreeNode();
                node.Text = folder.Name;
                node.Value = folder.ID.ToString();
                node.ImageUrl = "~/Images/folder-do.svg";
                node.Target = currentfolder.CategoryID.ToString();
                node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                if (CurrentNodeTree.Contains(folder.ID))
                {
                    Root.Expanded = true;
                    AddNode(ref node, ListFolder, CurrentNodeTree, folder);
                }
                Root.Nodes.Add(node);
            }
        }

        private void UpdatePropertiesFolder(Folder folder)
        {
            this.lblDocumentCount.Visible = true;
            this.txtDocumentCount.Visible = true;
            this.lblParentFolder.Visible = true;
            this.txtParentFolder.Visible = true;
            this.lblFolderCount.Visible = true;
            this.txtFolderCount.Visible = true;
            var creater = this.userService.GetByID(folder.CreatedBy.GetValueOrDefault());
            var uploader = this.userService.GetByID(folder.LastUpdatedBy.GetValueOrDefault());
            this.lblCreatedBy.Text = creater != null ? creater.UserNameFullName : "-";
            this.lblCreatedTime.Text = folder.CreatedDate.ToString();
            this.lblUpdatedBy.Text = uploader != null ? uploader.UserNameFullName : "-";
            this.lblUpdatedTime.Text = folder.LastUpdatedDate.ToString();
            this.txtDocumentCount.Text = folder.NumberOfDocument.GetValueOrDefault().ToString();
            this.lblDiskUsage.Text = SizeSuffix(folder.FolderSize.HasValue ? Convert.ToInt64(folder.FolderSize.Value) : 0, 1);
            this.txtParentFolder.Text = folder.ParentName;
            this.txtFolderCount.Text = folder.NumberOfSubfolder.GetValueOrDefault().ToString();
        }

        protected readonly string[] SizeSuffixes =
           { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.lbUserId.Value = UserSession.Current.User.Id.ToString();
            this.lbRoleId.Value = UserSession.Current.User.RoleId.ToString();
            var folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));
            this.lbFolderSize.Value = (folder.FolderSize / 1024 / 1024).ToString();
            var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null)).ToList();
            var ThisfolderPermission = folderPermission.Where(t => t.FolderId == folder.ID).ToList();
            Session.Remove("ThisfolderPermission");
            ChangeTooltip(e.Node, ThisfolderPermission);
            Session["ThisfolderPermission"] = ThisfolderPermission;
            Session.Remove("allChildFolder");
            Session.Remove("ListIdFolder");
            this.LoadActionPermission(e.Node.Value);
            e.Node.Expanded = true;
            Session.Remove("ListNodeExpanded");
            List<int> ListNodeExpanded = new List<int>();
            GetListNodeExpanded(ref ListNodeExpanded, e.Node.ParentNode);
            Session["ListNodeExpanded"] = ListNodeExpanded;
            //var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
            //if (temp != null && folder != null)
            //{
            //    temp.NavigateUrl = ConfigurationManager.AppSettings["ServerName"] + folder.DirName;
            //}
            if (ThisfolderPermission.Where(t => t.Folder_Read.GetValueOrDefault()).Any() || this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                var listFolderPermission = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                                : this.folderService.GetSpecificFolderStatic(folderPermission.Where(t => t.Folder_Read.GetValueOrDefault()).Select(t => t.FolderId.GetValueOrDefault()).Distinct().ToList());
                var folderlist = listFolderPermission.Where(t => t.ParentID.GetValueOrDefault() == folder.ID).OrderBy(t => t.Name).ToList();
                if (folderlist.Any())
                {
                    e.Node.Nodes.Clear();
                    foreach (var folderitem in folderlist)
                    {
                        var nodechild = new RadTreeNode();
                        nodechild.Text = folderitem.Name;
                        nodechild.Value = folderitem.ID.ToString();
                        nodechild.ImageUrl = "~/Images/folder-do.svg";
                        nodechild.Target = e.Node.Target;
                        if (listFolderPermission.Count(t => t.ParentID.GetValueOrDefault() == folderitem.ID) > 0)
                        {
                            nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                        }
                        e.Node.Nodes.Add(nodechild);
                    }

                    e.Node.Expanded = true;
                }
                //load data folder properties
                UpdatePropertiesFolder(folder);

                List<int> listTemp = new List<int>();
                var listFolder = this.GetAllChildren(Convert.ToInt32(e.Node.Value), listFolderPermission, ref listTemp);
                Session["allChildFolder"] = listFolder.Distinct().ToList();
                listFolder.Add(Convert.ToInt32(e.Node.Value));
                Session["ListIdFolder"] = listFolder.Distinct().ToList();
                //this.grdDocument.CurrentPageIndex = 0;
                this.LoadDocuments(true, false, listFolder, false);
            }
            else
            {
                lblNotification.Text = "Access denied Folder.";
                RadNotification1.Title = "Warning";
                RadNotification1.Show();
            }
        }

        protected void radTreeFolder_NodeEdit(object sender, RadTreeNodeEditEventArgs e)
        {
            e.Node.Text = e.Text;
            var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
            var folder = new Folder();
            List<int> ListNodeExpanded = new List<int>();
            GetListNodeExpanded(ref ListNodeExpanded, e.Node.ParentNode);
            if (!string.IsNullOrEmpty(Session["FolderMenuAction"].ToString()) && Session["FolderMenuAction"].ToString() == "New")
            {
                try
                {
                    var parentFol = this.folderService.GetById(Convert.ToInt32(e.Node.ParentNode.Value));
                    var folderGUID = Guid.NewGuid();
                    folder = new Folder()
                    {
                        Name = e.Text,
                        Description = Utility.RemoveAllSpecialCharacter(e.Text),
                        CategoryID = categoryId,
                        ParentID = Convert.ToInt32(e.Node.ParentNode.Value),
                        DirName = "HanhChinh" + "/" + folderGUID,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };
                    var a = Server.MapPath(folder.DirName);
                    Directory.CreateDirectory(Server.MapPath("/" + folder.DirName));

                    var ownerGroupIds = this.roleService.GetAllOwner().Select(t => t.Id);
                    var ownerUserIds = this.userService.GetAll().Where(t => ownerGroupIds.Contains(t.RoleId.GetValueOrDefault())).Select(t => t.Id);

                    var parentFolderGroupPermission = this.folderPermisstionService.GetAllByFolder(Convert.ToInt32(e.Node.ParentNode.Value));

                    var folId = this.folderService.Insert(folder);

                    var groupPermission =
                        parentFolderGroupPermission.Select(
                            t =>
                            new FolderPermission()
                            {
                                CategoryId = t.CategoryId,
                                ObjectId = t.ObjectId,
                                ObjectIdName = t.ObjectIdName,
                                FolderId = folder.ID,
                                TypeID = t.TypeID,
                                TypeName = t.TypeName,
                                Folder_IsFullPermission = t.Folder_IsFullPermission,
                                Folder_ChangePermission = t.Folder_ChangePermission,
                                Folder_CreateSubFolder = t.Folder_CreateSubFolder,
                                Folder_Delete = t.Folder_Delete,
                                Folder_Read = t.Folder_Read,
                                Folder_Write = t.Folder_Write,
                                File_FullPermission = t.File_FullPermission,
                                File_ChangePermission = t.File_ChangePermission,
                                File_Create = t.File_Create,
                                File_Delete = t.File_Delete,
                                File_Read = t.File_Read,
                                File_Write = t.File_Write,
                                File_NoAccess = t.File_NoAccess,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            }).ToList();
                    this.folderPermisstionService.AddGroupDataPermissions(groupPermission);
                    UpdatePropertisFolder(categoryId, ListNodeExpanded);
                }
                catch (Exception ex)
                {
                }
            }
            else if (!string.IsNullOrEmpty(Session["FolderMenuAction"].ToString()) && Session["FolderMenuAction"].ToString() == "Rename")
            {
                try
                {
                    folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));
                    folder.Name = e.Text;
                    //var watcherService = new ServiceController(ServiceName);
                    //if (Utility.ServiceIsAvailable(ServiceName))
                    //{
                    //    watcherService.ExecuteCommand(128);
                    //}
                    // if (Utility.ServiceIsAvailable(ServiceName))
                    //{
                    //    watcherService.ExecuteCommand(129);
                    //}
                    this.folderService.Update(folder);
                    ////var folderPermissionTmp =
                    ////    this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault())
                    ////        .Where(t => t.CategoryIdList == categoryId.ToString() && !string.IsNullOrEmpty(t.FolderIdList))
                    ////        .Select(t => Convert.ToInt32(t.FolderIdList)).ToList();
                    ////if (selectedFolder.GetAllNodes().Count > 0)
                    ////{
                    ////    tempListFolderId.AddRange(
                    ////        from folderNode in selectedFolder.GetAllNodes()
                    ////        where folderPermissionTmp.Contains(Convert.ToInt32(folderNode.Value))
                    ////        select Convert.ToInt32(folderNode.Value));
                    ////}
                }
                catch (Exception ex)
                {
                    //var watcherService = new ServiceController("EDMSFolderWatcher");
                    //if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                    //{
                    //    watcherService.ExecuteCommand(129);
                    //}
                }
            }

            Session.Remove("FolderMenuAction");

            //this.LoadTreeFolder(categoryId);
            this.ReLoadTreeFolder(categoryId, e.Node.ParentNode, folder.ID);
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        private void ReLoadTreeFolder(int categoryId, RadTreeNode ParrentNodeSelect, int idFodler)
        {
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                              .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                              .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                             .Select(t => t.FolderId.GetValueOrDefault()))
                                                            .Distinct().ToList();
            var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission);
            if (listFolder.Count > 0)
            {
                var nodeRoot = listFolder.Find(t => t.ParentID == null);

                var root = new RadTreeNode();
                root.Text = nodeRoot.Name;
                root.ImageUrl = "~/Images/folder-do.svg";
                root.Value = nodeRoot.ID.ToString();
                root.Target = nodeRoot.CategoryID.ToString();
                root.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                root.Expanded = true;

                List<int> ListNodeExpanded = new List<int>();
                ListNodeExpanded.Add(idFodler);
                GetListNodeExpanded(ref ListNodeExpanded, ParrentNodeSelect);
                AddNode(ref root, listFolder, ListNodeExpanded, nodeRoot);
                this.radTreeFolder.Nodes.Clear();
                this.radTreeFolder.Nodes.Add(root);
            }
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            //  e.Node.ImageUrl = "Images/folder-do.svg";

            ////if (this.AdminGroup.Contains(UserSession.Current.RoleId))
            ////{
            ////    e.Node.Category = "FullPermission";
            ////}
            ////else
            ////{
            ////    var groupPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.RoleId, this.lblCategoryId.Value, e.Node.Value);
            ////    if (groupPermission != null && groupPermission.IsFullPermission.GetValueOrDefault())
            ////    {
            ////        e.Node.Category = "FullPermission";
            ////    }
            ////    else
            ////    {
            ////        var userPermission = this.userDataPermissionService.GetByUserId(
            ////            UserSession.Current.User.Id,
            ////            Convert.ToInt32(this.lblCategoryId.Value),
            ////            Convert.ToInt32(e.Node.Value));
            ////        if (userPermission != null && userPermission.IsFullPermission.GetValueOrDefault())
            ////        {
            ////            e.Node.Category = "FullPermission";
            ////        }
            ////        else
            ////        {
            ////            e.Node.Category = "Read";
            ////        }
            ////    }
            ////}
        }

        protected void radTreeFolder_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            var folderId = Convert.ToInt32(e.Node.Value);
            e.Node.Nodes.Clear();
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                                     .Where(t => t.FolderId != null).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null)).ToList();
            if (folderPermission.Where(t => t.FolderId == folderId && t.Folder_Read.GetValueOrDefault()).Any() || this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission.Select(t => t.FolderId.GetValueOrDefault()).ToList());


                var folderlist = listFolder.Where(t => t.ParentID.GetValueOrDefault() == folderId).OrderBy(t => t.Name).ToList();
                foreach (var folder in folderlist)
                {
                    var nodechild = new RadTreeNode();
                    nodechild.Text = folder.Name;
                    nodechild.Value = folder.ID.ToString();
                    nodechild.ImageUrl = "~/Images/folder-do.svg";
                    nodechild.Target = e.Node.Target;
                    if (listFolder.Count(t => t.ParentID.GetValueOrDefault() == folder.ID) > 0)
                    {
                        nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                    }
                    e.Node.Nodes.Add(nodechild);
                }

                e.Node.Expanded = true;
            }
            else
            {
                lblNotification.Text = "Access denied Folder.";
                RadNotification1.Title = "Warning";
                RadNotification1.Show();
            }
        }

        private void ChangeTooltip(RadTreeNode st, List<FolderPermission> folderPermission)
        {
            this.radMenuFolder.Visible = true;
            RadButton radButton = (RadButton)this.radMenuFolder.Items[0].FindControl("btrename");
            if (radButton != null)
            {
                radButton.ToolTip = "Rename Folder '" + st.Text + "'";
            }
            RadButton radButton1 = (RadButton)this.radMenuFolder.Items[0].FindControl("btDelete");
            if (radButton1 != null)
            {
                radButton1.ToolTip = "Delete Folder '" + st.Text + "'";
            }
            RadButton radButton2 = (RadButton)this.radMenuFolder.Items[0].FindControl("btpermission");
            if (radButton2 != null)
            {
                radButton2.ToolTip = "Change Permission '" + st.Text + "'";
            }
            RadButton radButton3 = (RadButton)this.radMenuFolder.Items[0].FindControl("btProperties");
            if (radButton3 != null)
            {
                radButton3.ToolTip = "Properties Folder '" + st.Text + "'";
            }
            RadButton radButton4 = (RadButton)this.radMenuFolder.Items[0].FindControl("btmove");
            if (radButton4 != null)
            {
                radButton4.ToolTip = "Move folder '" + st.Text + "'";
            }
            RadButton radButton5 = (RadButton)this.radMenuFolder.Items[0].FindControl("btDownloadfolder");
            if (radButton5 != null)
            {
                radButton5.ToolTip = "Download folder '" + st.Text + "'";
            }
            //check function btDownloadfolder

            if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                RadButton radButtonbtnAddFolder = (RadButton)this.radMenuFolder.Items[0].FindControl("btnAddFolder");
                if (!folderPermission.Where(t => t.Folder_CreateSubFolder.GetValueOrDefault()).Any())
                {
                    radButtonbtnAddFolder.Visible = false;
                }

                if (!folderPermission.Where(t => t.Folder_Delete.GetValueOrDefault()).Any())
                { radButton1.Visible = false; }
                if (!folderPermission.Where(t => t.Folder_Read.GetValueOrDefault()).Any())
                { radButton3.Visible = false; radButton5.Visible = false; }
                if (!folderPermission.Where(t => t.Folder_ChangePermission.GetValueOrDefault()).Any())
                { radButton2.Visible = false; }
                if (!folderPermission.Where(t => t.Folder_Write.GetValueOrDefault()).Any())
                { radButton.Visible = false; radButton4.Visible = false; }
            }
        }

        private void LoadActionPermission(string folderId)
        {
            //if (this.AdminGroup.Contains(UserSession.Current.RoleId))
            //{
            //    this.CustomerMenu.Items[0].Visible = true;
            //    this.CustomerMenu.Items[1].Visible = true;
            //    //this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = true;
            //    //this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = true;
            //}
            //else
            //{
            //    this.CustomerMenu.Items[0].Visible = false;
            //    this.CustomerMenu.Items[1].Visible = false;
            //this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //var groupPermission = this._FolderPermisstionService.GetByRoleId(UserSession.Current.RoleId, this.lblCategoryId.Value, folderId);
            //if (groupPermission != null)
            //{
            //    if (groupPermission.ReadFile.GetValueOrDefault())
            //    {
            //        this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //    }
            //    if (groupPermission.ModifyFile.GetValueOrDefault())
            //    {
            //        this.CustomerMenu.Items[0].Visible = true;
            //        this.CustomerMenu.Items[1].Visible = true;
            //        this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = true;
            //    }
            //    if (groupPermission.DeleteFile.GetValueOrDefault())
            //    {
            //        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = true;
            //    }

            //}
            //else
            //{
            //    this.CustomerMenu.Items[0].Visible = false;
            //    this.CustomerMenu.Items[1].Visible = false;
            //    this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //    var userPermission = this.userDataPermissionService.GetByUserId(
            //        UserSession.Current.User.Id,
            //        Convert.ToInt32(this.lblCategoryId.Value),
            //        Convert.ToInt32(folderId));
            //    if (userPermission != null)
            //    {
            //        if (userPermission.ReadFile.GetValueOrDefault())
            //        {

            //            this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //            this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //        }
            //        if (userPermission.ModifyFile.GetValueOrDefault())
            //        {
            //            this.CustomerMenu.Items[0].Visible = true;
            //            this.CustomerMenu.Items[1].Visible = true;
            //            this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = true;
            //        }
            //        if (userPermission.DeleteFile.GetValueOrDefault())
            //        {
            //            this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = true;
            //        }
            //    }
            //}
            // }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocuments(false, false, new List<int>(), false);
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.taiLieuHanhChinhService.Delete(disId);

            this.grdDocument.Rebind();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var ID = item.GetDataKeyValue("ID");
                ImageButton btnDelete = (ImageButton)item["DeleteButton"].Controls[0];
                List<FolderPermission> folderPermission = (List<FolderPermission>)Session["ThisfolderPermission"];
                if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                {
                    if (!folderPermission.Where(t => t.Folder_Delete.GetValueOrDefault()).Any())
                    { btnDelete.Visible = false; }
                    if (!folderPermission.Where(t => t.Folder_Write.GetValueOrDefault()).Any())
                    { btnAdd.Visible = false;  }
                }
            }
        }

        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                if (this.radTreeFolder.SelectedNode != null)
                {
                    //var filePath = Server.MapPath(@"DocumentResource\Exports") + @"\";
                    //var fileName = this.radTreeFolder.SelectedNode.ParentNode != null
                    //               ? Utility.RemoveSpecialCharacter(this.radTreeFolder.SelectedNode.ParentNode.Text) + " - "
                    //                 + Utility.RemoveSpecialCharacter(this.radTreeFolder.SelectedNode.Text)
                    //               : Utility.RemoveSpecialCharacter(this.radTreeFolder.SelectedNode.Text);

                    var folderId = Convert.ToInt32(this.lblFolderId.Value);
                    var folderObj = this.folderService.GetById(folderId);
                    var tempListFolderId = new List<int>();

                    var dtFull = new DataTable();
                    dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("FileName", typeof(String)),
                        new DataColumn("Description", typeof(String)),

                    });

                    var filePath = Server.MapPath("~/Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\DocumentList.xls");
                    var sheets = workbook.Worksheets;

                    // Export master List
                    var docInFolder = this.taiLieuHanhChinhService.GetAllByFolder(folderId);
                    int count = 0;
                    foreach (var docItem in docInFolder)
                    {
                        count++;
                        DataRow dr = dtFull.NewRow();
                        dr[0] = docItem.ID.ToString();
                        dr[1] = count.ToString();
                        dr[2] = docItem.Name;
                        dr[3] = docItem.Description;
                        dtFull.Rows.Add(dr);
                    }
                    sheets[1].Cells.ImportDataTable(dtFull, false, 4, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                    sheets[2].Cells.ImportDataTable(dtFull, false, 4, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                    sheets[1].Cells["A4"].PutValue(docInFolder.Count);
                    sheets[1].Name = folderObj.Name;
                    var fileName = folderObj.Name;
                    var saveFileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + fileName;
                    workbook.Save(filePath + saveFileName + ".xls");
                    this.DownloadByWriteByte(filePath + saveFileName + ".xls", fileName + ".xls", false);
                }
            }
            else if (e.CommandName == "RowClick")
            {
                //if (isFolder.Value == "False")
                //{
                //    this.radMenuFile.Enabled = true;
                //    var docObj = this.taiLieuHanhChinhService.GetById(Convert.ToInt32(this.lblDocId.Value));
                //    if (docObj != null)
                //    {
                //        var btnEdit = (RadButton)this.radMenuFile.Items[0].FindControl("btnEdit");
                //        var btnFileDelete = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDelete");
                //        var btnFileDownload = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDownload");
                //        var btnFileView = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileView");

                //        if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                //        {
                //            var filePermission = this.filePermissionService.GetByRoleIdList(GroupFunctionList, docObj.ID).ToList();
                //            filePermission.Add(this.filePermissionService.GetByUserI(UserSession.Current.User.Id, docObj.ID));
                //            if (filePermission.Any())
                //            {
                //                if (!filePermission.Where(t => t.Create.GetValueOrDefault()).Any())
                //                {
                //                    this.CustomerMenu.Items[0].Enabled = false;
                //                }

                //                if (!filePermission.Where(t => t.Delete.GetValueOrDefault()).Any())
                //                { btnFileDelete.Enabled = false; }
                //                if (!filePermission.Where(t => t.Read.GetValueOrDefault()).Any())
                //                {
                //                    btnFileView.Enabled = false; btnFileDownload.Enabled = false;
                //                }
                //                if (!filePermission.Where(t => t.ChangePermission.GetValueOrDefault()).Any())
                //                { }
                //                if (!filePermission.Where(t => t.Write.GetValueOrDefault()).Any())
                //                {
                //                    btnEdit.Enabled = false;
                //                }
                //            }
                //            else
                //            {
                //                var folderPermission = (List<FolderPermission>)Session["ThisfolderPermission"];
                //                if (!folderPermission.Where(t => t.File_Create.GetValueOrDefault()).Any())
                //                {
                //                    this.CustomerMenu.Items[0].Enabled = false;
                //                }

                //                if (!folderPermission.Where(t => t.File_Delete.GetValueOrDefault()).Any())
                //                { btnFileDelete.Enabled = false; }
                //                if (!folderPermission.Where(t => t.File_Read.GetValueOrDefault()).Any())
                //                {
                //                    btnFileView.Enabled = false; btnFileDownload.Enabled = false;
                //                }
                //                if (!folderPermission.Where(t => t.File_ChangePermission.GetValueOrDefault()).Any())
                //                {

                //                }
                //                if (!folderPermission.Where(t => t.File_Write.GetValueOrDefault()).Any())
                //                {
                //                    btnEdit.Enabled = false;
                //                }
                //            }
                //        }
                //        else
                //        {
                //            btnEdit.Enabled = true;
                //            btnFileDelete.Enabled = true;
                //            btnFileDownload.Enabled = true;
                //            btnFileView.Enabled = true;
                //        }
                //    }

                //    this.lblDocumentCount.Visible = false;
                //    this.txtDocumentCount.Visible = false;
                //    this.lblParentFolder.Visible = false;
                //    this.txtParentFolder.Visible = false;
                //    this.lblFolderCount.Visible = false;
                //    this.txtFolderCount.Visible = false;

                //    var creater = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());
                //    var uploader = this.userService.GetByID(docObj.LastUpdatedBy.GetValueOrDefault());
                //    this.lblCreatedBy.Text = creater != null ? creater.UserNameFullName : "-";
                //    this.lblCreatedTime.Text = docObj.CreatedDate.ToString();
                //    this.lblUpdatedBy.Text = uploader != null ? uploader.UserNameFullName : "-";
                //    this.lblUpdatedTime.Text = docObj.LastUpdatedDate.ToString();
                //    this.lblDiskUsage.Text = SizeSuffix(docObj.FileSize.HasValue ? Convert.ToInt64(docObj.FileSize.Value) : 0, 1);

                //}
                if (isFolder.Value == "True")
                {
                    //this.radMenuFile.Enabled = false;
                    var folderId = Convert.ToInt32(this.lblDocId.Value);
                    var folder = this.folderService.GetById(folderId);
                    var selectedFolder = this.radTreeFolder.FindNodeByValue(this.lblDocId.Value);
                    if (selectedFolder != null)
                    {
                        this.lbFolderSize.Value = (folder.FolderSize / 1024 / 1024).ToString();
                        this.LoadActionPermission(selectedFolder.Value);
                        var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
                        var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                                                           .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault());

                        folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                                                                 .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault()))
                                                                                                 .ToList();
                        var folderPermissionID = folderPermission.Where(t => t.FolderId == folderId).ToList();
                        var listFolderPermission = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                                                ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                                                                : this.folderService.GetSpecificFolderStatic(folderPermission.Select(t => t.FolderId.GetValueOrDefault())
                                                                                                .Distinct().ToList());
                        selectedFolder.Selected = true;
                        UpdatePropertiesFolder(folder);
                        var folderlist = listFolderPermission.Where(t => t.ParentID.GetValueOrDefault() == Convert.ToInt32(folderId)).OrderBy(t => t.Name).ToList();
                        if (folderlist.Any())
                        {
                            selectedFolder.Nodes.Clear();
                            foreach (var folderitem in folderlist)
                            {
                                var nodechild = new RadTreeNode();
                                nodechild.Text = folderitem.Name;
                                nodechild.Value = folderitem.ID.ToString();
                                nodechild.ImageUrl = "~/Images/folder-do.svg";
                                nodechild.Target = selectedFolder.Target;
                                if (listFolderPermission.Count(t => t.ParentID.GetValueOrDefault() == folderitem.ID) > 0)
                                {
                                    nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                                }
                                selectedFolder.Nodes.Add(nodechild);
                            }
                        }
                        selectedFolder.Expanded = true;

                        List<int> listTemp = new List<int>();
                        var listFolder = this.GetAllChildren(Convert.ToInt32(selectedFolder.Value), listFolderPermission, ref listTemp);
                        listFolder.Add(Convert.ToInt32(selectedFolder.Value));
                        Session["ListIdFolder"] = listFolder;
                        this.grdDocument.CurrentPageIndex = 0;
                        this.LoadDocuments(true, false, listFolder, false);
                        ChangeTooltip(selectedFolder, folderPermissionID);
                    }
                }
            }
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        protected void grdDocument_PreRender(object sender, EventArgs e)
        {
            foreach (GridDataItem item in grdDocument.MasterTableView.Items)
            {
                if (item["IsFolder"].Text.ToLower() == "true")
                {
                    item.SelectableMode = GridItemSelectableMode.None;
                }
                else
                {
                    item.SelectableMode = GridItemSelectableMode.ServerAndClientSide;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var id = Request.QueryString["docId"].ToString();
                    if (id == item.GetDataKeyValue("ID").ToString())
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        protected void btnAddFolder_Click(object sender, EventArgs e)
        {
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            Session.Add("FolderMenuAction", "New");
            //// set node's value so we can find it in startNodeInEditMode
            //newFolder.Value = newFolder.GetFullPath("/");
            RadTreeNode newFolder = new RadTreeNode(string.Format("New Folder {0}", clickedNode.Nodes.Count + 1));
            newFolder.Selected = true;
            newFolder.ImageUrl = clickedNode.ImageUrl;
            clickedNode.Nodes.Add(newFolder);
            clickedNode.Expanded = true;
            //update the number in the brackets
            if (Regex.IsMatch(clickedNode.Text, UnreadPattern))
                clickedNode.Text = Regex.Replace(clickedNode.Text, UnreadPattern, "(" + clickedNode.Nodes.Count.ToString() + ")");
            else
                clickedNode.Text += string.Format(" ({0})", clickedNode.Nodes.Count);
            clickedNode.Font.Bold = true;
            //set node's value so we can find it in startNodeInEditMode
            newFolder.Value = newFolder.GetFullPath("/");
            StartNodeInEditMode(newFolder.Value);
        }

        protected void btrename_Click(object sender, EventArgs e)
        {
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            Session.Add("FolderMenuAction", "Rename");
            this.StartNodeInEditMode(clickedNode.Value);
        }

        private void StartNodeInEditMode(string nodeValue)
        {
            //find the node by its Value and edit it when page loads
            string js = "Sys.Application.add_load(editNode); function editNode(){ ";
            js += "var tree = $find(\"" + radTreeFolder.ClientID + "\");";
            js += "var node = tree.findNodeByValue('" + nodeValue + "');";
            js += "if (node) node.startEdit();";
            js += "Sys.Application.remove_load(editNode);};";

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "nodeEdit", js, true);
        }

        protected void btDelete_Click(object sender, EventArgs e)
        {
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            Session.Add("FolderMenuAction", "Delete");
            var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
            var folDelete = this.folderService.GetById(Convert.ToInt32(clickedNode.Value));
            var folderIDs = clickedNode.GetAllNodes().Select(t => Convert.ToInt32(t.Value)).ToList();
            folderIDs.Add(Convert.ToInt32(clickedNode.Value));
            if (folDelete != null)
            {
                // Delete main folder
                var dataPermission = this.folderPermisstionService.GetAllByFolder(folDelete.ID);
                this.folderPermisstionService.DeletePermission(dataPermission);
                folDelete.IsDelete = true;
                this.folderService.Update(folDelete);

                // Delete all child Folder
                foreach (var childNode in clickedNode.GetAllNodes())
                {
                    var childFolder = this.folderService.GetById(Convert.ToInt32(childNode.Value));
                    if (childFolder != null)
                    {
                        dataPermission = this.folderPermisstionService.GetAllByFolder(Convert.ToInt32(childNode.Value));
                        this.folderPermisstionService.DeletePermission(dataPermission);
                        childFolder.IsDelete = true;
                        this.folderService.Update(childFolder);
                    }
                }

                var docList = this.taiLieuHanhChinhService.GetAllByFolder(folderIDs);
                foreach (var document in docList)
                {
                    this.taiLieuHanhChinhService.Delete(document);
                }

                if (Directory.Exists(Server.MapPath(folDelete.DirName)))
                {
                    Directory.Delete(Server.MapPath(folDelete.DirName), true);
                }
            }

            // this.LoadTreeFolder(categoryId);
            // 
            List<int> ListNodeExpanded = new List<int>();
            GetListNodeExpanded(ref ListNodeExpanded, clickedNode.ParentNode);
            clickedNode.Remove();
            UpdatePropertisFolder(categoryId, ListNodeExpanded);
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        protected void radTreeFolder_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {

        }

        protected void btnFileDelete_Click(object sender, EventArgs e)
        {
            if (this.lblDocId.Value != "")
            {
                var docId = Convert.ToInt32(this.lblDocId.Value);

                var docObj = this.taiLieuHanhChinhService.GetById(docId);
                if (docObj != null)
                {
                    if (File.Exists(Server.MapPath(docObj.FilePath)))
                    {
                        File.Delete(Server.MapPath(docObj.FilePath));
                    }
                    this.taiLieuHanhChinhService.Delete(docObj);

                    List<int> ListNodeExpanded = new List<int>();
                    GetListNodeExpanded(ref ListNodeExpanded, this.radTreeFolder.SelectedNode);
                    UpdatePropertisFolder(docObj.CategoryID.GetValueOrDefault(), ListNodeExpanded);
                    grdDocument.Rebind();
                }
            }
            else
            {
                lblNotification.Text = "Please select file";
                RadNotification1.Title = "Warning";
                RadNotification1.Show();
            }
        }

        private void UpdatePropertisFolder(int categori, List<int> ListNodeCurrent)
        {
            //var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
            //    (t=> t.ID).ToList();
            //var listFolder= listFolder
            foreach (var folderItem in ListNodeCurrent.OrderByDescending(t => t))
            {
                var objFolder = this.folderService.GetById(folderItem);
                // List<int> listTemp = new List<int>();
                var subfolderList = this.folderService.GetAllByParentId(folderItem);
                objFolder.NumberOfSubfolder = subfolderList.Count + subfolderList.Sum(t => t.NumberOfSubfolder);
                ////subfolderList.Add(objFolder);
                var docList = this.taiLieuHanhChinhService.GetAllByFolder(objFolder.ID);
                var folderSize = docList.Sum(t => t.FileSize) + subfolderList.Sum(t => t.FolderSize);
                objFolder.NumberOfDocument = docList.Count + subfolderList.Sum(t => t.NumberOfDocument);
                objFolder.FolderSize = folderSize;
                this.folderService.Update(objFolder);
            }
        }

        //protected void btnGotoFolder_Click(object sender, EventArgs e)
        //{
        //    if (this.isFolder.Value != "True")
        //    {
        //        var docObj = this.documentService.GetById(Convert.ToInt32(this.lblDocId.Value));
        //        var selectedFolder = this.folderService.GetById(docObj.FolderID.GetValueOrDefault());
        //        ReLoadTreeFolder(docObj.CategoryID.GetValueOrDefault(), selectedFolder.ParentID.GetValueOrDefault(), selectedFolder.ID);
        //        //    var selectedFolder = this.radTreeFolder.FindNodeByValue(docObj.FolderID.GetValueOrDefault());
        //        //    if (selectedFolder != null)
        //        //    {
        //        //        this.LoadActionPermission(selectedFolder.Value);
        //        //        var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
        //        //        var folderPermission = this._FolderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
        //        //                                   .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault());
        //        //        folderPermission = folderPermission.Union(this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
        //        //                                                         .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault()))
        //        //                                                         .ToList();
        //        //        var folderPermissionID = folderPermission.Where(t => t.FolderId == folderId).ToList();
        //        //        var listFolderPermission = this.AdminGroup.Contains(UserSession.Current.RoleId)
        //        //                        ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
        //        //                        : this.folderService.GetSpecificFolderStatic(folderPermission.Select(t => t.FolderId.GetValueOrDefault())
        //        //                                                        .Distinct().ToList());
        //        //        selectedFolder.Selected = true;
        //        //        var folderlist = listFolderPermission.Where(t => t.ParentID.GetValueOrDefault() == Convert.ToInt32(folderId)).OrderBy(t => t.Name).ToList();
        //        //        if (folderlist.Any())
        //        //        {
        //        //            selectedFolder.Nodes.Clear();
        //        //            foreach (var folderitem in folderlist)
        //        //            {
        //        //                var nodechild = new RadTreeNode();
        //        //                nodechild.Text = folderitem.Name;
        //        //                nodechild.Value = folderitem.ID.ToString();
        //        //                nodechild.ImageUrl = "~/Images/folder-do.svg";
        //        //                nodechild.Target = selectedFolder.Target;
        //        //                if (listFolderPermission.Count(t => t.ParentID.GetValueOrDefault() == folderitem.ID) > 0)
        //        //                {
        //        //                    nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
        //        //                }
        //        //                selectedFolder.Nodes.Add(nodechild);
        //        //            }
        //        //        }
        //        //        selectedFolder.Expanded = true;
        //        //        List<int> listTemp = new List<int>();
        //        //        var listFolder = this.GetAllChildren(Convert.ToInt32(selectedFolder.Value), listFolderPermission, ref listTemp);
        //        //        listFolder.Add(Convert.ToInt32(selectedFolder.Value));
        //        //        Session["ListIdFolder"] = listFolder;
        //        //        this.grdDocument.CurrentPageIndex = 0;
        //        //        this.LoadDocuments(true, false, listFolder);
        //        //        ChangeTooltip(selectedFolder, folderPermissionID);
        //        //    }
        //        //}
        //    }
        //}

        protected void btDownloadfolder_Click(object sender, EventArgs e)
        {
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            var selectfolder = Convert.ToInt32(clickedNode.Value);

            var folderObj = this.folderService.GetById(selectfolder);
            if (folderObj != null)
            {
                var ListChildFodler = (List<int>)Session["ListIdFolder"];
                var docList = this.taiLieuHanhChinhService.GetAllByFolder(ListChildFodler);
                var folderSize = docList.Sum(t => t.FileSize);
                if (folderSize <= 2147483648)
                {
                    var filename = DateTime.Now.ToBinary() + "_" + folderObj.Name + "_DocPack.zip";
                    var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);

                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                    {
                        zip.AlternateEncoding = Encoding.Unicode;
                        zip.AddDirectory(Server.MapPath(folderObj.DirName));
                        zip.Save(serverTotalDocPackPath);
                    };
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "alert('Hello');", true);
                    //        //this.Download_File(serverTotalDocPackPath);
                    // Response.Redirect("DownloadFile.ashx?type=3&path=" + filename);
                    Response.ClearContent();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Disposition",
                                      "attachment; filename=" + folderObj.Name + ".zip;");
                    Response.TransmitFile(serverTotalDocPackPath);
                    Response.Flush();

                    File.Delete(serverTotalDocPackPath);

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "CloseImageLoading();", true);
                    File.Delete(serverTotalDocPackPath);
                    Response.End();


                }
            }
        }

        protected void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            var list = (List<int>)Session["ListIdFolder"];
            LoadDocuments(true, false, list, true);
        }
    }
}