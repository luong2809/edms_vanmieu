﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Business.Services;
using Aspose.Cells;
using System.Data;
using EDMs.Data.Entities;

namespace EDMs.Controls.Pages
{
    public partial class BiaTienSiPage : Page
    {
        private readonly BiaTienSiService biaTienSiService = new BiaTienSiService();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.Page.IsPostBack)
            {
            }
        }

        protected void ajaxCustomer_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ImportFileLoad")
            {
                // Instantiate a new workbook
                var workbook = new Workbook();
                workbook.Open(Server.MapPath(@"/Imports/Template/bia.xls"));
                var worksheet = workbook.Worksheets[0];
                // Create a datatable
                var dataTable = new DataTable();

                // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                dataTable = worksheet.Cells.ExportDataTable(1, 1,worksheet.Cells.MaxRow, 2);
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                    {
                        var obj = new BiaTienSi();
                        obj.TenBia = dataRow["Column1"].ToString().Trim();
                        int nam;
                        int.TryParse(dataRow["Column2"].ToString().Trim(), out nam);
                        obj.NamKhoaThi = nam;
                        this.biaTienSiService.Insert(obj);
                    }
                }
            }
        }

        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            //this.LoadDocuments();
            var a = this.biaTienSiService.GetAll();
            this.grdDocument.DataSource = a;
        }

        protected void LoadDocuments(bool isbind = false)
        {
            var a = this.biaTienSiService.GetAll();
            this.grdDocument.DataSource = a;
            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.biaTienSiService.Delete(disId);

            this.grdDocument.Rebind();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var ID = item.GetDataKeyValue("ID");
                ImageButton btn = (ImageButton)item["EditButton"].Controls[0];
                btn.Attributes.Add("OnClick", "ShowEditForm(" + ID + ");return false;");
                ImageButton btnInfo = (ImageButton)item["Inforgraphic"].Controls[0];
                btnInfo.Attributes.Add("OnClick", "ShowInforgraphic(" + ID + ");return false;");
            }
        }
    }
}