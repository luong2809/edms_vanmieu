﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site_VM_Rev1.Master" AutoEventWireup="true" CodeBehind="HoanCongPage.aspx.cs" Inherits="EDMs.Controls.Pages.HoanCongPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <style>
        .span-page {
            font-size: 28px;
            color: #B51F09;
        }

        .div-tool {
            height: 70px;
            padding-right: 15px;
        }

        .RadToolBar .rtbChoiceArrow {
            width: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap {
            background-color: transparent !important;
            border: 0 !important;
        }

        .RadToolBarDropDown_Bootstrap {
            color: #B51F09 !important;
        }

        .RadToolBar_Bootstrap .rtbOuter {
            background-color: transparent !important;
            border: 0 !important;
            padding: 0 !important;
        }

        .RadToolBar_Bootstrap .rtbWrap {
            padding: 6px 10px !important;
        }

        .RadToolBar .rtbIcon {
            width: 27px;
            height: 27px;
        }

        .RadToolBarDropDown_Bootstrap .rtbIcon {
            width: 16px;
            top: 4px !important;
        }

        .RadButton_Bootstrap.rbButton {
            background-color: #4E0B0B !important;
            color: #fff !important;
        }

        .RadGrid_Silk .rgSelectedRow {
            background-color: #FAF9EF !important;
            color: #252525 !important;
        }

        .icon-grid {
            height: 24px !important;
            width: 24px !important;
        }

        #ctl00_contentTool_ctl00_contentBody_grdDocumentPanel {
            height: 100% !important;
        }

        .RadTreeView .rtImg{
            width:20px!important;
            height:20px!important;
        }
        .RadTreeView_Sunset .rtSelected .rtIn{
            background-color:unset!important;
            border-color:#4E0B0B!important;
            color:#4E0B0B!important;
        }
        .RadTreeView_Sunset .rtHovered .rtIn{
            background-color:unset!important;
            border-color:#4E0B0B!important;
            color:#4E0B0B!important;
        }
        .RadTreeView_Sunset .rtHover .rtIn{
            background-color:unset!important;
            border-color:#4E0B0B!important;
            color:#4E0B0B!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentTool" runat="server">
    <%-- manager code js --%>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function RowClick(sender, eventArgs) {
                var id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblId.ClientID %>").value = id;
            }

            function ShowEditForm(id) {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("HoanCongEditPage.aspx?obj=" + id, "EditPage");
            }

            function ShowAddForm() {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("HoanCongEditPage.aspx", "AddNew");
            }

            function refreshGrid(arg) {
                if (!arg) {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("Rebind");
                }
                else {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("RebindAndNavigate");
                }
            }

            //expand dropdown left
            function dropdownOpened(sender, args) {
                var dropDownElem = args.get_item().get_element();
                var rightPos = $telerik.getLocation(dropDownElem).x + $telerik.getSize(dropDownElem).width;
                var dropDownElemContainer = args.get_item()._animationContainer;
                var leftPos = rightPos - dropDownElemContainer.clientWidth;
                dropDownElemContainer.style.left = leftPos + "px";
            }
            //disable ajax control
            function RequestStart(sender, args) {
                args.EventTargetElement.disabled = true;
            }
            function ResponseEnd(sender, args) {
                args.EventTargetElement.disabled = false;
            }

            function onClientContextMenuItemClicking(sender, args) {
                var menuItem = args.get_menuItem();
                var treeNode = args.get_node();
                menuItem.get_menu().hide();

                switch (menuItem.get_value()) {
                    case "Add":
                        var owd = $find("<%=EditPage.ClientID %>");
                    owd.Show();
                    owd.setUrl("ConstructionEditPage.aspx?action=add&parentId=" + treeNode.get_value(), "CustomerDialog");
                    break;
                    case "Edit":
                        var owd = $find("<%=EditPage.ClientID %>");
                        owd.Show();
                        owd.setUrl("ConstructionEditPage.aspx?Id=" + treeNode.get_value(), "CustomerDialog");
                        break;
                    case "Delete":
                        var result = confirm("Are you sure you want to delete the item: " + treeNode.get_text());
                        if (result) {
                            ajaxManager.ajaxRequest("Delete_" + treeNode.get_value());
                        }
                        //args.set_cancel(!result);
                        break;
                }
            }
        </script>
    </telerik:RadCodeBlock>

    <%-- manager ajax page --%>
    <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="ajaxCustomer_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rtvDoctype">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <%-- loading page --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" MinDisplayTime="1000" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>

    <%-- hidden field --%>
    <asp:HiddenField runat="server" ID="lblId" />

    <%-- manager window popup --%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditPage" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="570" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="row justify-content-between align-items-center">
        <div class="col-lg-6">
            <span class="span-page">THÔNG TIN CÔNG TRÌNH KHU DI TÍCH</span>
        </div>
        <div class="col-lg-auto">
            <div class="row align-items-center">
                <div class="col-lg-auto" style="padding-right: 0">
                    <div class="row align-items-center div-tool">
                        <telerik:RadButton ID="btnAdd" runat="server" Text="TẠO MỚI" Skin="Bootstrap" RenderMode="Lightweight" Height="40px" Width="130px" AutoPostBack="false" OnClientClicking="ShowAddForm">
                            <Icon PrimaryIconUrl="~/Images/add-trang.svg" PrimaryIconTop="6px" PrimaryIconLeft="8px" PrimaryIconHeight="19px" PrimaryIconWidth="19px" />
                        </telerik:RadButton>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <telerik:RadToolBar ID="CustomerMenu" runat="server" Skin="Bootstrap" Width="100%" Height="40px" OnClientDropDownOpened="dropdownOpened">
                        <Items>
                            <telerik:RadToolBarDropDown ImageUrl="~/Images/settings-do.svg" DropDownWidth="150px">
                                <Buttons>
                                </Buttons>
                            </telerik:RadToolBarDropDown>
                        </Items>
                    </telerik:RadToolBar>
                </div>
                <%--<div class="col-lg-auto">
                    <div class="row align-items-center div-tool">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox-search"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn-search div-search" />
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contentMenu" runat="server">
    <telerik:RadPanelBar ID="panelMenu" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="MENU" Value="Menu" runat="server" Expanded="true" Width="100%">
                <ContentTemplate>
                    <telerik:RadMenu ID="mainMenu" runat="server" Skin="Bootstrap" RenderMode="Lightweight" Flow="Vertical" Width="100%" Height="100%"></telerik:RadMenu>
                </ContentTemplate>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="panelDoctype" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="CÔNG TRÌNH" Value="Construction" runat="server" Expanded="true" Width="100%">
                <ContentTemplate>
                    <telerik:RadTreeView ID="rtvConstruction" runat="server" Width="100%" Height="100%" ShowLineImages="False" Skin="Sunset" RenderMode="Lightweight"
                        OnNodeClick="rtvConstruction_NodeClick"
                        OnNodeDataBound="rtvConstruction_OnNodeDataBound">
                        <DataBindings>
                            <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
                        </DataBindings>
                    </telerik:RadTreeView>
                </ContentTemplate>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <%-- grid data --%>
    <telerik:RadGrid ID="grdDocument" runat="server" Skin="Silk" RenderMode="Lightweight" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None" Height="98%"
        OnItemDataBound="grdDocument_ItemDataBound" OnDeleteCommand="grdDocument_DeleteCommand" OnNeedDataSource="grdDocument_OnNeedDataSource" PageSize="100" Style="outline: none">
        <GroupingSettings CaseSensitive="false" />
        <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" TableLayout="Auto" CssClass="rgMasterTable" AllowFilteringByColumn="true">
            <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; items." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
            <Columns>
                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                <telerik:GridButtonColumn UniqueName="EditButton" ButtonType="ImageButton" ImageUrl="~/Images/edit-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn UniqueName="DeleteButton" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridTemplateColumn UniqueName="IsGenerate" AllowFiltering="False">
                    <HeaderStyle Width="30px" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <a download='<%# DataBinder.Eval(Container.DataItem, "DocumentNumber") %>'
                            href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' target="_blank">
                            <asp:Image ID="GenTrans" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>'
                                Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "HasAttachFile")) %>' Style="cursor: pointer;" />
                        </a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="ConstructionName" HeaderText="Công trình" UniqueName="ConstructionName" AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" FilterControlWidth="97%">
                    <HeaderStyle HorizontalAlign="Center" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Year" HeaderText="Năm" UniqueName="Year" AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" FilterControlWidth="97%">
                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DocumentNumber" UniqueName="DocumentNumber" HeaderText="Số tài liệu" AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" FilterControlWidth="97%">
                    <HeaderStyle HorizontalAlign="Center" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Description" HeaderText="Tiêu đề" UniqueName="Description" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Center" Width="250px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DoctypeName" HeaderText="Loại tài liệu" UniqueName="DoctypeName" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Center" Width="150px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
            <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True"></Resizing>
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowClick="RowClick"></ClientEvents>
            <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
        </ClientSettings>
    </telerik:RadGrid>
</asp:Content>
