﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TienSiFile.aspx.cs" Inherits="EDMs.Controls.Pages.TienSiFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../../Content/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../../Content/styles.css" />

    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        body {
            background-image: linear-gradient(#E6E0AE,#FAF9EF);
            background-repeat: no-repeat;
        }

        .btn-save {
            width: 100%;
            height: 40px;
            background-color: #4E0B0B;
            color: #FAF9EF;
            border: 0;
            border-radius: 3px 4px;
            /*font-family: Roboto, sans-serif;*/
            font-size: 13px;
            cursor: pointer;
        }

        .dnnFormInfo {
            width: 100%;
        }

        .row-edit-form {
            margin-bottom: 5px;
        }

        .col-label-edit-form {
            padding-left: 0;
            padding-right: 0;
        }

        .RadUpload_Default .ruFakeInput{
            width:160px!important;
            border-left-color: #B51F09 !important;
            border-width: 0 0 0 5px!important;
            border-radius: 4px!important;
        }
        .RadUpload .ruBrowse{
            background:#4E0B0B!important;
            color:#fff!important;
            border-radius: 4px!important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptBlock runat="server">
            <script src="../../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
            <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                var ajaxManager;

                function pageLoad() {
                    $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                        //alert($('iframe').contents());
                    });

                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }
                function CloseAndRebind(args) {
                    GetRadWindow().BrowserWindow.refreshGrid(args);
                    GetRadWindow().close();
                }

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                    return oWindow;
                }
                function Highlight(Id) {
                    ajaxManager.ajaxRequest("Highlight_" + Id);
                }
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>

        <div class="container-fluid">
            <div class="row row-edit-form">
                <div class="dnnFormInfo">
                    <div class="dnnFormItem dnnFormHelp dnnClear">
                        <p class="dnnFormRequired" style="float: left;">
                            <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row row-edit-form">
                <div class="col-4">
                    <span></span>
                </div>
                <div class="col-8">
                    <telerik:RadAsyncUpload runat="server" ID="docuploader" MultipleFileSelection="Disabled" TemporaryFileExpiration="05:00:00" EnableInlineProgress="true"
                        Localization-Cancel="Cancel" Localization-Remove="Remove" Localization-Select="Select">
                    </telerik:RadAsyncUpload>
                </div>
            </div>
            <div class="row justify-content-center" style="margin-top: 10px; margin-bottom:10px">
                <asp:Button ID="btnSave" runat="server" Text="LƯU" CssClass="btn-save" OnClick="btnSave_Click" Width="175px" />
            </div>
            <div class="row row-edit-form">
                <telerik:RadGrid ID="grdDocument" runat="server" Skin="Silk" RenderMode="Lightweight" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None" Height="300px"
                    OnItemDataBound="grdDocument_ItemDataBound" OnDeleteCommand="grdDocument_DeleteCommand" OnNeedDataSource="grdDocument_OnNeedDataSource" PageSize="5" Style="outline: none">
                    <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" TableLayout="Auto">
                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; items." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                            <telerik:GridBoundColumn DataField="FileExtension" UniqueName="FileExtension" Display="false"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreatedBy" UniqueName="CreatedBy" Display="false"></telerik:GridBoundColumn>
                            <telerik:GridButtonColumn UniqueName="DeleteButton" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete-do.svg" ButtonCssClass="icon-grid">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridButtonColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <a download='<%# DataBinder.Eval(Container.DataItem, "FileName") %>' href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' target="_blank">
                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' Style="cursor: pointer;" ToolTip="Download document" />
                                    </a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <%--<telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <a href='javascript:Highlight(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' Style="cursor: pointer;" ToolTip="Download document" />
                                    </a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>
                            <telerik:GridBoundColumn DataField="FileName" HeaderText="File name" UniqueName="FileName">
                                <HeaderStyle HorizontalAlign="Center" Width="40%" />
                                <ItemStyle HorizontalAlign="Left" Width="40%" />
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="CreatedByUser" HeaderText="Upload by" UniqueName="CreatedByUser">
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Upload time" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm tt}">
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="FileSize" HeaderText="File size(Kb)" UniqueName="FileSize" DataFormatString="{0:0,0.00}">
                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                        <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True"></Resizing>
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </form>
</body>
</html>
