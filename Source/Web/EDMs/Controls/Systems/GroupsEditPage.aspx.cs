﻿using System;
using System.Web.UI;
using EDMs.Data.Entities;
using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using EDMs.Utilities;

namespace EDMs.Controls.Systems
{
    public partial class GroupsEditPage : Page
    {
        private readonly UserService userService = new UserService();
        private readonly RoleService roleService = new RoleService();

        public GroupsEditPage()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = roleService.GetByID(Convert.ToInt32(Request.QueryString["obj"]));
                    if (obj != null)
                    {
                        this.txtName.Text = obj.Name;
                        this.txtDescription.Text = obj.Description;
                        this.txtGroupmail.Text = obj.Email;
                        this.checkActive.Checked = obj.Active.GetValueOrDefault();
                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);
                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["obj"]))
                {
                    var id = Convert.ToInt32(this.Request.QueryString["obj"]);
                    var obj = this.roleService.GetByID(id);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.Email = this.txtGroupmail.Text.Trim();
                        obj.Active = this.checkActive.Checked;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        this.roleService.Update(obj);
                    }
                }
                else
                {
                    var obj = new Role();
                    obj.Name = this.txtName.Text.Trim();
                    obj.Description = this.txtDescription.Text.Trim();
                    obj.Email = this.txtGroupmail.Text.Trim();
                    obj.Active = this.checkActive.Checked;
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    this.roleService.Insert(obj);
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "closeScript", "CloseAndRebind();", true);
            }
        }
    }
}