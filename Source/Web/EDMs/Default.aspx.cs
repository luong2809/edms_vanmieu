﻿using EDMs.Business.Services;
using EDMs.Utilities.Sessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace edms
{
    public partial class _Default : Page
    {
        private readonly UserService userService = new UserService();
        private readonly PermissionService permissionService = new PermissionService();
        private readonly MenuService menuService = new MenuService();
        protected void Page_Load(object sender, EventArgs e)
        {
            var user = userService.GetByID(UserSession.Current.User.Id);
            var permission = permissionService.GetByRoleId(user.RoleId.GetValueOrDefault()).First();
            var menu = menuService.GetByID(permission.MenuId);
            Response.Redirect(menu.Url);
        }
    }
}