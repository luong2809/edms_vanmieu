﻿using EDMs.Utilities.Sessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace edms
{
    public partial class Site_VM_Rev1 : System.Web.UI.MasterPage
    {

        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblTime.Text = DateTime.Now.ToString("dddd, dd MMM");
            this.lblUser.Text = "Xin chào, " + UserSession.Current.User.Username;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!UserSession.Current.IsAvailable)
            {
                if (Request.RawUrl != "/Controls/Systems/Login.aspx")
                {
                    Session.Add("ReturnURL", Request.RawUrl);
                }
                Response.Redirect("~/Controls/Systems/Login.aspx");
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? string.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? string.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            UserSession.DestroySession();
            Response.Redirect("~/Controls/Systems/Login.aspx");
            Response.StatusCode = 401;
            Response.StatusDescription = "Unauthorized";
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");
            Response.End();
            Session.Clear();
        }

        protected void toolbarAvatar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton btn = e.Item as RadToolBarButton;
            if (btn.CommandName == "Logout")
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                UserSession.DestroySession();
                Response.Redirect("~/Controls/Systems/Login.aspx");
                Response.StatusCode = 401;
                Response.StatusDescription = "Unauthorized";
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");
                Response.End();
                Session.Clear();
            }
        }
    }
}