﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaiLieuHanhChinhDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class TaiLieuHanhChinhDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaiLieuHanhChinhDAO"/> class.
        /// </summary>
        public TaiLieuHanhChinhDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TaiLieuHanhChinh> GetIQueryable()
        {
            return EDMsDataContext.TaiLieuHanhChinhs;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHanhChinh> GetAll()
        {
            return EDMsDataContext.TaiLieuHanhChinhs.ToList();
        }

        /// <summary>
        /// The get all by owner.
        /// </summary>
        /// <param name="createdBy">
        /// The created by.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHanhChinh> GetAllByFolder(int folderID)
        {
            return EDMsDataContext.TaiLieuHanhChinhs.Where(t => t.FolderID == folderID).ToList();
        }

        public List<TaiLieuHanhChinh> GetAllByFolder(List<int> folderID)
        {
            return EDMsDataContext.TaiLieuHanhChinhs.Where(t => folderID.Contains(t.FolderID.Value)).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public TaiLieuHanhChinh GetById(int id)
        {
            return this.EDMsDataContext.TaiLieuHanhChinhs.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TaiLieuHanhChinh> GetSpecific(int folderID, string fileName)
        {
            return this.EDMsDataContext.TaiLieuHanhChinhs.Where(t => t.FolderID == folderID && t.FileNameOriginal==fileName).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(TaiLieuHanhChinh ob)
        {
            try
            {
                EDMsDataContext.AddToTaiLieuHanhChinhs(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(TaiLieuHanhChinh src)
        {
            try
            {
                TaiLieuHanhChinh des = (from rs in this.EDMsDataContext.TaiLieuHanhChinhs
                                where rs.ID == src.ID
                                select rs).First();

                des.Name = src.Name;
                des.Description = src.Description;
                des.CategoryID = src.CategoryID;
                des.FileNameOriginal = src.FileNameOriginal;
                des.FolderID = src.FolderID;
                des.FilePath = src.FilePath;
                des.FileExtension = src.FileExtension;
                des.FileExtensionIcon = src.FileExtensionIcon;
                des.FileSize = src.FileSize;
                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(TaiLieuHanhChinh src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
