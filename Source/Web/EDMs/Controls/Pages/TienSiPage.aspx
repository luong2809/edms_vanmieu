﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site_VM.Master" AutoEventWireup="true" CodeBehind="TienSiPage.aspx.cs" Inherits="EDMs.Controls.Pages.TienSiPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <style>
        .span-page {
            font-size: 28px;
            color: #B51F09;
        }

        .div-tool {
            height: 70px;
            padding-right: 15px;
        }

        .textbox-search {
            border: 0;
            height: 40px;
            border-radius: 3px 0 0 3px;
            padding-left: 10px;
        }

        .no-padding-col {
            padding-left: 0;
            padding-right: 0;
        }

        .div-search {
            height: 40px;
            width: 40px;
            background-color: #fff;
        }

        .btn-search {
            background-image: url(../../Images/search-do.svg);
            background-size: 19px;
            background-repeat: no-repeat;
            background-position: center;
            border: 0;
            box-shadow: none;
            border-radius: 0 3px 3px 0;
        }

        .ddl-bia {
            height: 26px !important;
        }

        #ctl00_contentTool_ctl00_contentBody_grdDocumentPanel {
            height: 100% !important;
        }

        .RadComboBox_Bootstrap .rcbHovered{
            border-color:#4E0B0B!important;
            color:#343434!important;
            background-color:#fff!important;
        }

        .RadComboBoxDropDown_Bootstrap .rcbItem{
            color:#343434!important;
        }

        .RadComboBoxDropDown_Bootstrap .rcbHovered{
            color:#fff!important;
            background-color:#4E0B0B!important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentTool" runat="server">
    <%-- manager code js --%>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $('iframe').load(function () {
                    //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function RowClick(sender, eventArgs) {
                var id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblId.ClientID %>").value = id;
            }

            function ShowInfographic(id) {
                var owd = $find("<%=Inforgraphic.ClientID %>");
                owd.Show();
                owd.setUrl("ViewImage.aspx?obj=" + id, "Inforgraphic");
            }

            function ShowEditForm(id) {
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("TienSiEditPage.aspx?obj=" + id, "EditPage");
            }

            function ShowAddForm() {
                var biaid = document.getElementById("<%= lblBiaId.ClientID %>").value;
                var owd = $find("<%=EditPage.ClientID %>");
                owd.Show();
                owd.setUrl("TienSiEditPage.aspx?bia=" + biaid, "AddNew");
            }

            function ShowAttachForm(id) {
                var owd = $find("<%=AttachFile.ClientID %>");
                owd.Show();
                owd.setUrl("TienSiFile.aspx?obj=" + id, "AttachFile");
            }

            function refreshGrid(arg) {
                if (!arg) {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("Rebind");
                }
                else {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("RebindAndNavigate");
                }
            }

            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                if (strValue == "sync") {
                    ajaxManager.ajaxRequest("sync");
                }
                if (strValue == "importdata") {
                    var owd = $find("<%=AttachFile.ClientID %>");
                    owd.Show();
                    owd.setUrl("ImportDocMasterList.aspx", "Import");
                }
            }

            //disable ajax control
            function RequestStart(sender, args) {
                args.EventTargetElement.disabled = true;
            }
            function ResponseEnd(sender, args) {
                args.EventTargetElement.disabled = false;
            }

        </script>
    </telerik:RadCodeBlock>

    <%-- manager ajax page --%>
    <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="ajaxCustomer_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlBia">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="lblBiaId"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <%-- loading page --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" MinDisplayTime="1000" runat="server" Skin="Simple"></telerik:RadAjaxLoadingPanel>

    <%-- hidden field --%>
    <asp:HiddenField runat="server" ID="lblId" />
    <asp:HiddenField runat="server" ID="lblBiaId" />

    <%-- manager window popup --%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditPage" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="570" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="AttachFile" runat="server" Title="File" VisibleStatusbar="false" Height="450" Width="650" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="Inforgraphic" runat="server" Title="THÔNG TIN" VisibleStatusbar="false" Height="2500" Width="1500" Left="150px" Style="z-index: 8001" CenterIfModal="true" Modal="true" KeepInScreenBounds="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="row justify-content-between align-items-center">
        <div class="col-lg-4">
            <span class="span-page">TIẾN SĨ</span>
        </div>
        <div class="col-lg-auto">
            <div class="row align-items-center">
                <div class="col-lg-auto" style="padding-right: 0">
                    <div class="row align-items-center div-tool">
                        <telerik:RadButton ID="btnAdd" runat="server" Text="TẠO MỚI" Skin="Bootstrap" RenderMode="Lightweight" Height="40px" Width="130px" AutoPostBack="false" OnClientClicking="ShowAddForm">
                            <Icon PrimaryIconUrl="~/Images/add-trang.svg" PrimaryIconTop="6px" PrimaryIconLeft="8px" PrimaryIconHeight="19px" PrimaryIconWidth="19px" />
                        </telerik:RadButton>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <telerik:RadToolBar ID="CustomerMenu" runat="server" Skin="Bootstrap" Width="100%" Height="40px" OnClientButtonClicking="OnClientButtonClicking">
                        <Items>
                            <telerik:RadToolBarDropDown ImageUrl="~/Images/settings-do.svg" DropDownWidth="150px">
                                <Buttons>
                                    <telerik:RadToolBarButton runat="server" Value="sync" Text="Sync Data"></telerik:RadToolBarButton>
                                    <telerik:RadToolBarButton runat="server" Value="importdata" Text="Import Data"></telerik:RadToolBarButton>
                                </Buttons>
                            </telerik:RadToolBarDropDown>
                        </Items>
                    </telerik:RadToolBar>
                </div>
                <div class="col-lg-auto">
                    <div class="row align-items-center div-tool">
                        <telerik:RadComboBox ID="ddlBia" runat="server" Skin="Bootstrap" RenderMode="Lightweight" InputCssClass="ddl-bia" Style="width: 100% !important;" MaxHeight="450px" AutoPostBack="true" OnSelectedIndexChanged="ddlBia_SelectedIndexChanged"></telerik:RadComboBox>
                    </div>
                </div>
                <div class="col-lg-auto">
                    <div class="row align-items-center div-tool">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox-search" placeholder="Advance Search"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn-search div-search" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <%-- grid data --%>
    <telerik:RadGrid ID="grdDocument" runat="server" Skin="Silk" RenderMode="Lightweight" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None" Height="98%"
        OnItemDataBound="grdDocument_ItemDataBound" OnDeleteCommand="grdDocument_DeleteCommand" OnNeedDataSource="grdDocument_OnNeedDataSource" PageSize="100" Style="outline: none">
        <GroupingSettings CaseSensitive="false" />
        <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" TableLayout="Auto" AllowFilteringByColumn="true">
            <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; items." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
            <Columns>
                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                <telerik:GridButtonColumn UniqueName="EditButton" ButtonType="ImageButton" ImageUrl="~/Images/edit-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn UniqueName="DeleteButton" CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn UniqueName="AttachFile" ButtonType="ImageButton" ImageUrl="~/Images/attach-file-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn UniqueName="Infographic" ButtonType="ImageButton" ImageUrl="~/Images/info-do.svg" ButtonCssClass="icon-grid">
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn DataField="HoTen" HeaderText="Họ tên" UniqueName="HoTen" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NamSinh" HeaderText="Năm sinh" UniqueName="NamSinh" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NamMat" HeaderText="Năm mất" UniqueName="NamMat" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TieuSu" HeaderText="Tiểu sử" UniqueName="TieuSu" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="QueQuan" HeaderText="Quê quán" UniqueName="QueQuan" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="250px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="GiaDinh" HeaderText="Gia đình" UniqueName="GiaDinh" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" Width="150px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DongHo" HeaderText="Dòng họ" UniqueName="DongHo" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" Width="150px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NamThiDo" HeaderText="Năm thi đỗ" UniqueName="NamThiDo" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="80px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TrieuDai" HeaderText="Triều đại" UniqueName="TrieuDai" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="120px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ChucQuan" HeaderText="Chức quan" UniqueName="ChucQuan" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="180px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SuNghiep" HeaderText="Sự nghiệp" UniqueName="SuNghiep" AllowFiltering="false" ShowFilterIcon="False" FilterControlWidth="97%" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true">
                    <HeaderStyle HorizontalAlign="Center" Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DongGop" HeaderText="Đóng góp" AllowFiltering="false" UniqueName="DongGop">
                    <HeaderStyle HorizontalAlign="Center" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TacPhamDeLai" HeaderText="Tác phẩm để lại" AllowFiltering="false" UniqueName="TacPhamDeLai">
                    <HeaderStyle HorizontalAlign="Center" Width="250px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ThongTinChiTietDiaPhuong" AllowFiltering="false" HeaderText="Thông tin chi tiết địa phương" UniqueName="ThongTinChiTietDiaPhuong">
                    <HeaderStyle HorizontalAlign="Center" Width="250px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ThongTinChiTietVanHoaDiaPhuong" AllowFiltering="false" HeaderText="Thông tin chi tiết văn hóa địa phương" UniqueName="ThongTinChiTietVanHoaDiaPhuong" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
            <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True"></Resizing>
            <Selecting AllowRowSelect="true" />
            <ClientEvents OnRowClick="RowClick"></ClientEvents>
            <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
        </ClientSettings>
    </telerik:RadGrid>
</asp:Content>
